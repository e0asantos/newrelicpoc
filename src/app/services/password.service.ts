import { Injectable } from '@angular/core';
import { RequestOptions, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

// Providers
import { HttpCemex, ProjectSettings } from '@cemex-core/angular-services-v2/dist';

@Injectable()
export class PasswordService {
    private options = new RequestOptions();
    constructor(
        private http: HttpCemex,
        private projectEnv: ProjectSettings,
    ) {
        this.options.headers = new Headers();
        this.options.headers.append('Content-Type', 'application/json');
    }

    public resetPassword(request: any): Observable<Response> {
        return this.http.post(this.projectEnv.getBaseOrgEnvPath() +
            'v5/secm/user/resetpassword', request, this.options)
        .map((res: Response) => res.json())
        .catch(err => this.handleError(err));
    }

    public changePassword(otp: string, password: string, country: string , userAccount: string): Observable<Response> {
        const data = { token: otp, password, country, userAccount };
        return this.http.post(this.projectEnv.getBaseOrgEnvPath() +
            'v5/secm/user/changePassword/token', data, this.options)
        .map((res: Response) => res.json())
        .catch(err => this.handleError(err));
    }

    public requestPasswordChange(email: string): Observable<Response> {
        return this.http.get(this.projectEnv.getBaseOrgEnvPath() +
            'v5/secm/user/changePassword?' + 'userId=' + email, this.options)
        .map((res: Response) => res.json())
        .catch(err => this.handleError(err));
    }

    public getUserProfile(token: string , countryCode: string): Observable<any> {
        return this.http.get(this.projectEnv.getBaseOrgEnvPath() + 'v6/secm/users/token?tokenId=' + token + '&country=' + countryCode)
        .map((res: any) => res.json())
        .catch((error: any) => this.handleError(error));
    }

    private handleError(error: Response) {
        return Observable.throw(error || 'Server error');
    }
}
