import { Component, ViewChild, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { TranslationService } from '@cemex-core/angular-localization-v1/dist/';
import { CustomerDetail } from '../../../models/index';
import { CmxDialogComponent } from '@cemex/cmx-dialog-v4/dist';
import { SharedFunctions } from '../../../helpers/functions/';
import { SearchComponent } from '../../../components/search/search.component';

@Component({
  selector: 'legal-entities-modal',
  templateUrl: './legal-entities.modal.html',
  styleUrls: ['./legal-entities.modal.scss']
})

export class LegalEntitiesModalComponent {

    @Input() public templateId: string;
    @Input() public isRTL = false;
    @ViewChild('legalEntities') public legalEntities: CmxDialogComponent;
    @ViewChild('searchModalLegalEntities') public searchModalLegalEntities: SearchComponent;
    public customers: CustomerDetail[] = [];
    public selectedCustomers: CustomerDetail[] = [];
    public filteredCustomers: CustomerDetail[] = [];
    public defaulCustomer = 0;
    // tslint:disable-next-line:no-output-on-prefix
    @Output() public onAssignLegalEntities = new EventEmitter();

    constructor(
        public sharedFunctions: SharedFunctions,
        public t: TranslationService
    ) { }

    public open() {
        this.legalEntities.open();
    }

    public close() {
        this.refreshModal()
        this.legalEntities.close();
    }

    public refreshModal() {
        this.searchModalLegalEntities.searchTerm = '';
        this.customers = [];
        this.selectedCustomers = [];
        this.filteredCustomers = [];
    }
    public updateSearchTerm(value: string): void {
        const arrayFilter = {
            customerDesc: value,
            customerCode: value,
            vat: value
        };
        this.filteredCustomers = this.sharedFunctions.filterData(this.customers, arrayFilter);
    }

    public assign() {
        this.onAssignLegalEntities.emit(this.selectedCustomers);
        this.refreshModal()
        this.legalEntities.close();
    }

    public updSelectedCustomers(event) {
        this.selectedCustomers = event || [];
    }

}
