import { Component, OnInit, OnDestroy, ViewChild, HostListener, AfterViewInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Router } from '@angular/router';

// CMX Dependencies
import { CmxDialogComponent } from '@cemex/cmx-dialog-v4';

// Providers
import { SessionService, Logger } from '@cemex-core/angular-services-v2/dist/';
import { TranslationService } from '@cemex-core/angular-localization-v1/dist';
import { CustomerService, RequestService, JobsiteService, ErrorService } from '../../services';

// Models
import { CustomerDetail, Jobsite, UserProfile, DataType, EmailStatus, ApiError } from '../../models';

import { SharedFunctions } from '../../helpers/functions';
import { LocalizationComponent } from './../../components/localization/localization.component';

@Component({
    selector: 'app-data-request',
    templateUrl: './data-request.component.html',
    styleUrls: ['./data-request.component.scss']
})
export class DataRequestComponent extends LocalizationComponent implements OnInit, OnDestroy {

    @ViewChild('successModal')
    public successModal: CmxDialogComponent;

    @ViewChild('errorModal')
    public errorModal: CmxDialogComponent;

    @ViewChild('pendingChangesWarningModal')
    public pendingChangesWarningModal: CmxDialogComponent;

    public dataTypes: DataType[] = [
        {
            name: 'views.global.lbl_legalEntities',
            code: 'customer'
        },
        {
            name: 'views.global.lbl_locations',
            code: 'jobsite'
        }
    ];
    public selectedDataType = 'views.global.lbl_legalEntities';

    public userCustomersIds: string;
    public userCustomers = new BehaviorSubject<CustomerDetail[]>([]);

    public jobsites = new BehaviorSubject<Jobsite[]>([]);
    public userJobsites = new BehaviorSubject<Jobsite[]>([]);

    public jobsitesBk: Jobsite[] = [];
    public selectedJobsites: Jobsite[] = [];

    public breadCrumbs: any[] = [];

    private user: UserProfile;
    private subscription: Subscription;
    private searchTerm = '';
    private loading = false;
    private redirectTo = '';
    public errorMsg = '';

    constructor(
        public translationService: TranslationService,
        private customerService: CustomerService,
        private sessionService: SessionService,
        private jobsiteService: JobsiteService,
        private userRequestService: RequestService,
        private errorService: ErrorService,
        private router: Router,
        private functions: SharedFunctions) {
        super();

        this.user = this.sessionService.userProfile;
    }

    public ngOnInit(): void {
        this.breadCrumbs = [
            { item: 'views.requestData.lblProfile', link: '/profile' },
            { item: 'views.requestData.lblRequest' },
        ];
        this.getUserJobsites();
        this.getUserCustomers();
        this.successModal.afterClosed().subscribe((result: boolean) => {
            this.router.navigate(['/profile']);
        });
    }

    public ngOnDestroy(): void {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    public canDeactivate() {
        if (this.hasChanges) {
            this.pendingChangesWarningModal.open();
            return false;
        }
        return true;
    }

    @HostListener('window:beforeunload', ['$event'])
    public canLeave($event) {
        if (this.hasChanges) {
            return false;
        }
    }

    public get hasChanges(): boolean {
        return this.selectedJobsites.length > 0;
    }

    // Fetching data
    private getUserJobsites(): void {
        this.jobsiteService.getJobsitesForUser(this.user.userId).subscribe(
            (data: Jobsite[]) => this.userJobsites.next(data),
            (error) => this.throwError(error));
    }

    private getUserCustomers(): void {
        this.customerService.getCustomersForUser(this.user.userId).subscribe(
            (data: CustomerDetail[]) => {
                this.userCustomers.next(data);
                this.userCustomersIds = data.length > 0 ? data.map((c) => c.customerId).toString() : '';
                this.getUserJobsitesForCustomers();
            },
            (error) => this.throwError(error));
    }

    private getUserJobsitesForCustomers(): void {
        this.jobsiteService
            .getUserJobsitesForCustomers(this.userCustomersIds)
            .subscribe((data: Jobsite[]) => {
                const unassignedJobsites = data.filter((j) => !this.userHasJobsite(j.jobsiteId)) || [];
                this.jobsites.next(unassignedJobsites);
                this.jobsitesBk = unassignedJobsites;
            },
                (error) => this.throwError(error));
    }

    private throwError(error, useModal?: boolean): void {
        this.errorService.handleError(error)
            .subscribe((apiError: ApiError) => {
                if (apiError) {
                    this.errorMsg = `${apiError.errorCode}: ${this.translationService.pt(apiError.errorDesc)}`;
                    if (useModal) {
                        this.errorModal.open();
                    } else {
                        Logger.error(this.errorMsg);
                    }
                }
            });
    }

    public leaveRoute() {
        this.selectedJobsites = [];
        this.router.navigate([this.redirectTo]);
    }

    private userHasJobsite(jobsiteId: number): boolean {
        return this.userJobsites.getValue().findIndex((jobsite) => jobsite.jobsiteId === jobsiteId) > -1;
    }

    private selectJobsites(value) {
        this.selectedJobsites = value;
    }

    private goToProfile(): void {
        this.redirectTo = 'profile';
        this.router.navigate(['profile']);
    }

    private goToHome(): void {
        this.redirectTo = 'dashboard';
        this.router.navigate(['dashboard']);
    }

    private filter(filterBy: string): void {
        this.searchTerm = filterBy;
        if (this.searchTerm !== '') {
            const arrayFilter = {
                customerDesc: this.searchTerm,
                jobsiteCode: this.searchTerm,
                jobsiteDesc: this.searchTerm
            };
            this.jobsites.next(this.functions.filterData(this.jobsitesBk, arrayFilter));
        } else {
            this.jobsites.next(this.jobsitesBk);
        }
    }

    private submitDataAccessRequest(): void {
        const dataRequest: any = {
            request: {
                type: 'authorization',
                jobsiteList: this.selectedJobsites.map((j) => {
                    return {
                        jobsiteId: j.jobsiteId,
                        jobsiteCode: j.jobsiteCode,
                        jobsiteDesc: j.jobsiteDesc,
                        customerId: j.customerId,
                        customerDesc: j.customerDesc
                    };
                })
            },
            notes: 'Data Request'
        };

        this.userRequestService
            .submitDataAccessRequest(dataRequest)
            .subscribe((response) => {
                if (response && !response['emailIsSent']) {
                    const emailStatus: EmailStatus = {
                        email: undefined,
                        sent: false
                    };
                    this.errorService.emailStatus = emailStatus;
                }
                this.successModal.open();
                this.selectedJobsites = [];
                window.scrollTo(0, 0);
            },
                (error) => this.throwError(error, true));
    }
}
