import { Component, EventEmitter, Input, Output } from '@angular/core';

// Providers
import { TranslationService } from '@cemex-core/angular-localization-v1/dist/';

// Models
import { Role, Application } from '../../models';

// Components
import { LocalizationComponent } from '../localization/localization.component';

@Component({
    selector: 'permission',
    templateUrl: 'permission.component.html',
    styleUrls: ['permission.component.scss']
})
export class PermissionComponent extends LocalizationComponent {

    @Input('userApplications')
    public userApplications: Application[];
    @Input()
    public counter: number;
    @Input()
    public country: string;
    @Input()
    public templateId: string;
    @Output()
    public onRoleAdded = new EventEmitter();
    @Output()
    public onApplicationSelected = new EventEmitter();
    @Output()
    public onDelete = new EventEmitter();
    @Output()
    public onDuplicate = new EventEmitter();

    public applicationPlaceholder = 'forms.placeholder.application';
    public rolePlaceholder = '';
    public roles: Role[];

    private selectedRoles: Role[] = new Array<Role>();
    private selectedApplication: Application;

    constructor(public translationService: TranslationService) {
        super();
        this.rolePlaceholder = this.translationService.pt('forms.placeholder.roles');
    }

    private addApplication(application: Application) {
        this.rolePlaceholder = this.translationService.pt('forms.placeholder.roles');
        let appHandler = {};
        this.filterAppList(application);
        if (this.selectedApplication === undefined) {
            this.selectedApplication = application;
            appHandler = {
                applicationToAdd: this.selectedApplication,
                applicationToRemove: undefined,
                added: true
            };
        } else {
            appHandler = {
                applicationToAdd: application,
                applicationToRemove: this.selectedApplication,
                added: false
            };

            this.selectedApplication = application;
        }
        this.applicationPlaceholder = application.applicationName;
        this.selectedRoles = [];
        this.roles = application.roles;
        if (this.roles.length === 0) {
            this.rolePlaceholder = this.translationService.pt('views.requestPermission.noRoles');
        }
        this.onApplicationSelected.emit(appHandler);
    }

    private addRoles(event, role: Role) {
        if (typeof event === 'boolean') {
            let roleHandler;
            if (event) {
                this.selectedRoles.push(role);
                roleHandler = {
                    role,
                    added: true,
                    appId: this.selectedApplication.applicationId
                };
            } else {
                this.selectedRoles = this.selectedRoles.filter((r) => r.roleId !== role.roleId);
                roleHandler = {
                    role,
                    added: false,
                    appId: this.selectedApplication.applicationId
                };
            }
            this.setRolePlaceholder();
            this.onRoleAdded.emit(roleHandler);
        }
    }

    public removeApplication() {
        this.onDelete.emit(this.selectedApplication);
    }

    private setRolePlaceholder(): void {
        if (this.selectedRoles.length > 0) {
            this.rolePlaceholder = this.selectedRoles.map((r) => {
                return this.translationService.pt(r.roleCode);
            }).join(', ');
        } else {
            this.rolePlaceholder = this.translationService.pt('forms.placeholder.roles');
        }
    }

    private filterAppList(app: Application) {
        this.userApplications.splice(this.userApplications.indexOf(app), 1);
        if (this.selectedApplication !== undefined && this.userApplications.indexOf(this.selectedApplication) === -1) {
            this.userApplications.push(this.selectedApplication);
        }
    }
}
