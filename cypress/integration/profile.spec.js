describe('Profile', function () {
    before(function () {
        cy.clearCookies();
        cy.clearLocalStorage();
        cy.window().then((win) => {
            win.sessionStorage.clear();
        });
    });
    beforeEach(function () {
        window.localStorage.setItem('language', 'en_US');
    });
    it('Login Page', function () {
        cy.visit('http://localhost:4200/login/');
    });
    it('Authenticate', function () {
        cy.get('#cmx-login-form-username-field').clear();
        cy.get('#cmx-login-form-username-field').type('da.us.qa@yopmail.com');
        cy.get('#cmx-login-form-password-field').clear();
        cy.get('#cmx-login-form-password-field').type('Amazon123!');
        cy.get('#cmx-login-form-submit-btn').click({ force: true });
        cy.wait(5000);
    });
    it('It is on Dashboard', function () {
        cy.url().should('include', 'dashboard');
    });
    it('Go to Profile', function () {
        cy.get('#cmx-header-profile-menu').click({ force: true });
        cy.get('#cmx-header-profile-link').click({ force: true });
        cy.wait(2000);
    });
    it('It is on Profile page', function () {
        cy.url().should('contain', '/profile');
        cy.get('#cmx-title-my-profile').click({ force: true });
    });
    it('User has Track on My Apps and Roles', function () {
        cy.get('#cmx-app-title-Foreman_App-MYPROFILE').click({ force: true });
    });
    it('Click my customers tab', function () {
        cy.get('#1').should('have.class', 'nav-link');
        cy.get('#1').click({ force: true });
    });
    it('Displaying user customers', function () {
        cy.get('#cmx-table-customers').click({ force: true });
    });
    it('Click my Locations tab', function () {
        cy.get('#2').should('have.class', 'nav-link');
        cy.get('#2').click({ force: true });
    });
    it('Click request tab', function () {
        cy.get('#3').should('have.class', 'nav-link');
        cy.get('#3').click({ force: true });
    });
    it('Displaying user requests', function () {
        cy.get('#cmx-table-requests').click({ force: true });
    });
    it('Show request detail', function () {
        cy.get('#cmx-btn-request-detail-0-MYPROFILE').click({ force: true });
        cy.wait(1000);
        cy.get('#cmx-request-detail-modal').click({ force: true });
    });
    it('Go to Self-Reset Password', function () {
        cy.get('#cmx-profile-box-selfreset-btn').click({ force: true });
        cy.wait(1000);
        cy.url().should('eq', 'http://localhost:4200/profile/self-reset-password');
    });
});