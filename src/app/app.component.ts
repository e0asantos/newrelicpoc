import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { TranslationService } from '@cemex-core/angular-localization-v1/dist/';
import { LocalizationComponent } from './components/localization/localization.component';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent extends LocalizationComponent implements OnInit {

    constructor(private router: Router, private translationService: TranslationService) {
        super();
        this.cmxTranslation = this.translationService;
        try {
            const $htmlElement = document.getElementsByTagName('html');
            if (this.cmxTranslation.selectedLanguage.rtl) {
                $htmlElement[0].dir = 'rtl';
            } else {
                $htmlElement[0].dir = 'ltr';
            }
        } catch ($exception) {
            console.error($exception);
        }
    }

    public ngOnInit() {
        this.router.events.subscribe((evt) => {
            if (!(evt instanceof NavigationEnd)) {
                return;
            }
            window.scrollTo(0, 0);
        });
    }
}
