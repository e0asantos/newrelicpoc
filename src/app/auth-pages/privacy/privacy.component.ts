import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

// Providers
import { TranslationService } from '@cemex-core/angular-localization-v1/dist';
import { AgreementService, ErrorService } from '../../services';

// Utils
import { Logger } from '@cemex-core/angular-services-v2/dist';
import { DCMConstants } from '../../helpers/DCM.constants';

import { ApiError } from '../../models/';
import { LocalizationComponent } from '../../components/localization/localization.component';

@Component({
    selector: 'app-privacy',
    templateUrl: './privacy.component.html',
    styleUrls: ['./privacy.component.scss']
})

export class PrivacyComponent extends LocalizationComponent implements OnInit, OnDestroy {

    public agreementContent = '';
    private userCountryCode: string;
    private subscription: Subscription;

    constructor(
        public translationService: TranslationService,
        private agreementService: AgreementService,
        private errorService: ErrorService) {
        super();
    }

    public ngOnInit(): void {
        this.userCountryCode = sessionStorage.getItem('country') || 'US';
        this.getPublicLegalAgreement();
    }

    public ngOnDestroy(): void {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    private getPublicLegalAgreement(): void {
        this.subscription =
            this.agreementService
                .getPublicAgreementContentByCategory(this.userCountryCode, DCMConstants.AGREEMENT_CAT.PrivacyPolicy)
                .subscribe((content) => this.agreementContent = content,
                    (error) => {
                        this.errorService.handleError(error).subscribe((apiError: ApiError) => {
                            if (apiError) {
                                Logger.error('Unable to retrieve Legal agreement for country: ' + this.userCountryCode);
                            }
                        });
                    });
    }
}
