import { Component, OnInit, Input, Output, EventEmitter, ViewChild, OnChanges } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { TranslationService } from '@cemex-core/angular-localization-v1/dist';
import { WatcherService } from '@cemex/cmx-table-v1/dist/services';
import { Column, Table, CmxTableComponent } from '@cemex/cmx-table-v1/dist';
import { CustomerDetail, Jobsite } from '../../../models';
import { CmxDialogComponent } from '@cemex/cmx-dialog-v4';

@Component({
    selector: 'jobsites-table',
    templateUrl: 'jobsites-table.component.html',
    styleUrls: ['./../table.component.scss'],
    providers: [WatcherService]
})

export class JobsitesTableComponent implements OnInit, OnChanges {

    // tslint:disable-next-line:no-output-on-prefix
    @Output() public onJobsitesToggled = new EventEmitter();

    @ViewChild(CmxTableComponent) public table: CmxTableComponent;

    @ViewChild('warUnJob') public warUnJob: CmxDialogComponent;

    @Input() public templateId: string;
    @Input() public selectedJobsites: Jobsite[] = [];
    @Input() public selectedCustomers: CustomerDetail[] = [];
    @Input() public showCheckboxes = false;
    @Input() public showDelete = false;
    @Input() public isRTL = false;
    @Input() public showFilter = true;

    @Input() set jobsites(value: Jobsite[]) {
        this._jobsites.next(value);
    }

    get jobsites() {
        return this._jobsites.getValue();
    }

    public jobsiteTable = new Table();
    private _jobsites = new BehaviorSubject<any[]>([]);
    private jobsiteDelete: Jobsite;

    public selectAll = false;

    constructor(public t: TranslationService) {
        const columns: Column[] = [
            new Column('views.global.lbl_name', 'jobsiteDesc', true, true, 'jobsiteDesc', 'jobsiteDesc'),
            new Column('views.global.lbl_jobsiteCode', 'jobsiteCode', true, true, 'jobsiteCode', 'jobsiteCode'),
            new Column('views.global.lbl_region_address', 'regionDesc', true, true, 'regionDesc', 'regionDesc'),
            new Column('views.global.lbl_legalEntity', 'customerDesc', true, true, 'customerDesc', 'customerDesc')
        ];
        this.jobsiteTable.setColumns(columns);
    }

    public ngOnInit(): void {
        this._jobsites.subscribe(
            (data) => {
                /** ADD INDEX ( SCRIPTS IN QA) */
                data.forEach((e, index) => {
                    e.index = index;
                });
                this.setFilters();
            });
    }

    public ngOnChanges(): void {
        this.jobsiteTable.getColumn('regionDesc').clearFilters();
        this.jobsiteTable.getColumn('customerDesc').clearFilters();
        this._jobsites.subscribe(
            () => {
                this.setFilters();
            });
    }

    public setFilters(): void {
        this.jobsites.forEach((data) => {
            this.jobsiteTable.getColumn('regionDesc').addFilter(data);
            this.jobsiteTable.getColumn('customerDesc').addFilter(data);
        });
    }

    public selectAllJobsites(event: boolean) {
        this.selectAll = event;
        if  (event) {
            this.selectedJobsites = this.jobsites;
        } else {
            this.selectedJobsites = [];
        }
        this.onJobsitesToggled.emit(this.selectedJobsites);
    }

    public getJobsiteSelected() {
        return this.selectedJobsites;
    }

    public getActiveFilters() {
        return this.table.getActiveFiltersCopy().map(o => o.name).length > 0 ? true : false;
    }

    private isJobsiteSelected(jobsiteId: number) {
        return this.selectedJobsites.some((jobsite) => jobsite.jobsiteId === jobsiteId);
    }

    private isAllJobsitesSelected() {
        const isActiveFilter = this.getActiveFilters();
        if (isActiveFilter) {
            if (this.selectedJobsites.length > 0 ) {
                const filterJobsites = this.table.filteredItems();
                const selectedJobsites = JSON.parse(JSON.stringify(this.selectedJobsites));
                const jobsites: Jobsite[] = [];
                this.selectedJobsites = [];

                filterJobsites.forEach(element => {
                    const item =   selectedJobsites.filter((j) => j.jobsiteId === element.jobsiteId)[0];
                    if (item !== undefined) {
                        this.selectedJobsites.push(item);
                    }
                });


            }
            this.onJobsitesToggled.emit(this.selectedJobsites);
            return this.table.filteredItems().length === this.selectedJobsites.length;
        }
        return this.jobsites.length === this.selectedJobsites.length;
    }

    private toggleJobsites(event: boolean, jobsite?: Jobsite) {
        if (typeof event === 'boolean') {
            const isActiveFilter = this.getActiveFilters();
            if (event && jobsite === undefined) {
                this.selectedJobsites = isActiveFilter ? this.table.filteredItems() : this.jobsites;
            } else if (event && jobsite !== undefined) {
                this.selectedJobsites.push(jobsite);
            } else if (!event && jobsite === undefined) {
                this.selectedJobsites = [];
                this.selectAll = false;
            } else {
                this.selectedJobsites = this.selectedJobsites.filter((j) => j.jobsiteId !== jobsite.jobsiteId);
                this.selectAll = false;
            }
            this.onJobsitesToggled.emit(this.selectedJobsites);
        }
    }

    // Remove Jobsites
    private removeJobSite(element: Jobsite) {
        this.jobsiteDelete = element;
        this.warUnJob.open();
    }

    public unassignJobsite() {
        this.warUnJob.close();
        this.jobsites = this.jobsites.filter((r) => r.jobsiteId !== this.jobsiteDelete.jobsiteId);
        this.selectedJobsites = this.selectedJobsites.filter((r) => r.jobsiteId !== this.jobsiteDelete.jobsiteId);

        const jobsiteObject = {
            jobsites: [this.jobsiteDelete],
            isDelete: true
        };

        this.onJobsitesToggled.emit(jobsiteObject);
        this.refreshTable();
    }

    public refreshTable() {
        this.table.ngOnDestroy();
        this.table.ngAfterContentInit();
        this.selectedJobsites =  [];
    }

    public isSelectAllFlag() {
        return  this.selectAll;
    }

}
