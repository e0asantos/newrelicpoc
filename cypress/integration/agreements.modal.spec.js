describe('Agreements', function () {
    before(function () {
        cy.clearCookies();
        cy.clearLocalStorage();
        cy.window().then((win) => {
            win.sessionStorage.clear();
        });
    });
    beforeEach(function () {
        window.localStorage.setItem('language', 'en_US');
    });
    it('Login Page', function () {
        cy.visit('http://localhost:4200/login/');
    });
    it('Authenticate', function () {
        cy.get('#cmx-login-form-username-field').clear({force:true});
        cy.get('#cmx-login-form-username-field').type('noterms.us.qa@mailinator.com',{force:true});
        cy.get('#cmx-login-form-password-field').clear({force:true});
        cy.get('#cmx-login-form-password-field').type('Amazon123!',{force:true});
        cy.get('#cmx-login-form-submit-btn').click({ force: true });
        cy.wait(5000);
    });
    it('It is on Dashboard', function () {
        cy.url().should('include', 'dashboard');
    });
    it('Agreements modal exists', function () {
        cy.get('#cmx-agreements-modal').click({ force: true });
    });
    it('First Step: Terms of Use and continue', function () {
        cy.get('#cmx-agreements-modal-terms-step').click({ force: true });
        cy.get('#cmx-agreements-modal-next-btn').click({ force: true });
        cy.wait(100);
    });
    it('Second step: Privacy, click on Allow info and Acceptance checkboxes ', function () {
        cy.get('#cmx-agreements-modal-privacy-step').click({ force: true });
        cy.get('#cmx-agreements-modal-alloInfo-chkbox').click({ force: true });
        cy.get('#cmx-agreements-modal-agree-chkbox').click({ force: true });
    });
    it('Accept Agreements', function () {
        cy.get('#cmx-agreements-modal-agree-btn').click({ force: true });
    });
    it('It is on last step: Summary ', function () {
        cy.get('#cmx-agreements-modal-summary-step').click({ force: true });
    });
    it('Close modal to redirect to Login', function () {
        cy.get('body > app-root > app-pages-auth > div > div > app-dashboard > cmx-dialog:nth-child(2) > div:nth-child(2) > button > span:nth-child(1) ').should('have.class', 'cmx-icon-close');
        cy.get('body > app-root > app-pages-auth > div > div > app-dashboard > cmx-dialog:nth-child(2) > div:nth-child(2) > button > span:nth-child(1) ').click({ force: true });
        cy.wait(1000);
    });
    it('It is on Login page', function () {
        cy.url().should('include', 'login');
    });
});