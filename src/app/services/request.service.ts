import { HttpModule, RequestOptions, Headers, Response } from '@angular/http';
import { Injectable, Inject, Optional } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';

// Providers
import { HttpCemex, ProjectSettings } from '@cemex-core/angular-services-v2/dist';

// Models
import { Request } from '../models';

@Injectable()
export class RequestService {

    private options = new RequestOptions();

    constructor(private http: HttpCemex, private projectEnv: ProjectSettings) {
    }

    public getAllRequests(status?: string): Observable<Request[]> {
        return this.http.get(this.projectEnv.getBaseOrgEnvPath() + 'v5/secm/requests?include=' + status)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }

    public getRequestsByStatus(status: string): Observable<Request[]> {
        return this.http.get(this.projectEnv.getBaseOrgEnvPath() + 'v5/secm/requests?include=' + status)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }

    public getRequestsForUser(userId: number): Observable<Request[]> {
        return this.http.get(this.projectEnv.getBaseOrgEnvPath() + 'v5/secm/requests/user/' + userId)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }

    public submitPermissionRequest(request: any): Observable<Response> {
        return this.http.post(this.projectEnv.getBaseOrgEnvPath() + 'v5/secm/request/accessrequest', request)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }

    public submitDataAccessRequest(request: any): Observable<Response> {
        return this.http.post(this.projectEnv.getBaseOrgEnvPath() + 'v5/secm/request/datarequest', request)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }

    public authorizeRequest(requestId: number) {
        return this.http.patch(this.projectEnv.getBaseOrgEnvPath() + 'v5/secm/request/authorize/' + requestId, null)
            .map((res: Response) => {
                try {
                    res.json();
                } catch (error) {
                    Observable.of(null);
                }
            })
            .catch(this.handleError);
    }

    public rejectRequest(requestId: number, notes: string) {
        const body = {
            notes
        };
        return this.http.patch(this.projectEnv.getBaseOrgEnvPath() + 'v5/secm/request/reject/' + requestId, body)
            .map((res: Response) => {
                try {
                    res.json();
                } catch (error) {
                    Observable.of(null);
                }
            })
            .catch(this.handleError);
    }

    public readRequest(requestId: number) {
        return this.http.patch(this.projectEnv.getBaseOrgEnvPath() + 'v5/secm/request/read/' + requestId, null)
            .map((res: Response) => res.status)
            .catch(this.handleError);
    }

    public cancelRequests(requestIds: string) {
        return this.http.patch(this.projectEnv.getBaseOrgEnvPath() + 'v5/secm/request/cancel', requestIds, null)
            .map((res: Response) => res.status)
            .catch(this.handleError);
    }

    private handleError(error: Response | any) {
        return Observable.throw(error);
    }
}
