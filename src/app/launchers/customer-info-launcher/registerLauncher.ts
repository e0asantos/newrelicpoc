// tslint:disable no-string-literal

import { AppLauncher } from './types';

export default function registerLauncher(
  name: string,
  launcher: AppLauncher
): void {
  if (!window['cemexApps']) {
    window['cemexApps'] = {};
  }
  window['cemexApps'][name] = launcher;
  if (!window.top.window['cemexApps']) {
    window.top.window['cemexApps'] = {};
  }
  window.top.window['cemexApps'][name] = launcher;
}
