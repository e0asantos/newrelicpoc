import { Injectable } from '@angular/core';
import {
    Router,
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
    CanActivate
} from '@angular/router';
import { AgreementService } from '../services';
import { Observable } from 'rxjs/Observable';
import { DCMConstants } from '../helpers/DCM.constants';
import { Agreement } from '../models/agreement';

@Injectable()
export class TermsConditionsGuard implements CanActivate {

    private country: string;
    private agreementIsSigned: boolean;

    constructor(
        private router: Router,
        private agreementService: AgreementService
    ) {
        this.country = sessionStorage.getItem('country') || 'US';
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        if (state.url === '/dashboard' || state.url.indexOf('/dashboard?returnUrl') > -1) {
            return Observable.of(true);
        } else {
            return this.agreementService
                .getAgreement(this.country, DCMConstants.AGREEMENT_CAT.TermsOfUse)
                .map((agreements: Agreement[]) => {
                    const firstAgreement: Agreement = agreements[0];
                    const signed = firstAgreement ? firstAgreement.IsSigned : false;
                    if (!signed) {
                        this.router.navigate(['/dashboard'],  { queryParams: { returnUrl: state.url }});
                    }
                    return signed;
                });
        }
    }
}
