if [ -f "cypress.json" ]; then
                set -e
                npm cache clean --force
                npm install start-server-and-test
                npm install
                npm install cypress
                npm run test:cypress
                set +e
                files=`ls /opt/atlassian/pipelines/agent/build/src/cemex/cypress/videos/*.mp4`
                echo $files
                for eachfile in $files
                do
                    curl --upload-file $eachfile https://transfer.sh/$(basename $eachfile)
                done
                cd ../..
            else
                echo "had no automated tests found"
                echo "/ \__/|/ \/ ___\/ ___\/ \/ \  /|/  __/  / \ /\/ \  /|/ Y__ __\""
                echo "| |\/||| ||    \|    \| || |\ ||| |  _  | | ||| |\ ||| | / \  "
                echo "| |  ||| |\___ |\___ || || | \||| |_//  | \_/|| | \||| | | |  "
                echo "\_/  \|\_/\____/\____/\_/\_/  \|\____\  \____/\_/  \|\_/ \_/  "                                                                    
                echo "_____ _____ ____ _____ "
                echo "/__ __Y  __// ___Y__ __\""
                echo "/ \ |  \  |    \ / \  "
                echo "| | |  /_ \___ | | |  "
                echo "\_/ \____\\____/ \_/ "
                cd ../..
            fi