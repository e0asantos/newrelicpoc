import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RecaptchaModule } from 'ng-recaptcha';

// CMX Dependencies
import { CmxFormFieldModule } from '@cemex/cmx-form-field-v4';
import { CmxButtonModule } from '@cemex/cmx-button-v4';
import { CmxDialogModule } from '@cemex/cmx-dialog-v4';

// Internal Dependencies
import { PanelModule } from '../../components/panel/panel.module';

// Providers
import { PasswordService } from '../../services';

import { ForgotPasswordComponent } from './forgot-password.component';

export const ROUTES: Routes = [
    { path: '', component: ForgotPasswordComponent }
];

@NgModule({
    imports: [
        RouterModule.forChild(ROUTES),
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        RecaptchaModule.forRoot(),
        PanelModule,
        CmxButtonModule,
        CmxFormFieldModule,
        CmxDialogModule
    ],
    declarations: [
        ForgotPasswordComponent,
    ],
    exports: [
        ForgotPasswordComponent,
    ],
    providers: [
        PasswordService
    ]
})
export class ForgotPasswordModule { }
