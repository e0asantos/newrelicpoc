import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';

// CMX Dependencies
import { CmxDialogModule } from '@cemex/cmx-dialog-v4';
import { CmxDropdownModule } from '@cemex/cmx-dropdown-v4';
import { CmxTabsModule } from '@cemex/cmx-tabs-v4';
import { CmxButtonModule } from '@cemex/cmx-button-v4';
import { CmxFormFieldModule } from '@cemex/cmx-form-field-v4';
import { AlertModule } from '@cemex/cmx-alert-v4/dist';

// Internal Dependencies
import { BreadcrumbsModule } from './../../components/breadcrumbs/breadcrumbs.module';
import { SearchModule } from './../../components/search/search.module';
import { NoInfoModule } from '../../components/no-info/no-info.module';
import { ButtonsFooterModule } from '../../components/buttons-footer/buttons-footer.module';
import { TablesModule } from '../../components/tables/table.module';
import { PipesModule } from '../../pipes/pipes.module';

// Providers
import { RequestService, UserService } from '../../services';

// Utils
import { SharedFunctions } from '../../helpers/functions';

// Components
import { UserManagementComponent } from './user-management.component';
import { DomChangeDirective } from '../../directives/dom-change.directive';

export const ROUTES: Routes = [
    { path: '', redirectTo: 'inbox', pathMatch: 'full' },
    { path: 'inbox', component: UserManagementComponent },
    {
        path: 'create-user',
        loadChildren: '../create-user/create-user.module#CreateUserModule'
    },
    {
        path: 'edit-user',
        loadChildren: '../edit-user/edit-user.module#EditUserModule'
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(ROUTES),
        CommonModule,
        FlexLayoutModule,
        NoInfoModule,
        SearchModule,
        FormsModule,
        ButtonsFooterModule,
        NoInfoModule,
        TablesModule,
        CmxDropdownModule,
        CmxTabsModule,
        CmxFormFieldModule,
        CmxDialogModule,
        CmxButtonModule,
        PipesModule,
        AlertModule,
        BreadcrumbsModule
    ],
    declarations: [
        UserManagementComponent,
        DomChangeDirective
    ],
    exports: [
        UserManagementComponent,
    ],
    providers: [
        UserService,
        RequestService,
        SharedFunctions
    ]
})
export class UserManagementModule { }
