import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { PipesModule } from '../../pipes/pipes.module';
import { TermsComponent } from './terms.component';

export const ROUTES: Routes = [
    { path: '', component: TermsComponent }
];

@NgModule({
    imports: [
        RouterModule.forChild(ROUTES),
        CommonModule,
        PipesModule
    ],
    declarations: [
        TermsComponent
    ],
    exports: [
        TermsComponent
    ]
})
export class TermsModule { }
