import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Providers
import { PasswordService } from '../../services/';

// CMX Dependencies
import { CmxDialogModule } from '@cemex/cmx-dialog-v4';
import { CmxFormFieldModule } from '@cemex/cmx-form-field-v4';
import { CmxButtonModule } from '@cemex/cmx-button-v4/dist';

// Internal Dependencies
import { PasswordTooltipModule } from './../../components/password-tooltip/password-tooltip.module';
import { BreadcrumbsModule } from '../../components/breadcrumbs/breadcrumbs.module';

import { SelfResetPasswordComponent } from './self-reset-password.component';

export const ROUTES: Routes = [
    { path: '', component: SelfResetPasswordComponent }
];

@NgModule({
    imports: [
        RouterModule.forChild(ROUTES),
        FormsModule,
        CmxFormFieldModule,
        ReactiveFormsModule,
        CommonModule,
        CmxDialogModule,
        CmxButtonModule,
        PasswordTooltipModule,
        BreadcrumbsModule
    ],
    declarations: [
        SelfResetPasswordComponent,
    ],
    exports: [
        SelfResetPasswordComponent,
    ],
    providers: [PasswordService]
})

export class SelfResetPasswordModule { }
