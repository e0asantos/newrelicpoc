#!/bin/sh -xe
apt-get update
git submodule update --init --recursive
# to clean publish folder if job is failing
if [ -d src/cemex/publish/ ]; then rm -Rf src/cemex/publish/; fi
cd src/cemex/submodules/dls
git checkout deployable        
cd ../../ClientApp

npm install
npm run build:prod
npm i gulp -g
gulp css
gulp minify
cd ..
dotnet publish --output publish --configuration release
pwd 