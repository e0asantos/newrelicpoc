import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';

// CMX Dependencies
import { CmxTableModule } from '@cemex/cmx-table-v1';
import { CmxCheckboxModule } from '@cemex/cmx-checkbox-v4';
import { CmxButtonModule } from '@cemex/cmx-button-v4';
import { CmxDropdownModule } from '@cemex/cmx-dropdown-v4/dist';
import { CmxDialogModule } from '@cemex/cmx-dialog-v4';
import { CmxTooltipModule } from '@cemex/cmx-tooltip-v4';

// Internal Dependencies
import { PipesModule } from '../../pipes/pipes.module';

// Components
import { JobsitesTableComponent } from './jobsites-table/jobsites-table.component';
import { CustomersTableComponent } from './customers-table/customers-table.component';
import { ActionLogTableComponent } from './action-log-table/action-log-table.component';
import { UsersTableComponent } from './users-table/users-table.component';
import { RequestTableComponent } from './requests-table/requests-table.component';

@NgModule({
    imports: [
        CommonModule,
        FlexLayoutModule,
        CmxTableModule,
        CmxCheckboxModule,
        PipesModule,
        CmxDropdownModule,
        CmxButtonModule,
        CmxDialogModule,
        CmxTooltipModule
    ],
    exports: [
        CustomersTableComponent,
        JobsitesTableComponent,
        RequestTableComponent,
        UsersTableComponent,
        ActionLogTableComponent
    ],
    declarations: [
        CustomersTableComponent,
        JobsitesTableComponent,
        RequestTableComponent,
        UsersTableComponent,
        ActionLogTableComponent
    ]
})

export class TablesModule { }
