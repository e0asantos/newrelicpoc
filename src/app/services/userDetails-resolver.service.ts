import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';

// Providers
import { UserService } from './user.service';

@Injectable()
export class UserDetailsResolver implements Resolve<any> {

    constructor(private userService: UserService) { }

    public resolve(route: ActivatedRouteSnapshot) {

        return this.userService.getUserDetails(route.params['id']);
    }

    private handleError(error: any) {
        return Observable.throw(error || 'Server error');
    }
}
