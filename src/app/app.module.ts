import { NgModule, Injector } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule, XHRBackend, Http, RequestOptions } from '@angular/http';
import { InterceptedHttp } from './interceptors/cmx-http.interceptor';

// Providers
import { CountlyService } from '@cemex-core/helpers-v1/dist';
import { ErrorService, AgreementService } from './services';
import {
    FeatureToggleService,
    HttpCemex,
    ProjectSettings,
    SessionService,
    CountryConfigService
} from '@cemex-core/angular-services-v2/dist';
import { TranslationService, LocaleService, FormatterService } from '@cemex-core/angular-localization-v1/dist/';
import { Broadcaster } from '@cemex-core/events-v1/dist/';

// CMX Dependencies
import { CmxLoginModule } from '@cemex/cmx-login-v3/dist';
import { CmxDropdownModule } from '@cemex/cmx-dropdown-v4';
import { CmxButtonModule } from '@cemex/cmx-button-v4/dist';
import { AlertModule } from '@cemex/cmx-alert-v4/dist';

// Internal Dependencies
import { PermissionModule } from './components/permission/permission.module';
import { AuthPagesModule } from './auth-pages/auth-pages.module';

// Components
import { LoginComponent } from './pages/login/login.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { AppComponent } from './app.component';

import { AppRoutingModule } from './app.routes';

export const language: any = window['LANGUAGE'];
export const cmx_language: any = window['CMX_LANGUAGES'];

@NgModule({
    bootstrap: [AppComponent],
    declarations: [
        AppComponent,
        LoginComponent,
        NotFoundComponent
    ],
    imports: [
        BrowserModule,
        HttpModule,
        FlexLayoutModule,
        FormsModule,
        ReactiveFormsModule,
        PermissionModule,
        AlertModule,
        CmxDropdownModule,
        CmxLoginModule,
        CmxButtonModule,
        AppRoutingModule,
        AuthPagesModule
    ],
    providers: [
        { provide: 'ORIGIN_URL', useValue: location.origin },
        ErrorService,
        CountlyService,
        HttpCemex,
        ProjectSettings,
        SessionService,
        FeatureToggleService,
        TranslationService,
        LocaleService,
        FormatterService,
        CountryConfigService,
        AgreementService,
        Broadcaster,
        {
            provide: Http,
            useClass: InterceptedHttp,
            deps: [XHRBackend, RequestOptions, Injector, ErrorService]
        },
        { provide: 'USE_TRANSLATION_SERVER', useValue: true },
        { provide: 'USE_LOCAL_MANIFEST', useValue: true },
        { provide: 'USE_RTL', useValue: false },
        { provide: 'PRODUCT_PATH', useValue: '/user-provisioning/' },
        { provide: 'TRANSLATION_PRODUCT_PATH', useValue: 'user-provisioning' },
        { provide: 'DEFAULT_LANGUAGE_ISO', useValue: language },
        { provide: 'TRANSLATION_LANGUAGES', useValue: cmx_language },
    ]
})

export class AppModule { }

