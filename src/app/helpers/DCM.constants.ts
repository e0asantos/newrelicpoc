export class DCMConstants {
    public static DCM_APPLICATIONS: string[] =
        [
            'CustomerInfo_App',
            'QuotationPricing_App',
            'INVOICE_PAYMENTS',
            'OrderProductCat_App',
            'Foreman_App',
            'DeliverySchedule_App',
            'UserProvisioning_App'
        ];
    public static DCM_APPLICATION_CODE = 'UserProvisioning_App';
    public static DCM_LOYALTY_PROGRAM_CODE = 'CMX_LOYALTY_PROG';
    public static CMX_LOYALTY_PROGRAM_NAME = 'CEMEX al Punto';
    public static DCM_ROLE_CODE = 'DCM_USER';
    public static DCM_USER_MANAGEMENT_APP_CODE = 'UserProvisioning_App';
    public static DCM_SUPER_USER_CODE = 'SECMORGROOT';
    public static GOOGLE_RECAPTCHA_KEY = '6LdhExwUAAAAAJPyJayFlhzl3C54hjUn2YIy6-1S';
    public static REGULAR_EXPRESSIONS = {
        OnlyText: '[a-zA-ZñÑáéíóúÁÉÍÓÚ]+',
        OnlyTextWithSpaces: '^[a-zA-ZñÑáéíóúÁÉÍÓÚ0-9_]+( [a-zA-ZñÑáéíóúÁÉÍÓÚ0-9_]+)*$',
        NoSpecialChars: '[^±!@£$%^&*_+§¡!€#¢§¶¬"°•ªº{}()«\\/=<>?¿:;]*',
        PhoneNumber: '[+]?([0-9]*[\\.\\s\\-\\(\\)]|[0-9]+){8,24}',
        Password: '[^+\\\\]*'
    };

    public static ADD_COUNTLY_TRACKING = 'ADD_COUNTLY_TRACKING';
    public static OIM_APP_KEY = 'CXGODIGITALADMIN';
    public static PRIVACY_URL_CO = 'https://www.cemexcolombia.com/legal';
    public static PRIVACY_URL_DE = 'http://www.cemex.de/Datenschutzrichtlinien.aspx';
    public static PRIVACY_URL_PA = 'http://www.cemexpanama.com/PoliticasDePrivacidad.aspx';
    public static PRIVACY_URL_NI = 'https://www.cemexnicaragua.com/legal';
    public static PRIVACY_URL_FR = 'http://www.cemex.fr/ProtectiondesDonnees.aspx';
    public static PRIVACY_URL_CR = 'http://www.cemexcostarica.com/PoliticasDePrivacidad.aspx';
    public static PRIVACY_URL = 'https://www.cemex.com/privacy-policy';

    public static USER_TYPE_CAT = {
        Customer: 'C',
        Employee: 'E',
    };

    public static AGREEMENT_CAT = {
        Legal: 'LGL',
        TermsOfUse: 'TOU',
        PrivacyPolicy: 'PRP',
        GDPR: 'GDPR',
        MKT: 'MKT'
    };
}
