import { Component, ViewChild, Input, Output, EventEmitter} from '@angular/core';
import { TranslationService } from '@cemex-core/angular-localization-v1/dist/';
import { Jobsite } from '../../../models/index';
import { CmxDialogComponent } from '@cemex/cmx-dialog-v4/dist';
import { SharedFunctions } from '../../../helpers/functions/';
import { JobsitesTableComponent } from '../../tables/jobsites-table/jobsites-table.component';
import { SearchComponent } from '../../../components/search/search.component';

@Component({
  selector: 'jobsites-modal',
  templateUrl: './jobsites.modal.html',
  styleUrls: ['./jobsites.modal.scss']
})

export class JobsitesModalComponent  {

  // tslint:disable-next-line:no-output-on-prefix
  @Output() public onAssign = new EventEmitter();
  @Input() public templateId: string;
  @Input() public isRTL = false;
  @Input() public isRequest = false;
  @ViewChild('jobsite') public jobsiteModal: CmxDialogComponent;
  @ViewChild('jobsiteTable') public jobsiteTable: JobsitesTableComponent;
  @ViewChild('customerJob') public customerJob: CmxDialogComponent;
  @ViewChild('searchModalJobsites') public searchModalJobsites: SearchComponent;

  public selectallIsChecked = false;
  public selectedJobsites: Jobsite[] = [];
  public filteredJobsites: Jobsite[] = [];
  public josbites: Jobsite[] = [];
  public selectedJobsitesLabel = '';
  public searchTerm: string;

  constructor(
    public sharedFunctions: SharedFunctions,
    public t: TranslationService
  ) { }

  public init(inputModal) {
    const input = JSON.parse(JSON.stringify(inputModal));
    this.josbites = input.jobsites;
    this.filteredJobsites = input.filteredJobsites;
    this.selectedJobsites = input.selectedJobsites;
    this.selectallIsChecked = input.selectAll;
    this.updateSelectedLabel(this.selectallIsChecked);
    this.jobsiteModal.open();
  }

  public close() {
    this.resetModal();
    this.jobsiteModal.closeAllDialogs();
  }

  public resetModal() {
    this.selectallIsChecked = false;
    this.selectedJobsites = [];
    this.filteredJobsites = [];
    this.josbites = [];
    this.selectedJobsitesLabel = '';
    this.searchModalJobsites.searchTerm = '';
    this.searchTerm = '';
  }
  public toggleSelectall(event: boolean) {
    this.selectallIsChecked = event;
    if (this.filteredJobsites.length > 0) {
      this.jobsiteTable.selectAllJobsites(event);
      this.updateSelectedLabel();
    }
  }

  public updateSearchTerm(value: string): void {
    this.searchTerm = value;
    const arrayFilter = {
      jobsiteCode: this.searchTerm,
      jobsiteDesc: this.searchTerm,
      customerCode: this.searchTerm,
      customerDesc: this.searchTerm
    };

    this.filteredJobsites = this.sharedFunctions.filterData(this.josbites, arrayFilter);
    this.updateSelectedLabel();
  }


  public updateSelectedJobsites(event): void {
    this.selectedJobsites = event;
    this.selectallIsChecked = this.jobsiteTable.isSelectAllFlag();
    this.updateSelectedLabel();
  }

  public updateSelectedLabel(selectAll?: boolean) {
    this.selectedJobsites = this.selectedJobsites === undefined ? [] : this.selectedJobsites;
    this.selectedJobsitesLabel = selectAll ?
    this.t.pt('views.createuser.selected_jobsites').
      replace('{f}', this.josbites.length.toString()).
      replace('{n}', this.josbites.length.toString())
      :
      this.t.pt('views.createuser.selected_jobsites').
      replace('{f}', this.selectedJobsites.length.toString()).
      replace('{n}', this.josbites.length.toString());
  }

  public assign() {
    if (this.selectallIsChecked) {
      this.jobsiteModal.hide();
      this.customerJob.open();
    } else {
      const evenEmiit = {
        jobsites: this.selectedJobsites,
        checkAllJobsites : this.selectallIsChecked
      };
      this.onAssign.emit(evenEmiit);
      this.resetModal();
      this.jobsiteModal.closeAllDialogs();
    }
  }

  public cancelWarning() {
    this.customerJob.close(true);
  }

  public continueWarning() {

    const evenEmiit = {
      jobsites: this.selectedJobsites,
      checkAllJobsites : this.selectallIsChecked
    };

    this.onAssign.emit(evenEmiit);
    this.resetModal();
    this.jobsiteModal.closeAllDialogs();
    this.customerJob.closeAllDialogs();
  }

  public requestJobsites() {
    this.onAssign.emit(this.selectedJobsites);
    this.resetModal();
    this.jobsiteModal.closeAllDialogs();
    this.customerJob.closeAllDialogs();
  }

}
