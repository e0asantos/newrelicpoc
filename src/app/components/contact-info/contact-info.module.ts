import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContactInfoComponent } from './contact-info.component';

@NgModule({
    imports: [
        CommonModule
    ],
    exports: [
        ContactInfoComponent
    ],
    declarations: [
        ContactInfoComponent
    ],
})

export class ContactInfoModule { }
