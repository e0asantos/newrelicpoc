describe('Login Screen', function () {
    it('starts the application', function () {
        cy.clearCookies();
        cy.clearLocalStorage();
        cy.window().then((win) => {
            win.sessionStorage.clear();
            win.localStorage.setItem('language', 'de_DE');
        });
        cy.visit('http://localhost:4200/login/');
    });
    it('should contain "Deutsch" in language dropdown', function () {
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(1) > cmx-dropdown > div > div > span ').contains('Deutsch');
    });
    it('should contain "Herzlich willkommen" for welcome message.', function(){
        cy.get('.login__content-title').contains('Herzlich willkommen');
        cy.get('.login__content-title').should('have.class', 'login__content-title');
        cy.get('.login__content-title').click({force:true});
        });
    it('should contain "Benutzername" in the username form label', function () {
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(2) > cmx-login > div > div:nth-child(2) > form > cmx-form-field:nth-child(1) > label ').contains('Benutzername');
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(2) > cmx-login > div > div:nth-child(2) > form > cmx-form-field:nth-child(1) > label ').click({ force: true });
    });
    it('should contain "Benutzernamen eingeben" on username input placeholder', function () {
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(2) > cmx-login > div > div:nth-child(2) > form > cmx-form-field:nth-child(1) > input ').should('have.attr', 'placeholder', 'Benutzernamen eingeben');
    });
    it('should contain "Passwort" in the password form label', function () {
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(2) > cmx-login > div > div:nth-child(2) > form > cmx-form-field:nth-child(3) > label ').contains('Passwort');
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(2) > cmx-login > div > div:nth-child(2) > form > cmx-form-field:nth-child(3) > label ').click({ force: true });
    });
    it('should contain "Passwort eingeben" on password input placeholder', function () {
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(2) > cmx-login > div > div:nth-child(2) > form > cmx-form-field:nth-child(3) > input ').should('have.attr', 'placeholder', 'Passwort eingeben');
    });
    it('should contain link text "Passwort vergessen?" to retrieve users password', function () {
        cy.get('[data-tid="cmx-login-form-forgot-password-link"]').contains('Passwort vergessen?');
        cy.get('[data-tid="cmx-login-form-forgot-password-link"]').should('have.class', 'cmx-button');
        cy.get('[data-tid="cmx-login-form-forgot-password-link"]').should('have.class', 'cmx-button--link');
    });
    it('should contain "Anmelden" as submit button', function () {
        cy.get('[data-tid="cmx-login-form-submit-btn"]').contains('Anmelden');
        cy.get('[data-tid="cmx-login-form-submit-btn"]').should('have.class', 'cmx-button');
        cy.get('[data-tid="cmx-login-form-submit-btn"]').should('have.class', 'cmx-ripple');
    });
});

describe('FORGOT PASSWORD SCREEN', function () {
    it('starts the application', function () {
        cy.window().then((win) => {
            win.localStorage.setItem('language', 'de_DE');
        });
        cy.visit('http://localhost:4200/forgotPassword/');
    });
    it('should contain "Passwort vergessen?" as page title', function () {
        cy.get('.custom-panel__content__title').contains('Passwort vergessen?');
        cy.get('.custom-panel__content__title').should('have.class', 'custom-panel__content__title');
        cy.get('.custom-panel__content__title').click({ force: true });
    });
    it('should contain correct text as page description', function () {
        cy.get('.custom-panel__content__instruction').contains('Geben Sie Ihre E-Mail-Adresse ein und wir senden Ihnen einen Link, um Ihr Passwort neu einzustellen.');
        cy.get('.custom-panel__content__instruction').should('have.class', 'custom-panel__content__instruction');
        cy.get('.custom-panel__content__instruction').click({ force: true });
    });
    it('should contain "E-mail" for email label', function () {
        cy.get('body > app > forgot-password > div > panel > div > div:nth-child(3) > form > cmx-form-field > label ').contains('E-mail');
        cy.get('body > app > forgot-password > div > panel > div > div:nth-child(3) > form > cmx-form-field > label ').click({ force: true });
    });
    it('should contain "E-Mail eingeben" for email input placeholder', function () {
        cy.get('body > app > forgot-password > div > panel > div > div:nth-child(3) > form > cmx-form-field > input ').should('have.attr', 'placeholder', 'E-Mail eingeben');
    });
    it('should contain correct error message for invalid email', function () {
        cy.get('#cmx-forgot-password-email-input').clear();
        cy.get('#cmx-forgot-password-email-input').type('foo');
        cy.get('body > app > forgot-password > div > panel > div > div:nth-child(3) > form > cmx-form-field > span > error-message ').contains('Ungültiges E-Mail-Format');
    });
    it('should contain "E-Mail ist erforderlich" when no text is in the input', function () {
        cy.get('#cmx-forgot-password-email-input').clear();
        cy.get('body > app > forgot-password > div > panel > div > div:nth-child(3) > form > cmx-form-field > span > error-message ').contains('E-Mail ist erforderlich');
    });
    it('should contain "Senden" as sumbit button', function () {
        cy.get('#cmx-forgot-password-submit-btn').contains('Senden');
    });
});

function login(window) {
    cy.request({
        method: 'POST',
        url: 'https://cemexqas.azure-api.net/v2/secm/oam/oauth2/token',
        form: true,
        body: {
            grant_type: 'password',
            scope: 'security',
            username: 'da.us.qa@yopmail.com',
            password: 'Amazon123!'
        }
    })
    .then(resp => {
        window.sessionStorage.setItem('access_token', resp.body.oauth2.access_token);
        window.sessionStorage.setItem('applications', JSON.stringify(resp.body.applications));
        window.sessionStorage.setItem('auth_token', resp.body.oauth2.access_token);
        window.sessionStorage.setItem('country', resp.body.country);
        window.sessionStorage.setItem('expires_in', resp.body.oauth2.expires_in);
        window.sessionStorage.setItem('jwt', resp.body.jwt);
        window.sessionStorage.setItem('language', `en_${resp.body.country}`);
        window.sessionStorage.setItem('refresh_token', resp.body.oauth2.refresh_token);
        window.sessionStorage.setItem('region', resp.body.oauth2.region);
        window.sessionStorage.setItem('role', resp.body.role);
        window.sessionStorage.setItem('token_data', JSON.stringify(resp.body));
        window.sessionStorage.setItem('userInfo', JSON.stringify(resp.body));
        window.sessionStorage.setItem('user_applications', JSON.stringify(resp.body.applications));
        window.sessionStorage.setItem('user_customer', JSON.stringify(resp.body.customer));
        window.sessionStorage.setItem('user_profile', JSON.stringify(resp.body.profile));
        window.sessionStorage.setItem('username', resp.body.profile.userAccount);
    });
}

describe('USER MANAGEMENT SCREEN', function () {
    it('starts the application', function () {
        cy.window().then((win) => {
            win.localStorage.setItem('language', 'de_DE');
            login(win);
        });
        cy.visit('http://localhost:4200/user-management/inbox/');
    });
    it('should contain "Benutzerverwaltung" as last breadcrumb item', function () {
        cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-breadcrumb > ul > li:nth-child(2) ').contains('Benutzerverwaltung');
    });
    it('should contain "Benutzerverwaltung" as page title', function () {
        cy.get('body > app > app-pages > div > div > div > user-management > div > h1 ').contains('Benutzerverwaltung');
    });
    it('should contain "Anfragen in Bearbeitung" for pending requests.', function () {
        cy.get('#cmx-user-pending-request-USER-MANAFEMENT').contains('Anfragen in Bearbeitung');
    });
    it('should contain "Gesamte Benutzer" for total users', function () {
        cy.get('body > app > app-pages > div > div > div > user-management > div > div:nth-child(4) > div > div:nth-child(2) > div > div:nth-child(2) > div:nth-child(2) ').contains('Gesamte Benutzer');
    });
    it('should contain "Posteingang" as first page tab', function () {
        cy.get('ul.nav-tabs > li:nth-child(1)').contains('Posteingang');
    });
    it('should contain "Benutzerverwaltung" as second page tab', function () {
        cy.get('ul.nav-tabs > li:nth-child(2)').contains('Benutzerverwaltung');
    });

    describe('Inbox Section', function() {
        it('should contain "Aktive Anfragen" for active requests filter option', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(2) > div:nth-child(1) > cmx-dropdown > div > div > span ').click({force:true});
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(2) > div:nth-child(1) > cmx-dropdown > div > div:nth-child(2) > div > cmx-dropdown-item:nth-child(1) > div').contains('Aktive Anfragen');
        });
        it('should contain "Genehmigt" for approved filter option', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(2) > div:nth-child(1) > cmx-dropdown > div > div > span ').click({force:true});
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(2) > div:nth-child(1) > cmx-dropdown > div > div:nth-child(2) > div > cmx-dropdown-item:nth-child(2) > div').contains('Genehmigt');
        });
        it('should contain "Abgelehnt" for rejected filter option', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(2) > div:nth-child(1) > cmx-dropdown > div > div > span ').click({force:true});
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(2) > div:nth-child(1) > cmx-dropdown > div > div:nth-child(2) > div > cmx-dropdown-item:nth-child(3) > div').contains('Abgelehnt');
        });
        it('should contain "Suche nach Benutzer oder Firma" on filter input placeholder', function () {
            cy.get('#cmx-input-search-request-USER-MANAGEMENT').should('have.attr', 'placeholder', 'Suche nach Benutzer oder Firma');
        });
        it('should contain "Status" as first table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(4) > requests-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(1) > div > div ').contains('Status');
        });
        it('should contain "ID anfordern" as second table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(4) > requests-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(2) > div > div ').contains('ID anfordern');
        });
        it('should contain "Anfragetyp" as third table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(4) > requests-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(3) > div > div ').contains('Anfragetyp');
        });
        it('should contain "Angefordert von" as fourth table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(4) > requests-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(4) > div > div ').contains('Angefordert von');
        });
        it('should contain "Firma" as fifth table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(4) > requests-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(5) > div > div ').contains('Firma');
        });
        it('should contain "Anfragedatum" as third table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(4) > requests-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(6) > div > div ').contains('Anfragedatum');
        });
        it('should contain "Daten Request" as title when opening a request detail', function () {
            cy.get('#cmx-btn-request-detail-0-USER-MANAGEMENT').click({ force: true });
            cy.get('body > app > app-pages > div > div > div > user-management > cmx-dialog:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > cmx-dialog-body > div:nth-child(1) > div:nth-child(1) > h2 ').contains('Daten Request');
        });
        it('should contains "Erstelltes Datum:" for request created date.', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > cmx-dialog:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > cmx-dialog-body > div:nth-child(1) > div:nth-child(2) > div > div:nth-child(1) > h6 ').contains('Erstelltes Datum:');
        });
        it('should contains "ID Anfordern:" for request id.', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > cmx-dialog:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > cmx-dialog-body > div:nth-child(1) > div:nth-child(2) > div > div:nth-child(2) > h6 ').contains('ID anfordern');
        });
        it('should contain "Vorname" as first table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > cmx-dialog:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > cmx-dialog-body > div:nth-child(5) > table > thead > tr > th:nth-child(1) ').contains('Vorname');
        });
        it('should contain "Baustellen-Nr." as second table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > cmx-dialog:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > cmx-dialog-body > div:nth-child(5) > table > thead > tr > th:nth-child(2) ').contains('Baustellen-Nr.');
        });
        it('should contain "Ablehnen" for reject button', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > cmx-dialog:nth-child(2) > div:nth-child(2) > div:nth-child(3) > div > cmx-dialog-footer > div > button:nth-child(1) ').contains('Ablehnen');
        });
        it('should contain "Genehmigen" for approve button', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > cmx-dialog:nth-child(2) > div:nth-child(2) > div:nth-child(3) > div > cmx-dialog-footer > div > button:nth-child(2) ').contains('Genehmigen');
        });
    })

    describe('Main Section', function() {
        it('should contain "Suche nach Benutzer, Firma oder E-Mail" on filter input placeholder', function () {
            cy.get('div#1.nav-link').click({force: true});
            cy.get('#cmx-input-search-user-USER-MANAGEMENT').should('have.attr', 'placeholder', 'Suche nach Benutzer, Firma oder E-Mail');
        });
        it('should contain "Benutzername" as first table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(2) > div:nth-child(4) > users-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(1) > div > div ').contains('Benutzername');
        });
        it('should contain "Firma" as second table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(2) > div:nth-child(4) > users-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(2) > div > div ').contains('Firma');
        });
        it('should contain "E-mail" as third table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(2) > div:nth-child(4) > users-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(3) > div > div ').contains('E-mail');
        });
        it('should contain "Telefonnummer" as fourth table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(2) > div:nth-child(4) > users-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(4) > div > div ').contains('Telefonnummer');
        });
        it('should contain "Zugang zu Apps" as fifth table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(2) > div:nth-child(4) > users-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(5) > div > div ').contains('Zugang zu Apps');
        });
        it('should contain "Neuen Benutzer erstellen" for user creation button.', function () {
            cy.get('#cmx-btn-go-create-user-USER-MANAGEMENT').contains('Neuen Benutzer erstellen');
        });
    });

});

describe('EDIT USER SCREEN', function () {
    it('starts the application', function () {
        cy.window().then((win) => {
            win.localStorage.setItem('language', 'de_DE');
        });
        cy.visit('http://localhost:4200/user-management/edit-user?id=omegaprodinterclerk@mailinator.com/');
    });
    it('should contain "Benutzerprofil" as last breadcrumb item.', function () {
        cy.get('body > app > app-pages > div > div > div > edit-user > div:nth-child(1) > cmx-breadcrumb > ul > li:nth-child(3) ').contains('Benutzerprofil');
    });
    it('should contain "Benutzerprofil" as page title', function () {
        cy.get('#cmx-edituser-pagetitle-lbl').contains('Benutzerprofil');
    });
    it('should contain "Telefonnummer" as user phone number', function () {
        cy.get('#cmx-profile-box-phonenumber-lbl').contains('Telefonnummer');
    });
    it('should contain "E-mail" as user email', function () {
        cy.get('#cmx-profile-box-email-lbl').contains('E-mail');
    });
    it('should contain "Firma" as user company', function () {
        cy.get('#cmx-profile-box-company-lbl').contains('Firma');
    });
    it('should contain "Land" as user company', function () {
        cy.get('#cmx-profile-box-country-lbl').contains('Land');
    });
    it('should contain "Kundennummer" for custumer number.', function () {
        cy.get('#cmx-profile-box-customer-code-title-EDIT-USER').contains('Kundennummer');
    });
    it('should contain "BENUTZER DEAKTIVIEREN" for disable user button (NOT WORKING)', function () {
        cy.get('body > app > app-pages > div > div > div > edit-user > div:nth-child(1) > profile-box > div > div:nth-child(3) > div:nth-child(2) > div > a:nth-child(1) > span ').contains('Benutzer deaktivieren');
    });
    it('should contain "AKTIONSPROTOKOLL" for action log button', function () {
        cy.get('body > app > app-pages > div > div > div > edit-user > div:nth-child(1) > profile-box > div > div:nth-child(3) > div:nth-child(2) > div > a:nth-child(2) > span ').contains('Aktionsprotokoll');
    });
    it('should contain "PASSWORT NEU EINSTELLEN" for reset password button', function () {
        cy.get('body > app > app-pages > div > div > div > edit-user > div:nth-child(1) > profile-box > div > div:nth-child(3) > div:nth-child(2) > div > a:nth-child(3) > span ').contains('Passwort neu einstellen');
    });
    it('should contain "Zugang zu Anwendungen" for first page tab', function () {
        cy.get('#0').contains('Zugang zu Anwendungen');
    });
    it('should contain "Informationen" for second page tab', function () {
        cy.get('#1').contains('Informationen');
        cy.get('#1').click({ force: true });
    });
    it('should contain "Zugriffe zuweisen" for assign accesses button', function () {
        cy.get('#cmx-btn-data-access-EDIT-USER').contains('Zugriffe zuweisen');
    });

});

describe('CREATE USER SCREEN', function () {
    it('starts the application', function () {
        cy.window().then((win) => {
            win.localStorage.setItem('language', 'de_DE');
        });
        cy.visit('http://localhost:4200/user-management/create-user/');
    });

    it('should contain "Neuen Benutzer erstellen" in last breadcrumb item', function () {
        cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-breadcrumb > ul > li:nth-child(3) ').contains('Neuen Benutzer erstellen');
    });
    it('should contain "Kontaktinformationen" as first process step', function () {
        cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > ul > li:nth-child(1) ').contains('Kontaktinformationen');
    });
    it('should contain "Anwendungen und Positionen zuweisen" as second process step', function () {
        cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > ul > li:nth-child(2) ').contains('Anwendungen und Positionen zuweisen');
    });
    it('should contain "Daten zuweisen" as third process step', function () {
        cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > ul > li:nth-child(3) ').contains('Daten zuweisen');
    });
    it('should contain "Zusammenfassung" as fourth process step  ', function () {
        cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > ul > li:nth-child(4) ').contains('Zusammenfassung');
    });

    describe('Contact Information Section', function() {
        it('should contain "Kundennummer" as first form input label and "Kundennummer wählen" as PH', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(3) > div:nth-child(1) > div > cmx-dropdown > div > label ').contains('Kundennummer');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(3) > div:nth-child(1) > div > cmx-dropdown > div > div > span ').contains('Kundennummer wählen');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(3) > div:nth-child(1) > div > cmx-dropdown > div > div > span ').click({ force: true });
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(3) > div:nth-child(1) > div > cmx-dropdown > div > div:nth-child(3) > div:nth-child(1) > div > input ').should('have.attr', 'placeholder', 'Kundennummer wählen');
            cy.get('#cmx-dropdown-item-customer--CREATE-USER').click({force:true});
        });
    
        it('should contain "Firma" as second form input label', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(3) > div:nth-child(2) > cmx-form-field > label ').contains('Firma');
        });
        it('should contain "Land" as third form input label', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(3) > div:nth-child(3) > cmx-form-field > label ').contains('Land');
        });
        it('should contain "Vorname" as fourth form input label and "Namen eingeben" as input PH', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(5) > div:nth-child(1) > cmx-form-field > label ').contains('Vorname');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(5) > div:nth-child(1) > cmx-form-field > input ').should('have.attr', 'placeholder', 'Namen eingeben');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(5) > div:nth-child(1) > cmx-form-field > input ').type('foo');
        });
        it('should contain "Nachname" as fifth form input label and "Nachnamen eingeben" as input PH', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(5) > div:nth-child(2) > cmx-form-field > label ').contains('Nachname');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(5) > div:nth-child(2) > cmx-form-field > input ').should('have.attr', 'placeholder', 'Nachnamen eingeben');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(5) > div:nth-child(2) > cmx-form-field > input ').type('foo');
        });
        it('should contain "Position" as sixth form input label and "Stellung eingeben" as input PH', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(5) > div:nth-child(3) > cmx-form-field > label ').contains('Position');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(5) > div:nth-child(3) > cmx-form-field > input ').should('have.attr', 'placeholder', 'Stellung eingeben');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(5) > div:nth-child(3) > cmx-form-field > input ').type('foo');
        });
    
        it('should contain "E-mail" as seventh form input label and "<E-Mail eingeben" as input PH', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(7) > div:nth-child(1) > cmx-form-field > label ').contains('E-mail');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(7) > div:nth-child(1) > cmx-form-field > input ').should('have.attr', 'placeholder', '<E-Mail eingeben');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(7) > div:nth-child(1) > cmx-form-field > input ').type('foo@gmail.com');
        });
    
        it('should contain "Telefon" as eighth form input label and "Telefonnr. eingeben" as input PH', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(7) > div:nth-child(2) > cmx-form-field > label ').contains('Telefon');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(7) > div:nth-child(2) > cmx-form-field > input ').should('have.attr', 'placeholder', 'Telefonnr. eingeben');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(7) > div:nth-child(2) > cmx-form-field > input ').type('123');
        });
    
        it('should contain "Temporäres Passwort" as nineth input label, "Temporäres Passwort eingeben" as input PH and correct tooltip message', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(7) > div:nth-child(3) > div > label ').contains('Temporäres Passwort');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(7) > div:nth-child(3) > div > input ').should('have.attr', 'placeholder', 'Temporäres Passwort eingeben');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(7) > div:nth-child(3) > div > input ').type('foobazaO34?');
            cy.get('#cmx-tooltip-temp-password-CREATE-USER > span ').should('have.attr', 'aria-label', 'Ihr Passwort muss mindestens 8 bis 20 Zeichen lang sein. Es wird empfohlen, Großbuchstaben, Kleinbuchstaben und Zahlen anzugeben.');
        });
    
        it('should contain correct text for contact me checkbox', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(9) > div > cmx-checkbox > label ').contains('Ich möchte, dass CEMEX meine E-Mail-Adresse und mein Telefon nutzt, um mich zu kontaktieren.');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(9) > div > cmx-checkbox > label ').click({ force: true });
        });
    
        it('should contain "Weiter" for submit button', function () {
            cy.get('#cmx-btn-next-CREATE-USER').contains('Weiter');
            cy.get('#cmx-btn-next-CREATE-USER').click({force: true});
        });
    });

    describe('Assign Applications and Roles', function() {
        it('should contain "Kontaktinformationen" as page title', function () {
            cy.get('.contact-info__title').contains('Kontaktinformationen');
        });
        it('should contain "Vorname" as first column', function () {
            cy.get('#cmx-contact-info-name-lbl').contains('Vorname');
        });
        it('should contain "Position" as second column', function () {
            cy.get('#cmx-contact-info-position-lbl').contains('Position');
        });
        it('should contain "Firma" as third column', function () {
            cy.get('#cmx-contact-info-company-lbl').contains('Firma');
        });
        it('should contain "Land" as fourth column', function () {
            cy.get('#cmx-contact-info-country-lbl').contains('Land');
        });
        it('should contain "E-mail" as fifth column', function () {
            cy.get('#cmx-contact-info-email-lbl').contains('E-mail');
        });
        it('should contain "Telefonnummer" as sixth column', function () {
            cy.get('#cmx-contact-info-phone-lbl').contains('Telefonnummer');
        });
        it('should contain "Erlaubnis zuweisen" as assign perm button', function () {
            cy.get('#cmx-btn-add-permission-CREATE-USER').contains('Erlaubnis zuweisen');
            cy.get('#cmx-btn-add-permission-CREATE-USER').click({ force: true });
        });
        it('should contain "Anwendung auswählen" as default perm option key', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(2) > cmx-select > div:nth-child(2) ').contains('Anwendung auswählen');
        });
        it('should contain "Positionen auswählen" as default perm option value', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').contains('Positionen auswählen');
        });

        it('should contain "Track" for track key and its corresponding roles', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(2) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('#cmx-item-app-Foreman_App-CREATE-USER').contains('Track');
            cy.get('#cmx-item-app-Foreman_App-CREATE-USER').click({ force: true });
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(1) > cmx-checkbox > label ').contains('Benutzer anzeigen');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(2) > cmx-checkbox > label ').contains('Änderung Benutzer');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
        });

        it('should contain "Kundeninformation" for customer info key and its corresponding roles', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(2) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('#cmx-item-app-CustomerInfo_App-CREATE-USER').contains('Kundeninformation');
            cy.get('#cmx-item-app-CustomerInfo_App-CREATE-USER').click({force:true});
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click();
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option > cmx-checkbox > label ').contains('Baustellenerstellung');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
        });

        it('should contain "Vertragskonditionen" for commercial cond key and its corresponding roles', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(2) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('#cmx-item-app-QuotationPricing_App-CREATE-USER').contains('Vertragskonditionen');
            cy.get('#cmx-item-app-QuotationPricing_App-CREATE-USER').click({force:true});
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(1) > cmx-checkbox > label ').contains('Käufer');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(2) > cmx-checkbox > label ').contains('Angebotsanfragen anzeigen');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(3) > cmx-checkbox > label ').contains('Erstellen / Akzeptieren');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(4) > cmx-checkbox > label ').contains('Besondere Geschäftsbedingungen');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(5) > cmx-checkbox > label ').contains('Allgemeine Geschäftsbedingungen');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
        });

        it('should contain "Bestell- und Produktkatalog" for order and prod key and its corresponding roles', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(2) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('#cmx-item-app-OrderProductCat_App-CREATE-USER').contains('Bestell- und Produktkatalog');
            cy.get('#cmx-item-app-OrderProductCat_App-CREATE-USER').click({force:true});
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(1) > cmx-checkbox > label ').contains('Eigentümer erstellt Aufträge');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(2) > cmx-checkbox > label ').contains('Vorarbeiter erstellt Aufträge');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(3) > cmx-checkbox > label ').contains('Bestellungen erstellen Einkaufsleiter');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
        });

        it('should contain "Benutzerverwaltung" for user manage key and its corresponding roles', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(2) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('#cmx-item-app-UserProvisioning_App-CREATE-USER').contains('Benutzerverwaltung');
            cy.get('#cmx-item-app-UserProvisioning_App-CREATE-USER').click({force:true});
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option > cmx-checkbox > label ').contains('Benutzer erstellen');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
        });

        it('should contain "Rechnung und Zahlungen" for invoice and pay key and its corresponding roles', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(2) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('#cmx-item-app-INVOICE_PAYMENTS-CREATE-USER').contains('Rechnung und Zahlungen');
            cy.get('#cmx-item-app-INVOICE_PAYMENTS-CREATE-USER').click({force:true});
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(1) > cmx-checkbox > label ').contains('Rechnungsprüfer');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(2) > cmx-checkbox > label ').contains('Kreditorenbuchhalter');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(3) > cmx-checkbox > label ').contains('Kreditorenbuchhalter (Vorauszahlungen)');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(4) > cmx-checkbox > label ').contains('Käufer Bar');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(5) > cmx-checkbox > label ').contains('Abgleich');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(6) > cmx-checkbox > label ').contains('Zugang Pfandrechtanfrage');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(1) > cmx-checkbox > label ').click({ force: true });
            cy.get('#cmx-btn-next-CREATE-USER').click({force: true});
        });

    });

    describe('Assign Data', function() {
        it('should contain "Daten zuweisen" as page title', function () {
            cy.get('#cmx-createuser-pagetitle-lbl').contains('Daten zuweisen');
        });
        it('should contain "Juristische Personen" as first page tab', function () {
            cy.get('#0').contains('Juristische Personen');
            cy.get('#0').click({ force: true });
        });
        it('should contain "Vorname" as first Legal Entities column', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(1) > customers-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(2) > div > div ').contains('Vorname');
        });
        it('should contain "Kundennummer" as second Legal Entities column', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(1) > customers-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(3) > div > div ').contains('Kundennummer');
        });
        it('should contain "Region / Adresse" as third Legal Entities column', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(1) > customers-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(4) > div > div ').contains('Region / Adresse');
        });
        it('should contain "Telefonnummer" as fourth Legal Entities column', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(1) > customers-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(6) > div > div ').contains('Telefonnummer');
        });
        it('should contain "MwSt." as fifth Legal Entities column', function(){
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(1) > customers-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(5) > div > div ').contains('MwSt.');
        });
        it('should contain "E-mail" as sixth Legal Entities column', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(1) > customers-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(7) > div > div ').contains('E-mail');
        });
        it('should contain "Land" as seventh Legal Entities column', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(1) > customers-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(8) > div > div ').contains('Land');
        });
        it('should contain "Suche nach Kunde oder Region" as search input PH', function () {
            cy.get('#cmx-input-search-customer-CREATE-USER').should('have.attr', 'placeholder', 'Suche nach Kunde oder Region');
        });

        it('should contain "Baustellen" as second page tab', function () {
            cy.get('#1').contains('Baustellen');
            cy.get('#1').click({ force: true });
        });
        it('should contain "Vorname" as first Locations column', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(2) > jobsites-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(2) > div > div ').contains('Vorname');
        });
        it('should contain "Baustellen-Nr." as second Locations column', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(2) > jobsites-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(3) > div > div ').contains('Baustellen-Nr.');
        });
        it('should contain "Region / Adresse" as third Locations column', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(2) > jobsites-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(4) > div > div ').contains('Region / Adresse');
        });
        it('should contain "Juristische Person" as fourth Locations column', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(2) > jobsites-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(5) > div > div ').contains('Juristische Person');
        });
        it('should contain "Suche nach Kunde oder Baustelle" as filter PH', function () {
            cy.get('#cmx-input-search-jobsite-CREATE-USER').should('have.attr', 'placeholder', 'Suche nach Kunde oder Baustelle');
            cy.get('#cmx-btn-next-CREATE-USER').click({force: true});
        });
    });

    describe('Summary', function() {
        it('should contain "Kontaktinformationen" as first table title', function () {
            cy.get('.contact-info__title').contains('Kontaktinformationen');
        });
        it('should contain "Vorname" as first column of first table', function () {
            cy.get('#cmx-contact-info-name-lbl').contains('Vorname');
        });
        it('should contain "Position" as second column of first table', function () {
            cy.get('#cmx-contact-info-position-lbl').contains('Position');
        });
        it('should contain "Firma" as third column of first table', function () {
            cy.get('#cmx-contact-info-company-lbl').contains('Firma');
        });
        it('should contain "Land" as fourth column of first table', function () {
            cy.get('#cmx-contact-info-country-lbl').contains('Land');
        });
        it('should contain "E-mail" as fifth column of first table', function () {
            cy.get('#cmx-contact-info-email-lbl').contains('E-mail');
        });
        it('should contain "Telefonnummer" as sixth column of first table', function () {
            cy.get('#cmx-contact-info-phone-lbl').contains('Telefonnummer');
        });
        it('should contain "Zugang zu Daten" as title of second table', function () {
            cy.get('#cmx-title-access-data-CREATE-USER').contains('Zugang zu Daten');
        });
        it('should contain "Juristische Personen" as subtitle of second table', function () {
            cy.get('#cmx-title-customer-CREATE-USER').contains('Juristische Personen');
        });
        it('should contain "Code" as first table column', function () {
            cy.get('#cmx-title-customer-code-CREATE-USER').contains('Code');
        });
        it('should contain "Vorname" as second table column', function () {
            cy.get('#cmx-title-customer-name-CREATE-USER').contains('Vorname');
        });
        it('should contain "Senden" for last step submit button', function () {
            cy.get('#cmx-btn-submit-CREATE-USER').contains('Senden');
        });
    });
});