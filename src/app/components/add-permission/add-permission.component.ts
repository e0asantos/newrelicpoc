import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

// Providers
import { TranslationService } from '@cemex-core/angular-localization-v1/dist/';
import { Broadcaster } from '@cemex-core/events-v1/dist';

// Models
import { Role, Application } from '../../models';

@Component({
    selector: 'add-permission',
    templateUrl: 'add-permission.component.html',
    styleUrls: ['add-permission.component.scss']
})
export class AddPermissionComponent implements OnInit {

    @Input('userApplications')
    public userApplications = [];
    @Input()
    public isRTL = false;
    @Input()
    public templateId: string;
    @Input()
    public counter: number;
    @Output()
    public onRoleToggled = new EventEmitter();
    @Output()
    public onApplicationSelected = new EventEmitter();
    @Output()
    public onRemovePermission = new EventEmitter();

    public applicationPlaceholder = 'forms.placeholder.application';
    public rolePlaceholder = '';
    public roles: Role[];
    public selectedApplication;
    private selectedRoles: Role[] = new Array<Role>();

    constructor(public translationService: TranslationService, private broadcaster: Broadcaster) {
    }

    public ngOnInit(): void {
        this.rolePlaceholder = this.translationService.pt('forms.placeholder.roles');
    }

    public toggleApplication(application) {
        this.rolePlaceholder = this.translationService.pt('forms.placeholder.roles');
        let appHandler = {};
        this.filterAppList(application);
        if (this.selectedApplication === undefined) {
            this.selectedApplication = application;

            appHandler = {
                applicationToAdd: this.selectedApplication,
                applicationToRemove: undefined,
                added: true
            };

        } else {
            appHandler = {
                applicationToAdd: application,
                applicationToRemove: this.selectedApplication,
                added: false
            };
            this.selectedApplication = application;
        }
        this.selectedRoles = [];
        this.applicationPlaceholder = application.applicationName;
        this.roles = application.rolesViewModel;

        if (this.roles.length === 0) {
            this.rolePlaceholder = this.translationService.pt('views.requestPermission.noRoles');
        }
        this.onApplicationSelected.emit(appHandler);
    }

    public toggleRole(event: any, role: Role) {
        if (typeof event === 'boolean') {
            const roleHandler = {
                role,
                appId: this.selectedApplication.applicationId,
                added: event
            };
            const index = this.selectedApplication.rolesViewModel.findIndex((r) => r.roleId === role.roleId);
            if (index !== -1) {
                this.selectedApplication.rolesViewModel[index].assigned = event;
            }
            this.setRolePlaceholder();
            this.onRoleToggled.emit(roleHandler);
        }
    }

    public removePermission() {
        this.onRemovePermission.emit(this.selectedApplication);
    }

    private setRolePlaceholder(translate?: boolean): void {
        if (this.selectedApplication.rolesViewModel.length > 0) {
            const assignedRoles = this.selectedApplication.rolesViewModel.filter((role) => role.assigned === true);
            if (assignedRoles.length > 0) {
                this.rolePlaceholder = assignedRoles.map((r) => {
                    return this.translationService.pt(r.roleCode);
                }).join(', ');
            } else {
                this.rolePlaceholder = this.translationService.pt('forms.placeholder.roles');
            }
        }
    }

    private filterAppList(app: Application) {
        this.userApplications.splice(this.userApplications.indexOf(app), 1);
        if (this.selectedApplication !== undefined && this.userApplications.indexOf(this.selectedApplication) === -1) {
            this.userApplications.push(this.selectedApplication);
        }
    }
}
