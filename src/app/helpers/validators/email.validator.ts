import { FormControl } from '@angular/forms';

export class EmailValidators {

    public static pattern(c: FormControl): any {
        // tslint:disable-next-line:max-line-length
        const regex = /^(([^±!@£$%^&*'|,´ñÑáéíóúÁÉÍÓÚäëïöüåÄËÏÖÜÅýÝ~+§¡!€#¢§¶¬"°•ªº{}()«\\/=<>?¿:;\s@"]+(\.[^±!@£$%^&*'|,´ñÑáéíóúÁÉÍÓÚäëïöüåÄËÏÖÜÅýÝ~+§¡!€#¢§¶¬"°•ªº{}()«\\/=<>?¿:;\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (regex.test(c.value)) {
            return null;
        } else {
            return {
                patternInvalid: true
            };
        }
    }
}
