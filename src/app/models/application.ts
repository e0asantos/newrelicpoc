import { Role } from './role';

export class Application {
        public applicationName: string;
        public applicationCode: string;
        public applicationType: string;
        public applicationId: number;
        public roles: Role[];
        public applicationIcon: string;
        public applicationUrl: string;
        public applicationTitle: string;
}
