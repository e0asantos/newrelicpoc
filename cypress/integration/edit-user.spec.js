describe('Edit User', function () {
    before(function () {
        cy.clearCookies();
        cy.clearLocalStorage();
        cy.window().then((win) => {
            win.sessionStorage.clear();
        });
    });
    beforeEach(function () {
        window.localStorage.setItem('language', 'en_US');
    });
    it('Login Page', function () {
        cy.visit('http://localhost:4200/login/');
    });
    it('Authenticate', function () {
        cy.get('#cmx-login-form-username-field').clear({force:true});
        cy.get('#cmx-login-form-username-field').type('da.us.qa@yopmail.com', {force:true});
        cy.get('#cmx-login-form-password-field').clear({force:true});
        cy.get('#cmx-login-form-password-field').type('Amazon123!', {force:true});
        cy.get('#cmx-login-form-submit-btn').click({ force: true });
        cy.wait(5000);
    });
    it('It is on Dashboard', function () {
        cy.url().should('include', 'dashboard');
    });
    it('Go to User Management', function () {
        cy.get('#cmx-card-app-UserProvisioning_App-DASHBOARD').click({ force: true });
        cy.url().should('include', 'user-management/inbox');
    });
    it('Go to Edit User', function () {
        cy.get('#1').should('have.class', 'nav-link');
        cy.get('#1').click({ force: true }); 
        cy.get('#cmx-table-users-username-2-USER-MANAGEMENT').click({ force: true });
        cy.url().should('include', 'edit-user');
    });
    it('Open/close reset password modal', function () {
        cy.get('#cmx-btn-profile-box-reset-password-EDIT-USER').click({ force: true });
        cy.get('#cmx-btn-cancel-PASSWORD-CHANGE').click({ force: true });
    });
    it('Adding new permission', function () {
        cy.wait(1000);
        cy.get('#cmx-btn-add-permission-EDIT-USER').click({ force: true });
        cy.wait(1000);
    });
    it('Select application and role', function () {
        cy.wait(1000);
        cy.get('#cmx-dropdown-add-permission-application-dropdown').click({ force: true });
        cy.wait(1000);
        cy.get('#cmx-item-app-CustomerInfo_App-EDIT-USER').click({ force: true });
        cy.get('body > app-root > app-pages-auth > div > div > app-edit-user > div:nth-child(1) > cmx-tabs > div > cmx-tab:nth-child(1) > add-permission > div:nth-child(1) > div:nth-child(2) > cmx-dropdown > div > div ').children().should('have.length', 2)
        cy.get('body > app-root > app-pages-auth > div > div > app-edit-user > div:nth-child(1) > cmx-tabs > div > cmx-tab:nth-child(1) > add-permission > div:nth-child(1) > div:nth-child(2) > cmx-dropdown > div > div ').should('have.class', 'cmx-dropdown__placeholder');
        cy.get('body > app-root > app-pages-auth > div > div > app-edit-user > div:nth-child(1) > cmx-tabs > div > cmx-tab:nth-child(1) > add-permission > div:nth-child(1) > div:nth-child(2) > cmx-dropdown > div > div ').click({ force: true });
        cy.get('body > app-root > app-pages-auth > div > div > app-edit-user > div:nth-child(1) > cmx-tabs > div > cmx-tab:nth-child(1) > add-permission > div:nth-child(1) > div:nth-child(2) > cmx-dropdown > div > div:nth-child(2) > div > cmx-dropdown-item:nth-child(1) > div > cmx-checkbox > input ').check();
        cy.get('body > app-root > app-pages-auth > div > div > app-edit-user > div:nth-child(1) > cmx-tabs > div > cmx-tab:nth-child(1) > add-permission > div:nth-child(1) > div:nth-child(2) > cmx-dropdown > div > div:nth-child(2) > div > cmx-dropdown-item:nth-child(1) > div > cmx-checkbox > input ').check();
        cy.get('.cmx-dropdown__mask').should('have.class', 'cmx-dropdown__mask');
        cy.get('.cmx-dropdown__mask').click({ force: true });
    });
    it('Remove permission', function () {
        cy.get('#cmx-btn-remove-application-4-EDIT-USER').should('have.class', 'cmx-icon-delete');
        cy.get('#cmx-btn-remove-application-4-EDIT-USER').click({ force: true });
    });
    it('Click on Customer Information Tab', function () {
        cy.get('#1').should('have.class', 'nav-link');
        cy.get('#1').click({ force: true });
    });
    it('Click on Locations Tab', function () {
        cy.get('#2').should('have.class', 'nav-link');
        cy.get('#2').click({ force: true });
    });
    it('Open Locations Modal', function () {
        cy.get('#cmx-btn-data-access-EDIT-USER').click({ force: true });
    });
    it('Closing Locations Modal', function () {
        cy.get('#cmx-btn-ok-EDIT-USER').click({ force: true });
    });
});