import { Component, ViewChild, OnInit } from '@angular/core';

// Providers
import { CountlyService } from '@cemex-core/helpers-v1/dist';
import { SessionService } from '@cemex-core/angular-services-v2/dist/';
import { TranslationService } from '@cemex-core/angular-localization-v1/dist/';
import { ErrorService } from '../services';

// Components
import { CmxSidebarComponent } from '@cemex/cmx-sidebar-v4/dist';
import { CmxDialogComponent } from '@cemex/cmx-dialog-v4/dist';
import { LocalizationComponent } from '../components/localization/localization.component';

// Utils
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-pages-auth',
    templateUrl: 'auth-pages.component.html',
    styleUrls: ['auth-pages.component.scss']
})

export class AuthPagesComponent extends LocalizationComponent implements OnInit {
    @ViewChild(CmxSidebarComponent)
    public _sidebar: CmxSidebarComponent;

    @ViewChild('warning')
    public warningModal: CmxDialogComponent;

    public emailErrorMessage: string;
    public alertInDOM = false;
    public isOnPublicPage = false;
    public logo = 'cemex_mono_white.svg';

    constructor(
        private countlyService: CountlyService,
        public sessionService: SessionService,
        private router: Router,
        public translationService: TranslationService,
        public errorService: ErrorService,
        private activatedRoute: ActivatedRoute
    ) {
        super();
        const langCookie = this.sessionService.readCookie('_cmxgo_language');
        if (langCookie) {
            this.translationService.setLanguage(langCookie);
            this.sessionService.clearCookie('_cmxgo_language');
        }
        const countlyKey = window['COUNTLY_KEY'];
        const countlyUrl = window['COUNTLY_URL'];
        const user = this.getUserInfoForCountly();
        this.countlyService.startService(countlyUrl, countlyKey);
        const userId = (sessionService.userProfile) ? sessionService.userProfile.userId.toString() : '0';
        this.sessionService.setUserProfile();

        if (this.countlyService.Countly && user) {
            this.countlyService.Countly.app_version = '2.0';
            this.countlyService.changeId(userId);
            this.countlyService.identifyUser(user);
            this.countlyService.init();
        }

        this.router.events.subscribe((event) => {
            const currentPath = window.location.pathname || this.router.url;
            if (currentPath && (currentPath === '/public/legal' || currentPath === '/public/privacy')) {
                this.isOnPublicPage = true;
            }
        });

        this.activatedRoute.queryParams.subscribe((queries) => {
            const currentPath = window.location.pathname;
            const countryParam = queries['countryCode'];
            if (countryParam && (currentPath === '/public/legal' || currentPath === '/public/privacy')) {
                sessionStorage.setItem('country', countryParam);
                const languages = window['CMX_LANGUAGES'];
                const currentLang =
                    languages.find((lang) => lang.countryCode === countryParam) || this.translationService.selectedLanguage;
                this.translationService.setLanguage(currentLang.languageISO);
                this.logo = countryParam === 'IL' ? 'readymix.co.il.white.svg' : 'cemex_mono_white.svg';
            }
        });
    }

    public ngOnInit(): void {
        this.errorService
            .internalServerError
            .subscribe((internalServerError) => {
                if (internalServerError) {
                    this.warningModal.open();
                }
            });
    }

    private getUserInfoForCountly() {
        const user = this.sessionService.userProfile;

        if (user) {
            return {
                email: '',
                // if user has email as userAccount,so use userAcount, otherwise user UserId
                userId: (user.userAccount.indexOf('@') > -1) ? user.userAccount : user.userId,
                name: (user.userAccount.indexOf('@') > -1) ? user.userAccount : user.userId,
                phone: '',
                username: ''
            };
        } else {
            return;
        }
    }

    public onClickMenuButton(): void {
        this._sidebar.isCollapsed = !this._sidebar.isCollapsed;
    }
}
