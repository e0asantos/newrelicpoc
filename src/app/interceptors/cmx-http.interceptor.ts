import { Router } from '@angular/router';
import { Injectable, Injector } from '@angular/core';
import { ConnectionBackend, Request, RequestOptionsArgs, Response, Http, BaseRequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class InterceptedHttp extends Http {
    private router;
    constructor(
        backend: ConnectionBackend,
        defaultOptions: BaseRequestOptions,
        private injector: Injector
    ) {
        super(backend, defaultOptions);
    }

    public request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
        return super.request(url, options).catch(this.catchErrors(url));
    }

    private catchErrors(url: string | Request) {
        let _url;
        if (this.router == null) {
            this.router = this.injector.get(Router);
        }
        return (res: Response) => {
            if (typeof url === 'string') {
                _url = url;
            } else {
                _url = url.url;
            }
            if (_url.indexOf('user/resetpassword') === -1
            && _url.indexOf('oauth2/token') === -1 && (res.status === 401 || res.status === 403)) {
                this.router.navigate(['login'], { queryParams: { sessionExpired: true } });
            }

            return Observable.throw(res);
        };
    }
}
