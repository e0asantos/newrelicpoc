// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  apiHost: 'https://cemexqas.azure-api.net/',
  apiOrg: '',
  apiEnv: '',
  env: 'qa',
  appCode: '',
  clientId: '',
  hostEurope: 'localhost:4200',
  hostAmerica: 'localhost:4200',
  siteDomain: '.cemexgo.com',
  countlyKey: 'b287fda9ffdf4d330741597ae2eff4cc47206260',
  countlyUrl: 'https://cemex.count.ly',
  showMaintenanceAlert: '',
  cmxLoyaltyProgramUrl: 'https://www.mycemex.cemex.com/',
  language: 'en_US',
  translations: {
    translateApiUrl: 'https://uscldcnxwaadmq01.azurewebsites.net',
    translateAppName: 'user-provisioning',
    getLanguages: '/translate/getRegionalLanguages',
    getTranslation: '/translate/translate/',
    fallbackLanguage: 'en_US',
  },
};
