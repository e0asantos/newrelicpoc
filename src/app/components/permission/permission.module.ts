import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { CmxCheckboxModule } from '@cemex/cmx-checkbox-v4';
import { CmxDropdownModule } from '@cemex/cmx-dropdown-v4';
import { PermissionComponent } from './permission.component';

@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        CmxCheckboxModule,
        CmxDropdownModule
    ],
    declarations: [
        PermissionComponent,
    ],
    exports: [
        PermissionComponent,
    ]
})
export class PermissionModule { }
