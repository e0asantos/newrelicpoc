import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { LoginComponent } from './pages/login/login.component';

const appRoutes: Routes = [
    { path: 'login', component: LoginComponent },
    {
        path: 'signup',
        loadChildren: './pages/signup/signup.module#SignupModule'
    },
    {
        path: 'forgotPassword',
        loadChildren: './pages/forgot-password/forgot-password.module#ForgotPasswordModule'
    },
    {
        path: 'resetPassword',
        loadChildren: './pages/reset-password/reset-password.module#ResetPasswordModule'
    },
    {
        path: '',
        loadChildren: './auth-pages/auth-pages.module#AuthPagesModule'
    },
    { path: '**', redirectTo: '' }
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [RouterModule]
})

export class AppRoutingModule { }
