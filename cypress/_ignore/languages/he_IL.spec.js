describe('Login Screen', function () {
    it('starts the application', function () {
        cy.clearCookies();
        cy.clearLocalStorage();
        cy.window().then((win) => {
            win.sessionStorage.clear();
            win.localStorage.setItem('language', 'he_IL');
        });
        cy.visit('http://localhost:4200/login/');
    });
    it('should contain "עברית" in language dropdown', function () {
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(1) > cmx-dropdown > div > div > span ').contains('עברית');
    });
    it('should contain "ברוך הבא" for welcome message.', function(){
        cy.get('.login__content-title').contains('ברוך הבא');
        cy.get('.login__content-title').should('have.class', 'login__content-title');
        cy.get('.login__content-title').click({force:true});
        });
    it('should contain "שם משתמש" in the username form label', function () {
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(2) > cmx-login > div > div:nth-child(2) > form > cmx-form-field:nth-child(1) > label ').contains('שם משתמש');
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(2) > cmx-login > div > div:nth-child(2) > form > cmx-form-field:nth-child(1) > label ').click({ force: true });
    });
    it('should contain "Enter your username" on username input placeholder', function () {
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(2) > cmx-login > div > div:nth-child(2) > form > cmx-form-field:nth-child(1) > input ').should('have.attr', 'placeholder', 'Enter your username');
    });
    it('should contain "סיסמה" in the password form label', function () {
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(2) > cmx-login > div > div:nth-child(2) > form > cmx-form-field:nth-child(3) > label ').contains('סיסמה');
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(2) > cmx-login > div > div:nth-child(2) > form > cmx-form-field:nth-child(3) > label ').click({ force: true });
    });
    it('should contain "Enter your password" on password input placeholder', function () {
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(2) > cmx-login > div > div:nth-child(2) > form > cmx-form-field:nth-child(3) > input ').should('have.attr', 'placeholder', 'Enter your password');
    });
    it('should contain "נראה כי הססמה שלכם אינה מספיק בטוחה." to retrieve users password', function () {
        cy.get('[data-tid="cmx-login-form-forgot-password-link"]').contains('נראה כי הססמה שלכם אינה מספיק בטוחה.');
        cy.get('[data-tid="cmx-login-form-forgot-password-link"]').should('have.class', 'cmx-button');
        cy.get('[data-tid="cmx-login-form-forgot-password-link"]').should('have.class', 'cmx-button--link');
    });
    it('should contain "שמירה." as submit button', function () {
        cy.get('[data-tid="cmx-login-form-submit-btn"]').contains('שמירה.');
        cy.get('[data-tid="cmx-login-form-submit-btn"]').should('have.class', 'cmx-button');
        cy.get('[data-tid="cmx-login-form-submit-btn"]').should('have.class', 'cmx-ripple');
    });
});

describe('FORGOT PASSWORD SCREEN', function () {
    it('starts the application', function () {
        cy.clearCookies();
        cy.clearLocalStorage();
        cy.window().then((win) => {
            win.sessionStorage.clear();
            win.localStorage.setItem('language', 'he_IL');
        });
        cy.visit('http://localhost:4200/forgotPassword/');
    });
    it('should contain "שינוי ססמה" as page title', function () {
        cy.get('.custom-panel__content__title').contains('שינוי ססמה');
        cy.get('.custom-panel__content__title').should('have.class', 'custom-panel__content__title');
        cy.get('.custom-panel__content__title').click({ force: true });
    });
    it('should contain correct text as page description', function () {
        cy.get('.custom-panel__content__instruction').contains('נא להזין את הססמה החדשה שלכם ולאשר על-ידי הזנתה שנית.');
        cy.get('.custom-panel__content__instruction').should('have.class', 'custom-panel__content__instruction');
        cy.get('.custom-panel__content__instruction').click({ force: true });
    });
    it('should contain "נא לאשר את הססמה" for email label', function () {
        cy.get('body > app > forgot-password > div > panel > div > div:nth-child(3) > form > cmx-form-field > label ').contains('נא לאשר את הססמה');
        cy.get('body > app > forgot-password > div > panel > div > div:nth-child(3) > form > cmx-form-field > label ').click({ force: true });
    });
    it('should contain "שמירה." for email input placeholder', function () {
        cy.get('body > app > forgot-password > div > panel > div > div:nth-child(3) > form > cmx-form-field > input ').should('have.attr', 'placeholder', 'שמירה.');
    });
    it('should contain correct error message for invalid email', function () {
        cy.get('#cmx-forgot-password-email-input').clear();
        cy.get('#cmx-forgot-password-email-input').type('foo');
        cy.get('body > app > forgot-password > div > panel > div > div:nth-child(3) > form > cmx-form-field > span > error-message ').contains('הססמאות אינן תואמות.');
    });
    it('should contain "Email is required" when no text is in the input', function () {
        cy.get('#cmx-forgot-password-email-input').clear();
        cy.get('body > app > forgot-password > div > panel > div > div:nth-child(3) > form > cmx-form-field > span > error-message ').contains('Email is required');
    });
    it('should contain "Send" as sumbit button', function () {
        cy.get('#cmx-forgot-password-submit-btn').contains('Send');
    });
});