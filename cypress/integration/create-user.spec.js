describe('Create User', function () {
    before(function () {
        cy.clearCookies();
        cy.clearLocalStorage();
        cy.window().then((win) => {
            win.sessionStorage.clear();
        });
    });
    beforeEach(function () {
        window.localStorage.setItem('language', 'en_US');
    });
    it('Login Page', function () {
        cy.visit('http://localhost:4200/login/');
    });
    it('Authenticate', function () {
        cy.get('#cmx-login-form-username-field').clear();
        cy.get('#cmx-login-form-username-field').type('da.us.qa@yopmail.com');
        cy.get('#cmx-login-form-password-field').clear();
        cy.get('#cmx-login-form-password-field').type('Amazon123!');
        cy.get('#cmx-login-form-submit-btn').click({ force: true });
        cy.wait(5000);
    });
    it('It is on Dashboard', function () {
        cy.url().should('include', 'dashboard');
    });
    it('Go to User Management', function () {
        cy.get('#cmx-card-app-UserProvisioning_App-DASHBOARD').click({ force: true });
        cy.url().should('include', 'user-management/inbox');
    });
    it('Go to Create User', function () {
        cy.get('#1').should('have.class', 'nav-link');
        cy.get('#1').click({ force: true });
        cy.get('#cmx-btn-go-create-user-USER-MANAGEMENT').click({ force: true });
        cy.url().should('include', 'user-management/create-user');
    });
    it('Add name', function () {
        cy.get('#cmx-input-first-name-CREATE-USER').clear({ force: true });
        cy.get('#cmx-input-first-name-CREATE-USER').type('Cypress', { force: true });
    });
    it('Add lastname', function () {
        cy.get('#cmx-input-last-name-CREATE-USER').clear();
        cy.get('#cmx-input-last-name-CREATE-USER').type('Test');
    });
    it('Add position', function () {
        cy.get('#cmx-input-position-CREATE-USER').clear();
        cy.get('#cmx-input-position-CREATE-USER').type('Position');
    });
    it('Add email', function () {
        cy.get('#cmx-input-email-CREATE-USER').clear();
        cy.get('#cmx-input-email-CREATE-USER').type('cypress@test.com');
    });
    it('Add phone', function () {
        cy.get('#cmx-input-phone-CREATE-USER').clear();
        cy.get('#cmx-input-phone-CREATE-USER').type('1554765235');
    });
    it('Add password', function () {
        cy.get('#cmx-input-temp-password-CREATE-USER').clear();
        cy.get('#cmx-input-temp-password-CREATE-USER').type('Gjrot4_d5-');
    });
    it('Select customer', function () {
        cy.get('#cmx-createuser-dropdown-list').click({ force: true });
        cy.get('#cmx-dropdown-item-customer--CREATE-USER').click({ force: true });
    });
    it('Click on Next', function () {
        cy.get('#cmx-btn-next-CREATE-USER').click({ force: true });
    });
    it('Add new permission', function () {
        cy.get('#cmx-btn-add-permission-CREATE-USER').click({ force: true });
    });
    it('Select User Management Application', function () {
        cy.get('#cmx-dropdown-permission-application-dropdown').click({ force: true });
        cy.get('#cmx-item-app-UserProvisioning_App-CREATE-USER').click({ force: true });
    });
    it('Select Creates Users role', function () {
        cy.get('#cmx-dropdown-permission-role-dropdown').click({ force: true });
        cy.get('#cmx-checkbox-role-SECMORGUSR-CREATE-USER').click({ force: true });
    });
    it('Click on Next if is enabled', function () {
        cy.get('#cmx-btn-next-CREATE-USER').click({ force: true });
        cy.wait(1000);
    });
    it('Add customer', function () {
        cy.get('#cmx-btn-add-jobsites-CREATE-USER').click({ force: true });
    });
    it('Assign first customer', function () {
        cy.get('#cmx-check-table-customer-1-legal-entites-modal-CREATE-USER').click({ force: true });
    });
    it('Click on Josbites tab', function () {
        cy.get('#1').should('have.class', 'nav-link');
        cy.get('#1').click({ force: true });
    });
    it('Go to Summary', function () {
        cy.get('#cmx-btn-next-CREATE-USER').click({ force: true });
    });
});