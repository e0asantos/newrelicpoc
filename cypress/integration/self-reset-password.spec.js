describe('Self reset password', function () {
    before(function () {
        cy.clearCookies();
        cy.clearLocalStorage();
        cy.window().then((win) => {
            win.sessionStorage.clear();
        });
    });
    beforeEach(function () {
        window.localStorage.setItem('language', 'en_US');
    });
    it('Login Page', function () {
        cy.visit('http://localhost:4200/login/');
    });
    it('Authenticate', function () {
        cy.get('#cmx-login-form-username-field').clear();
        cy.get('#cmx-login-form-username-field').type('da.us.qa@yopmail.com');
        cy.get('#cmx-login-form-password-field').clear();
        cy.get('#cmx-login-form-password-field').type('Amazon123!');
        cy.get('#cmx-login-form-submit-btn').click({ force: true });
        cy.wait(5000);
    });
    it('It is on Dashboard', function () {
        cy.url().should('include', 'dashboard');
    });
    it('Go to Profile', function () {
        cy.get('#cmx-header-profile-menu').click({ force: true });
        cy.get('#cmx-header-profile-link').click({ force: true });
        cy.wait(2000);
    });
    it('Go to Self reset password', function () {
        cy.get('#cmx-profile-box-selfreset-btn').click({ force: true });
        cy.wait(1000);
        cy.url().should('include', 'profile/self-reset-password');
    });
    it('Test error when current password is invalid', function () {
        cy.wait(1000);
        cy.get('#cmx-input-current-password-SELF-RESET-PASSWORD').clear({ force: true });
        cy.get('#cmx-input-current-password-SELF-RESET-PASSWORD').type('123456', { force: true });
        cy.get('#cmx-input-password-SELF-RESET-PASSWORD').clear({ force: true });
        cy.get('#cmx-input-password-SELF-RESET-PASSWORD').type('Amazon123!', { force: true });
        cy.get('#cmx-input-confirm-password-SELF-RESET-PASSWORD').clear({ force: true });
        cy.get('#cmx-input-confirm-password-SELF-RESET-PASSWORD').type('Amazon123!', { force: true });
        cy.get('#cmx-selfreset-password-save-btn').click({ force: true });
        cy.wait(1000);
        cy.get('#cmx-selfreset-password-error-lbl').click({ force: true });
    });
    it('Password reset ok', function () {
        cy.get('#cmx-input-current-password-SELF-RESET-PASSWORD').clear({ force: true });
        cy.get('#cmx-input-current-password-SELF-RESET-PASSWORD').type('Amazon123!', { force: true });
        cy.get('#cmx-selfreset-password-save-btn').click({ force: true });
    });
});