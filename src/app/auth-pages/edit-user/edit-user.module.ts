import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';

// CMX Dependencies
import { CmxDialogModule } from '@cemex/cmx-dialog-v4';
import { CmxButtonModule } from '@cemex/cmx-button-v4';
import { CmxDropdownModule } from '@cemex/cmx-dropdown-v4';
import { CmxTabsModule } from '@cemex/cmx-tabs-v4';

// Internal Dependencies
import { SearchModule } from './../../components/search/search.module';
import { AddPermissionModule } from '../../components/add-permission/add-permission.module';
import { EditPermissionModule } from '../../components/edit-permission/edit-permission.module';
import { ProfileBoxModule } from './../../components/profile-box/profile-box.module';
import { TablesModule } from '../../components/tables/table.module';
import { ButtonsFooterModule } from '../../components/buttons-footer/buttons-footer.module';
import { BreadcrumbsModule } from '../../components/breadcrumbs/breadcrumbs.module';

// Providers
import { ApplicationService, UserDetailsResolver, CustomerService, UserService } from '../../services';

// Components
import { EditUserComponent } from './edit-user.component';

// Modal Dependencies
import { JobsitesModalModule } from '../../components/modals/jobsites/jobsites.modal.module';
import { LegalEntitiesModalModule } from '../../components/modals/legal-entities/legal-entities.modal.module';
import { CanDeactivateGuard } from '../../guards/can-deactivate.guard';
import { JobsiteService } from '../../services/jobsite.service';

export const ROUTES: Routes = [
    { path: '', component: EditUserComponent, canDeactivate: [CanDeactivateGuard]},
    { path: 'action-log', loadChildren: '../action-log/action-log.module#ActionLogModule'}
];

@NgModule({
    imports: [
        RouterModule.forChild(ROUTES),
        CommonModule,
        EditPermissionModule,
        AddPermissionModule,
        FlexLayoutModule,
        SearchModule,
        TablesModule,
        ButtonsFooterModule,
        ProfileBoxModule,
        CmxTabsModule,
        CmxDialogModule,
        CmxDropdownModule,
        CmxButtonModule,
        BreadcrumbsModule,
        JobsitesModalModule,
        LegalEntitiesModalModule
    ],
    declarations: [
        EditUserComponent
    ],
    exports: [
        EditUserComponent
    ],
    providers: [
        UserDetailsResolver,
        ApplicationService,
        CanDeactivateGuard,
        CustomerService,
        JobsiteService,
        UserService
    ]
})

export class EditUserModule { }
