import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingsComponent } from './settings.component';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { CmxDropdownModule } from '@cemex/cmx-dropdown-v4/dist';
import { BreadcrumbsModule } from '../../components/breadcrumbs/breadcrumbs.module';

export const ROUTES: Routes = [
    { path: '', component: SettingsComponent }
];

@NgModule({
    imports: [
        RouterModule.forChild(ROUTES),
        CommonModule,
        FormsModule,
        CmxDropdownModule,
        BreadcrumbsModule
    ],
    declarations: [SettingsComponent],
})
export class SettingsModule { }
