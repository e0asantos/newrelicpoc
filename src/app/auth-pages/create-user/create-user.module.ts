import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';

// CMX Dependencies
import { CmxFormFieldModule } from '@cemex/cmx-form-field-v4';
import { CmxDropdownModule } from '@cemex/cmx-dropdown-v4';
import { CmxTabsModule } from '@cemex/cmx-tabs-v4';
import { CmxStepsModule } from '@cemex/cmx-steps-v4';
import { CmxCheckboxModule } from '@cemex/cmx-checkbox-v4';
import { CmxButtonModule } from '@cemex/cmx-button-v4';
import { CmxDialogModule } from '@cemex/cmx-dialog-v4';

// Internal Dependencies
import { PermissionModule } from '../../components/permission/permission.module';
import { ContactInfoModule } from '../../components/contact-info/contact-info.module';
import { TablesModule } from './../../components/tables/table.module';
import { ButtonsFooterModule } from './../../components/buttons-footer/buttons-footer.module';
import { UserService, ApplicationService, CustomerService } from '../../services';
import { PasswordTooltipModule } from './../../components/password-tooltip/password-tooltip.module';
import { CreateUserComponent } from './create-user.component';
import { BreadcrumbsModule } from '../../components/breadcrumbs/breadcrumbs.module';

// Modal Dependencies
import { JobsitesModalModule } from '../../components/modals/jobsites/jobsites.modal.module';
import { LegalEntitiesModalModule } from '../../components/modals/legal-entities/legal-entities.modal.module';

export const ROUTES: Routes = [
    { path: '', component: CreateUserComponent }
];

@NgModule({
    imports: [
        RouterModule.forChild(ROUTES),
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        PermissionModule,
        FlexLayoutModule,
        ContactInfoModule,
        TablesModule,
        ButtonsFooterModule,
        CmxDialogModule,
        CmxStepsModule,
        CmxTabsModule,
        CmxDropdownModule,
        CmxCheckboxModule,
        CmxButtonModule,
        CmxFormFieldModule,
        BreadcrumbsModule,
        PasswordTooltipModule,
        JobsitesModalModule,
        LegalEntitiesModalModule
    ],
    declarations: [
        CreateUserComponent],
    exports: [
        CreateUserComponent
    ],
    providers: [
        CustomerService,
        ApplicationService,
        UserService
    ]
})

export class CreateUserModule { }
