import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RecaptchaModule } from 'ng-recaptcha';

// CMX Dependencies
import { CmxFormFieldModule } from '@cemex/cmx-form-field-v4';
import { CmxButtonModule } from '@cemex/cmx-button-v4';
import { CmxDialogModule } from '@cemex/cmx-dialog-v4';

// Internal Dependencies
import { PasswordBardModule } from './../../components/password-bar/password-bar.module';
import { PanelModule } from './../../components/panel/panel.module';
import { TooltipModule } from '../../components/tooltip/tooltip.module';

// Providers
import { UserService } from '../../services';

import { SignupComponent } from './signup.component';

export const ROUTES: Routes = [
    { path: '', component: SignupComponent }
];

@NgModule({
    imports: [
        RouterModule.forChild(ROUTES),
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RecaptchaModule.forRoot(),
        NgbModule.forRoot(),
        PasswordBardModule,
        PanelModule,
        TooltipModule,
        CmxFormFieldModule,
        CmxButtonModule,
        CmxDialogModule
    ],
    declarations: [
        SignupComponent
    ],
    exports: [
        SignupComponent
    ],
    providers: [
        UserService
    ]
})
export class SignupModule { }
