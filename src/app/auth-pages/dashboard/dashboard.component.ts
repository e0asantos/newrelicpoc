import { Component, OnInit, OnDestroy, ViewChild, AfterViewInit, HostListener, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

// CMX Dependencies
import { CmxDialogComponent } from '@cemex/cmx-dialog-v4';

// Providers
import { SessionService } from '@cemex-core/angular-services-v2/dist/';
import { TranslationService } from '@cemex-core/angular-localization-v1/dist/';
import { CountlyService } from '@cemex-core/helpers-v1/dist';
import { AgreementService, UserService, ErrorService } from '../../services';

// Models
import { Application, Agreement, ApiError } from '../../models';
import { IApplicationMenu } from '@cemex-core/types-v2/dist/index.interface';

// Utils
import { DCMConstants } from '../../helpers/DCM.constants';
import { Logger } from '@cemex-core/angular-services-v2/dist';

import { LocalizationComponent } from '../../components/localization/localization.component';

@Component({
    selector: 'app-dashboard',
    templateUrl: 'dashboard.component.html',
    styleUrls: ['dashboard.component.scss']
})
export class DashboardComponent extends LocalizationComponent implements OnInit, OnDestroy, AfterViewInit {

    @ViewChild('agreementModal')
    public agreementModal: CmxDialogComponent;

    @ViewChild('errorModal')
    public errorModal: CmxDialogComponent;

    @ViewChild('topModal')
    public topModal: ElementRef;

    public cmxLoyaltyProgramName = DCMConstants.CMX_LOYALTY_PROGRAM_NAME;
    public cmxLoyaltyProgramUrl = window['CMX_LOYALTY_PROGRAM_URL'];
    public dcmApplications = new BehaviorSubject<IApplicationMenu[]>([]);
    public userApps: Application[] = [];
    public touContent: string;
    public privacyPolicyContent: string;
    public marketingContent: string;
    public hasMkt = true;
    public hasPrivacy = true;
    public userCountryCode = '';
    public hasCMXLoyaltyProgram = false;
    public termsAccepted = false;
    public isMobile = false;
    public allowInformationShare = false;
    public errorMsg = '';

    private subscription: Subscription;
    private mobileBreakpoint = 576;
    private agreementCode: string;
    private agreementVersionId: number;
    private agreementSigned = false;
    private redirectTo;

    constructor(
        public translationService: TranslationService,
        private agreementService: AgreementService,
        private sessionService: SessionService,
        private router: Router,
        private countlyService: CountlyService,
        private userService: UserService,
        private errorService: ErrorService,
        private route: ActivatedRoute) {

        super();

        const countryFromStorage = sessionStorage.getItem('country') || '';
        this.userCountryCode = countryFromStorage.trim().toUpperCase();

        this.checkForLoyaltyProgram();
    }

    public ngOnInit(): void {
        this.redirectTo = this.route.snapshot.queryParams['returnUrl'];
        this.detectMobile();
        this.getUserApps();
        this.getAgreements();
        this.agreementModal.afterClosed().subscribe((result: boolean) => {
            if (!this.agreementSigned) {
                this.router.navigate(['/login']);
            }
        });
    }

    public ngOnDestroy(): void {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    public ngAfterViewInit(): void {
        // temporary patch fix for DCM021CX0118-1436
        if (this.countlyService && this.countlyService.Countly && this.countlyService.Countly.q) {
            this.countlyService.addTrackingPage('Dashboard');
        }
    }

    public launchProduct(path: string) {
        const whereTo = (path || path !== '') ? path : '/';
        const event = {
            key: 'applicationLaunch: ' + path,
        };
        if (this.countlyService && this.countlyService.Countly && this.countlyService.Countly.q) {
            this.countlyService.addTracking('add_event', event);
        }
        this.router.navigate([whereTo]);
    }

    public toggleTermsAcceptance(): void {
        this.termsAccepted = !this.termsAccepted;
    }

    public toggleAllowInfoShare(): void {
        this.allowInformationShare = !this.allowInformationShare;
    }

    public signAgreements(): void {
        if (this.allowInformationShare) {
            this.updateInformationShareFlag();
        }
        this.agreementService
            .signAgreement(this.agreementCode, this.agreementVersionId)
            .subscribe((data) => {
                this.agreementSigned = true;
                this.agreementService.updateTermsOfUse(true);
                sessionStorage.setItem('touSigned', 'true');
                this.agreementModal.close();
                if (this.redirectTo) {
                    this.router.navigate([this.redirectTo]);
                }
            },
                (error) => {
                    this.errorService
                        .handleError(error)
                        .subscribe((apiError: ApiError) => {
                            if (apiError) {
                                this.errorMsg = `${apiError.errorCode}: ${this.translationService.pt(apiError.errorDesc)}`;
                                this.errorModal.open();
                            }
                        });
                });
    }

    public scrollToTop(): void {
        this.topModal.nativeElement.scrollIntoView(true);
    }

    public openLoyaltyProgram(): void {
        window.open(this.cmxLoyaltyProgramUrl, '_blank');
    }

    private updateInformationShareFlag(): void {
        this.userService
            .updateFlag(this.allowInformationShare)
            .subscribe((status) => Logger.log('User Information share option updated successfully.'),
                (error) => Logger.error('Error while trying to update user information share flag: ' + error));
    }

    private getUserApps(): void {
        this.sessionService.menuApplicationItems.subscribe(
            (result: IApplicationMenu[]) => {
                this.dcmApplications.next(result);
            }
        );
    }

    private getAgreements(): void {
        this.agreementService
            .getAgreements(this.userCountryCode)
            .subscribe((agreements: Agreement[]) => {
                if (agreements && agreements.length === 0) {
                    sessionStorage.setItem('touError', 'true');
                    this.router.navigate(['/login']);
                }
                // Get Terms of Use content
                const touFound: Agreement = agreements
                    .find((agreement) => agreement.AgreementCategory === DCMConstants.AGREEMENT_CAT.TermsOfUse);

                if (touFound && !touFound.IsSigned) {
                    this.agreementVersionId = touFound.AgreementVersionId;
                    this.agreementCode = touFound.DocumentHash;
                    const touHash = touFound.DocumentHash;
                    if (touHash) {
                        this.agreementService
                            .getAgreementContent(touHash)
                            .subscribe((touContent) => {
                                this.touContent = touContent;
                                if (!this.touContent && this.touContent === '') {
                                    Logger.error('Error getting Terms of Use Document');
                                    sessionStorage.setItem('touError', 'true');
                                    this.router.navigate(['/login']);
                                    return;
                                }
                                // Get Privacy Policy content
                                const privacyPolicy: Agreement = agreements
                                    .find((agreement) => agreement.AgreementCategory === DCMConstants.AGREEMENT_CAT.PrivacyPolicy);

                                if (privacyPolicy && privacyPolicy.DocumentHash) {
                                    this.agreementService
                                        .getAgreementContent(privacyPolicy.DocumentHash)
                                        .subscribe((privacyContent: string) => this.privacyPolicyContent = privacyContent,
                                            (error) => {
                                                Logger.error('Error while getting Privacy policy document: ' + error);
                                                this.hasPrivacy = false;
                                            });
                                } else {
                                    this.hasPrivacy = false;
                                    Logger.warn('No privacy policy available for your country');
                                }
                                const marketing: Agreement = agreements
                                    .find((agreement) => agreement.AgreementCategory === DCMConstants.AGREEMENT_CAT.MKT);

                                // Get Marketing
                                if (marketing && marketing.DocumentHash) {
                                    this.agreementService
                                        .getAgreementContent(marketing.DocumentHash)
                                        .subscribe((marketingContent: string) => this.marketingContent = marketingContent,
                                            (error) => {
                                                Logger.error('Error while getting Marketing document: ' + error);
                                                this.hasMkt = false;
                                            });
                                } else {
                                    Logger.warn('No Marketing agreement available for your country');
                                    this.hasMkt = false;
                                }
                                this.agreementModal.open();
                            }, (error) => {
                                Logger.error('Error getting Terms of Use Document');
                                sessionStorage.setItem('touError', 'true');
                                this.router.navigate(['/login']);
                            });
                    }
                } else {
                    this.agreementSigned = true;
                }
            }, (error) => {
                this.errorService
                    .handleError(error)
                    .subscribe((apiError: ApiError) => {
                        Logger.error('Error getting Agreements');
                        sessionStorage.setItem('touError', 'true');
                        this.router.navigate(['/login']);
                    });
            });
    }

    private checkForLoyaltyProgram(): void {
        if (this.userCountryCode !== 'MX') {
            this.hasCMXLoyaltyProgram = false;
        } else {
            const _userApps = sessionStorage.getItem('applications') || '';
            this.userApps = (_userApps !== '') ? JSON.parse(_userApps) : [];
            const userProvApp: Application = this.userApps.find((app) => app.applicationCode === DCMConstants.DCM_APPLICATION_CODE);

            if (userProvApp) {
                this.hasCMXLoyaltyProgram = userProvApp.roles
                    .findIndex((app) => app.roleCode === DCMConstants.DCM_LOYALTY_PROGRAM_CODE) > -1;
            } else {
                this.hasCMXLoyaltyProgram = false;
            }

        }
    }

    @HostListener('window:resize', [])
    private onresize(): void {
        this.detectMobile();
    }

    private detectMobile(): void {
        this.isMobile = window.innerWidth <= this.mobileBreakpoint;
    }
}
