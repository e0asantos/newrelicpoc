import { NgModule } from '@angular/core';
import { CustomerInfoLauncherComponent } from './customer-info-launcher.component';
import { UserService } from '../../services/user.service';

@NgModule({
  imports: [],
  declarations: [CustomerInfoLauncherComponent],
  exports: [CustomerInfoLauncherComponent],
  providers: [UserService]
})
export class CustomerInfoLauncherModule {}
