describe('Login Screen', function () {
    it('starts the application', function () {
        cy.clearCookies();
        cy.clearLocalStorage();
        cy.window().then((win) => {
            win.sessionStorage.clear();
            win.localStorage.setItem('language', 'pl_PL');
        });
        cy.visit('http://localhost:4200/login/');
    });
    it('should contain "Polski" in language dropdown', function () {
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(1) > cmx-dropdown > div > div > span ').contains('Polski');
    });
    it('should contain "Welcome" for welcome message.', function(){
        cy.get('.login__content-title').contains('Welcome');
        cy.get('.login__content-title').should('have.class', 'login__content-title');
        cy.get('.login__content-title').click({force:true});
        });
    it('should contain "Username" in the username form label', function () {
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(2) > cmx-login > div > div:nth-child(2) > form > cmx-form-field:nth-child(1) > label ').contains('Username.');
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(2) > cmx-login > div > div:nth-child(2) > form > cmx-form-field:nth-child(1) > label ').click({ force: true });
    });
    it('should contain "Enter your username" on username input placeholder', function () {
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(2) > cmx-login > div > div:nth-child(2) > form > cmx-form-field:nth-child(1) > input ').should('have.attr', 'placeholder', 'Enter your username');
    });
    it('should contain "Password" in the password form label', function () {
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(2) > cmx-login > div > div:nth-child(2) > form > cmx-form-field:nth-child(3) > label ').contains('Password');
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(2) > cmx-login > div > div:nth-child(2) > form > cmx-form-field:nth-child(3) > label ').click({ force: true });
    });
    it('should contain "Enter your password" on password input placeholder', function () {
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(2) > cmx-login > div > div:nth-child(2) > form > cmx-form-field:nth-child(3) > input ').should('have.attr', 'placeholder', 'Enter your password');
    });
    it('should contain link text "Forgot password?" to retrieve users password', function () {
        cy.get('[data-tid="cmx-login-form-forgot-password-link"]').contains('Forgot password?');
        cy.get('[data-tid="cmx-login-form-forgot-password-link"]').should('have.class', 'cmx-button');
        cy.get('[data-tid="cmx-login-form-forgot-password-link"]').should('have.class', 'cmx-button--link');
    });
    it('should contain "Log In" as submit button', function () {
        cy.get('[data-tid="cmx-login-form-submit-btn"]').contains('Log In');
        cy.get('[data-tid="cmx-login-form-submit-btn"]').should('have.class', 'cmx-button');
        cy.get('[data-tid="cmx-login-form-submit-btn"]').should('have.class', 'cmx-ripple');
    });
});

describe('FORGOT PASSWORD SCREEN', function () {
    it('starts the application', function () {
        cy.clearCookies();
        cy.clearLocalStorage();
        cy.window().then((win) => {
            win.sessionStorage.clear();
            win.localStorage.setItem('language', 'pl_PL');
        });
        cy.visit('http://localhost:4200/forgotPassword/');
    });
    it('should contain "Forgot your password?" as page title', function () {
        cy.get('.custom-panel__content__title').contains('Forgot your password?');
        cy.get('.custom-panel__content__title').should('have.class', 'custom-panel__content__title');
        cy.get('.custom-panel__content__title').click({ force: true });
    });
    it('should contain correct text as page description', function () {
        cy.get('.custom-panel__content__instruction').contains('Enter your e-mail address and we will send you a link to reset your password.');
        cy.get('.custom-panel__content__instruction').should('have.class', 'custom-panel__content__instruction');
        cy.get('.custom-panel__content__instruction').click({ force: true });
    });
    it('should contain "Email" for email label', function () {
        cy.get('body > app > forgot-password > div > panel > div > div:nth-child(3) > form > cmx-form-field > label ').contains('Email');
        cy.get('body > app > forgot-password > div > panel > div > div:nth-child(3) > form > cmx-form-field > label ').click({ force: true });
    });
    it('should contain "Enter email" for email input placeholder', function () {
        cy.get('body > app > forgot-password > div > panel > div > div:nth-child(3) > form > cmx-form-field > input ').should('have.attr', 'placeholder', 'Enter email');
    });
    it('should contain correct error message for invalid email', function () {
        cy.get('#cmx-forgot-password-email-input').clear();
        cy.get('#cmx-forgot-password-email-input').type('foo');
        cy.get('body > app > forgot-password > div > panel > div > div:nth-child(3) > form > cmx-form-field > span > error-message ').contains('Invalid email format');
    });
    it('should contain "Email is required" when no text is in the input', function () {
        cy.get('#cmx-forgot-password-email-input').clear();
        cy.get('body > app > forgot-password > div > panel > div > div:nth-child(3) > form > cmx-form-field > span > error-message ').contains('Email is required');
    });
    it('should contain "Send" as sumbit button', function () {
        cy.get('#cmx-forgot-password-submit-btn').contains('Send');
    });
    // not working
    // it('should contain correct message when the email is not associated with an account', function () {
    //     cy.get('#cmx-forgot-password-email-input').clear();
    //     cy.get('#cmx-forgot-password-email-input').type('foo@gmail.com');
    //     cy.get('#ngrecaptcha-0 > div > div > iframe:nth-child(1)').click({ force: true });
    // });
});