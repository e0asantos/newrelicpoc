import {
  Component,
  ElementRef,
  OnInit,
  ViewEncapsulation
} from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from '@cemex-core/angular-services-v2/dist/';
import Product1Launcher from '../../../commercialconditions/main';
import registerLauncher from './registerLauncher';
import { Environment } from './types';

@Component({
  selector: 'app-customer-info-launcher',
  templateUrl: './customer-info-launcher.component.html',
  styleUrls: ['../../../commercialconditions/styles.css'],
  encapsulation: ViewEncapsulation.ShadowDom
})
export class CustomerInfoLauncherComponent implements OnInit {
  // private appRoot = 'react-app';
  private _selector: string;

  constructor(
    elem: ElementRef,
    private router: Router,
    public sessionService: SessionService
  ) {
    window['cemexApps']['product5'] = {
      session: this.sessionService
    };
    registerLauncher('product1', new Product1Launcher());
    this._selector = elem.nativeElement.tagName.toLowerCase();
  }

  public navigateTo(path: string) {
    /*
      window['cemexApps']['product1'].destroy(
        document.getElementById(this.appRootNode)
      );
    */
    this.router.navigate([path]);
  }

  public ngOnInit() {
    const PFX = '[wrapper]';

    // const shadowRoot = this.makeShadowTree(
    //   document.getElementById(this.appRoot)
    // );

    const shadowRoot = document.querySelector(this._selector).shadowRoot;

    const environment: Environment = {
      apiHost: window['API_HOST'],
      appFeatures: 'api',
      basePath: '/customerinfo',
      build: '',
      countlyKey: {
        CC: '0de2da6e902991c80e8d526e451298078e1cb3d0',
        CI: 'f2eb502c0ecddbc67dc7a41d4534afe7669fa0cb'
      },
      dcmHost: 'https://uscldcnxwacxgoq01.azurewebsites.net',
      environment: 'quality',
      googleMapsApiKey: '',
      ibmClientId: '721e5c7b-73b8-40e1-8cb2-31c7dbdbd1be',
      production: true,
      translations: {
        locale: 'en_US',
        fallbackLocale: 'en_US',
        translateUrl:
          'https://uscldcnxwaadmq01.azurewebsites.net/api/Translation/'
      }
    };

    window['cemexApps']['product1'].init(shadowRoot, { environment }, () => {
      shadowRoot.appendChild(this.createStyle());
      console.log(PFX, 'App inited');
    });
  }

  /**
   * @description
   *
   * Creates the shadow root for the component.
   *
   * https://w3c.github.io/webcomponents/spec/shadow/
   */
  private makeShadowTree(hostRoot: HTMLElement) {
    return hostRoot.attachShadow({ mode: 'open' });
  }

  /**
   * @description
   *
   * Creates the encapsulated styles for the component.
   *
   * Note: As Nov '18 Shadow DOM doesn't support shared stylesheets from the host,
   * in the meantime global styles are reimported in the component itself.
   */
  private createStyle() {
    const style = document.createElement('style');
    style.textContent = `
        @import url("/commercialconditions/styles.css");
        `;
    return style;
  }
}
