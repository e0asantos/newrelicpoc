import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// CMX Dependencies
import { CmxNavHeaderModule } from '@cemex/cmx-nav-header-v4/dist';
import { CmxSidebarModule } from '@cemex/cmx-sidebar-v4/dist';
import { CmxFooterModule } from '@cemex/cmx-footer-v4/dist';
import { CmxDialogModule } from '@cemex/cmx-dialog-v4';
import { CmxButtonModule } from '@cemex/cmx-button-v4/dist';

// Providers
import { AdminAuthGuard } from '../guards/admin-auth.guard';
import { AuthGuard } from '../guards/auth.guard';
import { UserService } from '../services/user.service';

import { AuthPagesComponent } from './auth-pages.component';
import { AuthRoutingModule } from './auth-pages.routes';
import { TermsConditionsGuard } from '../guards/terms-conditions.guard';
import { SeveralErrorComponent } from './several-error/several-error.component';
import { CustomerInfoLauncherModule } from '../launchers/customer-info-launcher/customer-info-launcher.module';

@NgModule({
    imports: [
        CommonModule,
        CmxNavHeaderModule,
        CmxSidebarModule,
        CmxFooterModule,
        CmxDialogModule,
        CmxButtonModule,
        AuthRoutingModule,
        CustomerInfoLauncherModule
    ],
    exports: [
        AuthPagesComponent
    ],
    declarations: [
        AuthPagesComponent,
        SeveralErrorComponent
    ],
    providers: [
        AdminAuthGuard,
        AuthGuard,
        TermsConditionsGuard,
        UserService
    ],
})

export class AuthPagesModule { }
