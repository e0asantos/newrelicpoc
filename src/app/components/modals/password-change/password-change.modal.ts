import { Component, ViewChild, OnInit } from '@angular/core';

// Providers
import { TranslationService } from '@cemex-core/angular-localization-v1/dist';
import { PasswordService } from '../../../services/';

// CMX Dependencies
import { CmxDialogComponent } from '@cemex/cmx-dialog-v4';

@Component({
    selector: 'password-change-modal',
    templateUrl: './password-change.modal.html',
    styleUrls: ['./password-change.modal.scss']
})

export class PasswordChangeModal implements OnInit {

    public email = '';
    public modalMsg = '';
    public modalText = '';
    public emailSent = true;
    public submitting = false;

    @ViewChild('passwordChange')
    private passwordChange: CmxDialogComponent;
    @ViewChild('successModal')
    private successModal: CmxDialogComponent;

    constructor(
        public translationService: TranslationService,
        private passwordService: PasswordService
    ) { }

    public ngOnInit(): void {
        this.successModal.afterClosed().subscribe(() => this.email = '');
    }

    public open(event) {
        this.email = event.userAccount;
        this.emailSent = true;
        this.modalText = `${this.translationService.pt('views.password_reset')} ${this.email}`;
        this.passwordChange.open();
    }

    public close() {
        this.passwordChange.close();
    }

    public resetPassword(): void {
        this.submitting = true;
        this.passwordService
            .requestPasswordChange(this.email)
            .subscribe((response) => {
                this.submitting = false;
                this.emailSent = !response['emailIsSent'] || true;
                if (!this.emailSent) {
                    this.modalText = this.translationService.pt('views.forgotPassword.EmailNotDelivered');
                } else {
                    this.passwordChange.close();
                    this.successModal.open();
                }
            },
                (error) => this.submitting = false
            );
    }
}
