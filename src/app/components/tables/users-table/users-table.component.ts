import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

// Utils
import { SharedFunctions } from '../../../helpers/functions';

// Components
import { Column, Table, CmxTableComponent } from '@cemex/cmx-table-v1/dist';
import { LocalizationComponent } from './../../localization/localization.component';

// Providers
import { TranslationService } from '@cemex-core/angular-localization-v1/dist';
import { WatcherService } from '@cemex/cmx-table-v1/dist/services';

@Component({
    selector: 'users-table',
    templateUrl: 'users-table.component.html',
    styleUrls: ['./../table.component.scss'],
    providers: [WatcherService]
})

export class UsersTableComponent implements OnInit {

    @Input()
    public showCheckboxes = false;

    @Input()
    public templateId: string;

    @Input() public isRTL = false;

    @ViewChild(CmxTableComponent)
    public table: CmxTableComponent;

    @Input()
    get users(): any[] {
        return this._users.getValue();
    }
    set users(value: any[]) {
        this._users.next(value);
    }

    @Output() public onEditUser = new EventEmitter();

    public userTable = new Table();
    private _users = new BehaviorSubject<any[]>([]);

    constructor(public translationService: TranslationService, public functions: SharedFunctions) {
        const userTableColumns: Column[] = [
            new Column('views.global.lbl_userName', 'fullName', true, true, 'fullName', 'fullName'),
            new Column('views.global.lbl_company', 'customerDesc', true, true, 'customerDesc', 'customerDesc'),
            new Column('views.global.lbl_email', 'userAccount', true, true, 'userAccount', 'userAccount'),
            new Column('views.global.lbl_phone', 'phoneNumber', true, true, 'phoneNumber', 'phoneNumber'),
            new Column('views.global.lbl_accessApps', 'applications', true, true, 'applications', 'applications')

        ];
        this.userTable.setColumns(userTableColumns);
    }

    public ngOnInit(): void {
        this._users.subscribe(
            (data) => {
                /** ADD INDEX ( SCRIPTS IN QA) */
                data.forEach((e, index) => {
                    e.index = index;
                });
                if (data.length > 600) {
                    this.table.pageSize = Math.ceil(data.length / 20);
                }
                this.setUserFilters();
            });
    }

    public ngOnChanges(): void {
        this.userTable.getColumn('fullName').clearFilters();
        this.userTable.getColumn('customerDesc').clearFilters();
        this._users.subscribe(
            () => {
                this.setUserFilters();
            });
    }

    public setUserFilters(): void {
        this.users.forEach((data) => {
            this.userTable.getColumn('fullName').addFilter(data);
            this.userTable.getColumn('customerDesc').addFilter(data);
        });
    }

    public editUser(user: any) {
        this.onEditUser.emit(user);
    }
}
