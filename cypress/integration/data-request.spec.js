describe('Data Request', function () {
    before(function () {
        cy.clearCookies();
        cy.clearLocalStorage();
        cy.window().then((win) => {
            win.sessionStorage.clear();
        });
    });
    beforeEach(function () {
        window.localStorage.setItem('language', 'en_US');
    });
    it('Login Page', function () {
        cy.visit('http://localhost:4200/login/');
    });
    it('Authenticate', function () {
        cy.get('#cmx-login-form-username-field').clear();
        cy.get('#cmx-login-form-username-field').type('da.us.qa@yopmail.com');
        cy.get('#cmx-login-form-password-field').clear();
        cy.get('#cmx-login-form-password-field').type('Amazon123!');
        cy.get('#cmx-login-form-submit-btn').click({ force: true });
        cy.wait(5000);
    });
    it('It is on Dashboard', function () {
        cy.url().should('include', 'dashboard');
    });
    it('Go to Profile', function () {
        cy.wait(1000);
        cy.get('#cmx-header-profile-menu').click({ force: true });
        cy.get('#cmx-header-profile-link').click({ force: true });
        cy.wait(2000);
    });
    it('Click on My jobsites tab', function () {
        cy.get('#2').should('have.class', 'nav-link');
        cy.get('#2').click({ force: true });
    });
    it('Go to Data Access Request', function () {
        cy.get('#cmx-btn-go-data-request-MYPROFILE').click({ force: true });
    });
    it('Display jobsite modal', function () {
        cy.get('#cmx-title-jobsite-modal').click({ force: true });
    });
});