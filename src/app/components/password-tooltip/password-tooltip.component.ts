import { Component, OnChanges, Input, SimpleChange, HostListener, Output, EventEmitter } from '@angular/core';

// Providers
import { TranslationService } from '@cemex-core/angular-localization-v1/dist/';
import { UserService } from '../../services';

@Component({
    selector: 'app-password-tooltip',
    templateUrl: './password-tooltip.component.html',
    styleUrls: ['./password-tooltip.component.scss']
})

export class PasswordTooltipComponent implements OnChanges {

    @Input() public rtl: boolean;
    @Input() public passwordToCheck: string;
    @Input() public firstName = '';
    @Input() public lastName = '';
    @Input() public userAcount = '';
    @Input() public active = false;
    @Output() public valid = new EventEmitter();

    public errorMsg = '';
    public hasError = false;
    public passwrd = '';
    public validName = false;
    public validLast = false;
    public validAlphabetiCharacter = false;
    public validLong = false;
    public validNumeric = false;
    public validStartsWithAlphabet = false;
    public validUserAccount = false;
    public minLength = 0;
    public minAlphas = 0;
    public minNumerals = 0;
    public showStartsWithAlphabet = false;
    public showMinLength = false;
    public showMinAlphas = false;
    public showMinNumerals = false;
    public showLastNameDisallowed = false;
    public showFirstNameDisallowed = false;
    public showUserIdDisallowed = false;

    constructor(
        public translationService: TranslationService,
        private userService: UserService
    ) { }

    @HostListener('document:click', ['$event.target'])
    public clickOutside(targetElement) {
        if (targetElement.type !== 'password') {
            this.active = false;
            this.valid.emit();
        } else if (targetElement.id === 'cmx-input-current-password-SELF-RESET-PASSWORD' ||
            targetElement.id === 'cmx-input-confirm-password-SELF-RESET-PASSWORD') {
            this.active = false;
            this.valid.emit();
        }
    }

    @HostListener('document:keydown', ['$event'])
    public onKeydownHandler(event: KeyboardEvent) {
        if ((event.keyCode === 9 || event.keyCode === 13 || event.keyCode === 27) && this.active) {
            this.active = false;
            this.valid.emit();
        }
    }

    @HostListener('window:resize', ['$event'])
    public onResize(event) {
        this.active = false;
        this.valid.emit();
    }

    public getRulePassword(countryCode) {
        this.userService.getRulesPassword(countryCode).subscribe(
            (data) => {
                this.hasError = false;
                this.showFirstNameDisallowed = data.lastNameDisallowed;
                this.showLastNameDisallowed = data.firstNameDisallowed;
                this.showUserIdDisallowed = data.userIdDisallowed;
                this.showStartsWithAlphabet = data.startsWithAlphabet;
                this.showMinLength = data.minLength > 0 ? true : false;
                this.showMinAlphas = data.minAlphas > 0 ? true : false;
                this.showMinNumerals = data.minNumerals > 0 ? true : false;
                this.minLength = data.minLength;
                this.minAlphas = data.minAlphas;
                this.minNumerals = data.minNumerals;
            },
            (error) => {
                this.hasError = true;
                this.errorMsg = 'views.passwordTooltip.countryRuleError';
            }
        );
    }

    public ngOnChanges(changes: { [propName: string]: SimpleChange }): void {
        if (changes['passwordToCheck']) {
            this.passwrd = changes['passwordToCheck'].currentValue;
            this.checkValidation();
        }
    }

    public isPassValid() {
        this.checkValidation();
        const isValid = this.validStartsWithAlphabet && this.validNumeric && this.validAlphabetiCharacter && this.validName &&
            this.validLast && this.validLong && this.validUserAccount && !this.hasError;
        return isValid;
    }

    private checkValidation() {
        this.passwrd = this.passwrd === null ? '' : this.passwrd;

        if (this.passwrd !== '') {
            this.validName = !this.showFirstNameDisallowed ? true : this.validName;
            if (this.showFirstNameDisallowed) {
                this.validName = false;
                if (this.firstName !== undefined) {
                    const fn = this.passwrd.toLowerCase().indexOf(this.firstName.toLowerCase());
                    this.validName = this.passwrd === '' ? false : fn === -1 ? true : false;
                }
            }

            this.validLast = !this.showLastNameDisallowed ? true : this.validLast;
            if (this.showLastNameDisallowed) {
                this.validLast = false;
                if (this.lastName !== undefined) {
                    const ln = this.passwrd.toLowerCase().indexOf(this.lastName.toLowerCase());
                    this.validLast = this.passwrd === '' ? false : ln === -1 ? true : false;
                }
            }

            this.validUserAccount = !this.showUserIdDisallowed ? true : this.validUserAccount;
            if (this.showUserIdDisallowed) {
                this.validUserAccount = false;
                if (this.userAcount !== undefined) {
                    const ua = this.passwrd.toLowerCase().indexOf(this.userAcount.toLowerCase());
                    this.validUserAccount = this.passwrd === '' ? false : ua === -1 ? true : false;
                }
            }

            this.validStartsWithAlphabet = !this.showStartsWithAlphabet ? true : this.validStartsWithAlphabet;
            if (this.showStartsWithAlphabet) {
                this.validStartsWithAlphabet = this.passwrd === '' ? false : /[a-zA-Z]+/.test(this.passwrd[0]);
            }

            this.validLong = !this.showMinLength ? true : this.validLong;
            if (this.showMinLength) {
                this.validLong = this.passwrd.length > (this.minLength - 1) ? true : false;
            }

            this.validAlphabetiCharacter = !this.showMinAlphas ? true : this.validAlphabetiCharacter;
            if (this.showMinAlphas) {
                let totalAlpha = 0;
                this.passwrd.split('').forEach((e) => {
                    const a = /[a-zA-Z]+/.test(e);
                    totalAlpha = a ? (totalAlpha + 1) : totalAlpha;
                });

                this.validAlphabetiCharacter = totalAlpha > (this.minAlphas - 1);
            }

            this.validNumeric = !this.showMinNumerals ? true : this.validNumeric;
            if (this.showMinNumerals) {
                if (this.minNumerals === 1) {
                    this.validNumeric = /[0-9]+/.test(this.passwrd);
                } else {
                    let totalNumeric = 0;
                    this.passwrd.split('').forEach((e) => {
                        const a = /[0-9]+/.test(e);
                        totalNumeric = a ? (totalNumeric + 1) : totalNumeric;
                    });
                    this.validNumeric = totalNumeric > (this.minNumerals - 1);
                }
            }
        } else {
            this.validName = false;
            this.validLast = false;
            this.validUserAccount = false;
            this.validStartsWithAlphabet = false;
            this.validLong = false;
            this.validAlphabetiCharacter = false;
            this.validNumeric = false;
        }

    }
}
