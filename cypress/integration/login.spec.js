describe('Login', function () {
    it('starts the application', function () {
        cy.clearCookies();
        cy.clearLocalStorage();
        cy.window().then((win) => {
            win.sessionStorage.clear();
        });
        cy.visit('http://localhost:4200/login/');
    });
    it('Open language selector', function () {
        cy.get('#cmx-login-language-dropdown').click({ force: true });
        cy.wait(1000);
    });
    it('Select es_MX language', function () {
        cy.get('#cmx-login-language-item-es_MX').click({ force: true });
        cy.wait(1000);
    });
    it('Select en_US language', function () {
        cy.get('#cmx-login-language-dropdown').click({ force: true });
        cy.wait(1000);
        cy.get('#cmx-login-language-item-en_US').click({ force: true });
    });
    it('Close language selector', function () {
        cy.get('#cmx-login-language-dropdown').click({ force: true });
    });
    it('Go to forgot password', function () {
        cy.get('#cmx-login-form-forgot-password-link').click({ force: true });
        cy.wait(1000);
        cy.url().should('eq', 'http://localhost:4200/forgotPassword');
    });
    it('Back to login', function () {
        cy.get('#cmx-panel-close-btn').click({ force: true });
        cy.wait(1000);
        cy.url().should('eq', 'http://localhost:4200/login');
    });
    it('Authentication - Fill username', function () {
        cy.get('#cmx-login-form-username-field').click({ force: true });
        cy.get('#cmx-login-form-username-field').clear();
        cy.get('#cmx-login-form-username-field').type('da.us.qa@yopmail.com');
    });
    it('Authentication - Fill password', function () {
        cy.get('#cmx-login-form-password-field').click({ force: true });
        cy.get('#cmx-login-form-password-field').clear();
        cy.get('#cmx-login-form-password-field').type('Amazon123!');
    });
    it('Authentication - Submit', function () {
        cy.get('#cmx-login-form-submit-btn').click({ force: true });
        cy.wait(3000);
    });
    it('Go to Dashboard', function () {
        cy.url().should('eq', 'http://localhost:4200/dashboard');
    });
});