import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

// Providers
import { TranslationService } from '@cemex-core/angular-localization-v1/dist';
import { UserService, ErrorService } from '../../services';
import { Logger } from '@cemex-core/angular-services-v2/dist';

// CMX Dependencies
import { CmxDialogComponent } from '@cemex/cmx-dialog-v4';

// Models
import { UserLog, ApiError } from '../../models';

import { LocalizationComponent } from './../../components/localization/localization.component';

@Component({
    selector: 'app-action-log',
    templateUrl: './action-log.component.html',
    styleUrls: ['./action-log.component.scss'],
})

export class ActionLogComponent extends LocalizationComponent implements OnInit {

    public breadCrumbs;
    public _logEntries: BehaviorSubject<UserLog[]> = new BehaviorSubject<UserLog[]>([]);
    public notesLog;
    public isJobsites;
    public isCustomers;
    public isKey;
    public isJSON;
    public aplicationRoles;
    public objectData;
    public objectJson;
    public errorMsg = '';
    private userAccount = '';
    private userId = 0;
    private dateTo = '2020-12-21 23:59';
    private dateFrom = '1970-01-01 00:01';

    @ViewChild('actionLogNotesModal')
    private actionLogNotesModal: CmxDialogComponent;

    constructor(
        private router: Router,
        public translationService: TranslationService,
        private route: ActivatedRoute,
        private userService: UserService,
        private errorService: ErrorService) {
        super();
        this.route.queryParams.subscribe((params) => {
            this.userAccount = params['id'];
            this.userId = params['data'];
        });
    }

    public ngOnInit(): void {
        this.breadCrumbs = [
            { item: 'views.userManagement.edit.userProfile', link: '/user-management/edit-user', param: this.userAccount },
            { item: 'views.actionLog.actionLogHistory' },
        ];
        if (this.userAccount) {
            this.getUserLog();
        } else {
            this.router.navigate(['dashboard']);
        }
    }

    private getUserLog(): void {
        this.userService
            .getUserFullActionLog(this.userId, this.dateFrom, this.dateTo)
            .subscribe((data) => {
                const tmpData: any[] = this.groupByDate(data.logEntries);
                tmpData.map((entry) => {
                    entry.entryDate = entry.entryDate + '+00:00';
                });
                this._logEntries.next(tmpData);
            },
                (error) => {
                    this.errorService
                        .handleError(error)
                        .subscribe((apiError: ApiError) => {
                            if (apiError) {
                                const errMsg = `${apiError.errorCode}: ${this.translationService.pt(apiError.errorDesc)}`;
                                Logger.error((errMsg));
                            }
                        });
                });
    }

    private groupByDate(items: UserLog[]): UserLog[] {
        if (items !== undefined) {
            const tempItems = this.setArrayElements(items);
            let flag = false;
            let previous = 0;
            for (let index = 0; index < items.length; index++) {
                if (index > 0 && items[index].entryDate.substring(0, 10) === items[previous].entryDate.substring(0, 10)) {
                    flag = true;
                    tempItems[index].show = false;
                } else {
                    flag = false;
                }
                if (index > 0 && !flag) {
                    previous = index;
                }
            }
            return tempItems;
        }
    }

    private setArrayElements(items: UserLog[]): UserLog[] {
        const itemsResult = new Array();
        items.forEach((i) => {
            const item = {
                auditTrailLogId: i.auditTrailLogId,
                entryDate: i.entryDate,
                entryTime: i.entryDate,
                executedBy: i.executedBy,
                changeType: i.changeType,
                action: i.action,
                actionType: i.actionType,
                changedEntityTable: i.changedEntityTable,
                changedEntityId: i.changedEntityId,
                notes: i.notes,
                show: true
            };
            itemsResult.push(item);
        });
        return itemsResult;
    }

    public backToEdit() {
        this.router.navigate(['user-management/edit-user'], { queryParams: { id: this.userAccount } });
    }

    private openNotes(noteDetails) {
        this.notesLog = noteDetails;
        this.notesLog.notes = noteDetails.notes.split(',').join(', ');
        this.isJobsites = noteDetails.action.toLowerCase().indexOf('jobsite') === -1 ? false : true;
        this.isCustomers = noteDetails.action.toLowerCase().indexOf('customer') === -1 ? false : true;
        this.isKey = noteDetails.actionType === 'KEY' ? true : false;
        this.isJSON = noteDetails.actionType === 'JSON' ? true : false;
        if (this.isKey) {
            this.aplicationRoles = noteDetails.notes.split(',');
        }

        if (this.isJSON) {
            const objectJson = JSON.parse(noteDetails.notes);
            const arr = [];
            for (const key in objectJson) {
                if (objectJson.hasOwnProperty(key)) {
                    const value = key === 'userSelectedId' ?
                        objectJson[key] === 'A' ?
                            this.translationService.pt('views.sidebar.active') :
                            this.translationService.pt('views.sidebar.inactive') : objectJson[key];
                    arr.push({ data: key, value: value });
                }
            }
            this.objectJson = arr;
        }
        this.actionLogNotesModal.open();
    }

}
