import {
    Component, ComponentFactoryResolver, ElementRef,
    OnDestroy, OnInit, ViewChild, ViewContainerRef, ChangeDetectorRef, AfterViewChecked
} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

// Providers
import { UserService, ApplicationService, CustomerService, ErrorService } from '../../services';
import { TranslationService } from '@cemex-core/angular-localization-v1/dist';
import { SessionService, Logger } from '@cemex-core/angular-services-v2/dist';
import { CountlyService } from '@cemex-core/helpers-v1/dist';

// Components
import { LocalizationComponent } from './../../components/localization/localization.component';
import { PermissionComponent } from './../../components/permission/permission.component';
import { CmxStepsComponent } from '@cemex/cmx-steps-v4';
import { CmxDropdownComponent } from '@cemex/cmx-dropdown-v4/dist/components/cmx-dropdown';
import { CmxDialogComponent } from '@cemex/cmx-dialog-v4';
import { JobsitesTableComponent } from '../../components/tables/jobsites-table/jobsites-table.component';
import { CustomersTableComponent } from '../../components/tables/customers-table/customers-table.component';

// Models
import { CustomerDetail, Jobsite, Application, ContactInfo, ApiError, EmailStatus } from '../../models';

// Modals
import { JobsitesModalComponent } from '../../components/modals/jobsites/jobsites.modal';
import { LegalEntitiesModalComponent } from '../../components/modals/legal-entities/legal-entities.modal';

// Utils
import { EmailValidators } from '../../helpers/validators';
import { DCMConstants } from '../../helpers/DCM.constants';
import { PasswordTooltipComponent } from '../../components/password-tooltip/password-tooltip.component';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Component({
    selector: 'app-create-user',
    entryComponents: [PermissionComponent],
    templateUrl: 'create-user.component.html',
    styleUrls: ['create-user.component.scss']
})
export class CreateUserComponent extends LocalizationComponent implements OnInit, OnDestroy, AfterViewChecked {

    @ViewChild('permissionsContainer', { read: ViewContainerRef })
    public permissionsContainer: ViewContainerRef;

    @ViewChild('steps')
    public steps: CmxStepsComponent;

    @ViewChild('scrollableDiv')
    public scrollableDiv: ElementRef;

    @ViewChild('successModal')
    public successModal: CmxDialogComponent;

    @ViewChild('errorModal')
    public errorModal: CmxDialogComponent;

    @ViewChild('successLocCreateUser')
    public successLocCreateUser: CmxDialogComponent;

    @ViewChild('warningDelete')
    public warningDelete: CmxDialogComponent;

    @ViewChild('customerDropdown')
    public customerDropdown: CmxDropdownComponent;

    @ViewChild('passtool')
    public passtool: PasswordTooltipComponent;

    @ViewChild('jobsiteModal')
    public jobsiteModal: JobsitesModalComponent;

    @ViewChild('tableJobsiteList')
    public tableJobsiteList: JobsitesTableComponent;

    @ViewChild('tableCustomerList')
    public tableCustomerList: CustomersTableComponent;

    @ViewChild('legalEntitiesModal')
    public legalEntitiesModal: LegalEntitiesModalComponent;

    public form: FormGroup;
    public $customers = new BehaviorSubject<CustomerDetail[]>([]);
    public userApplications: Application[] = [];
    public jobsites: Jobsite[] = [];
    public jobsitesLoggedUser: Jobsite[] = [];
    public filteredJobsites: Jobsite[] = [];
    public selectedJobsiteDelet: Jobsite[] = [];
    public selectedApplications: Application[] = [];
    public selectedCustomers: CustomerDetail[] = [];
    public selectedCustomersDelet: CustomerDetail[] = [];
    public selectedJobsites: Jobsite[] = [];
    public errorMsg = '';
    public customerId: number;
    public customerPlaceholder = '';
    public selectedRoles = [];
    public profileContactInfo: ContactInfo;
    public showMoreCust = false;
    public showMoreJobsite = false;
    public submitting = false;
    public breadCrumbs: any[] = [];
    public passwordHasDigit = false;
    public appsInDOM = 0;
    public selectallCheckedJobsites = false;
    public templateMsg = '';
    public validPass = false;

    private subscription: Subscription;
    private counter = 1;
    public isHauler = false;
    private loggedUserId;
    private isPermissionCompleted = true;
    public searchTerm = '';
    private passwordMaxLength = 20;
    private countryCode = '';
    private isDeleteCustomer = false;

    constructor(
        public translationService: TranslationService,
        public router: Router,
        private customerService: CustomerService,
        private fb: FormBuilder,
        private cdr: ChangeDetectorRef,
        private applicationService: ApplicationService,
        private userService: UserService,
        private componentFactoryResolver: ComponentFactoryResolver,
        private viewContainerRef: ViewContainerRef,
        private sessionService: SessionService,
        private countlyService: CountlyService,
        private errorService: ErrorService) {

        super();

        this.loggedUserId = this.sessionService.userProfile.userId;
        const profile = this.sessionService.userProfile;
        this.isHauler = profile.userType === 'Z';

        this.form = this.fb.group({
            company: [{ value: null, disabled: true }],
            country: [{ value: null, disabled: true }],
            firstName: [null, Validators.compose(
                [Validators.required, Validators.pattern(DCMConstants.REGULAR_EXPRESSIONS.NoSpecialChars)])],
            lastName: [null, Validators.compose(
                [Validators.required, Validators.pattern(DCMConstants.REGULAR_EXPRESSIONS.NoSpecialChars)])],
            position: [null, Validators.compose(
                [Validators.required, Validators.pattern(DCMConstants.REGULAR_EXPRESSIONS.NoSpecialChars)])],
            email: [null, Validators.compose(
                [Validators.required, EmailValidators.pattern])],
            phone: [null, Validators.compose(
                [Validators.required,
                Validators.pattern(DCMConstants.REGULAR_EXPRESSIONS.PhoneNumber)])],
            temporaryPassword: ['', Validators.compose([
                Validators.required,
                Validators.maxLength(this.passwordMaxLength),
                Validators.pattern(DCMConstants.REGULAR_EXPRESSIONS.Password)
            ])]
        });

        this.profileContactInfo = {
            firstName: '',
            lastName: '',
            position: '',
            email: '',
            phone: '',
            company: '',
            country: ''
        };
    }

    public ngAfterViewChecked() {
        this.cdr.detectChanges();
    }

    public ngOnInit(): void {
        this.breadCrumbs = [
            { item: 'views.userManagement.userManagement', link: '/user-management' },
            { item: 'views.userManagement.createUser' },
        ];

        if (!this.isHauler) {
            this.getUserData();
        }

        this.getApplications();

        this.successModal.afterClosed().subscribe((result: boolean) => {
            this.router.navigate(['/user-management']);
        });

        const countryCode = sessionStorage.getItem('country');
        this.passtool.getRulePassword(countryCode);
    }

    public ngOnDestroy(): void {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    public get customerValidation(): boolean {
        return this.isHauler || this.customerId !== undefined;
    }

    public get canAssignNewPermission(): boolean {
        return this.userApplications.length > 0 && this.isPermissionCompleted;
    }

    public goToConsole(): void {
        this.router.navigate(['/user-management']);
    }

    public buildContactInfoData(): void {
        this.profileContactInfo = {
            firstName: this.form.get('firstName').value,
            lastName: this.form.get('lastName').value,
            phone: this.form.get('phone').value,
            email: this.form.get('email').value,
            position: this.form.get('position').value,
            company: this.form.get('company').value,
            country: this.form.get('country').value
        };
    }

    public getRolesString(appId: number) {
        let rolesList = '';
        let roles = [];
        if (this.selectedRoles === undefined || this.selectedRoles.length === 0) {
            return '';
        } else {
            roles = this.selectedRoles.filter((r) => r.appId === appId);
            rolesList = roles.map((r) => {
                return this.translationService.pt(r.roleCode);
            }).join(',');

            return this.formatRolesString(rolesList);
        }
    }

    private getApplications(): void {
        this.applicationService.getUserApplicationsAndRoles().subscribe(
            (dcmApps: Application[]) => {
                let filterApps: Application[];
                // Remove User Management app from the list if does not have any roles.
                const dcmApp = dcmApps.find((app => app.applicationCode === DCMConstants.DCM_APPLICATION_CODE));
                if (dcmApp && dcmApp.roles && dcmApp.roles.length === 0 && this.isHauler) {
                    filterApps = dcmApps
                        .filter((app) => app.applicationCode !== DCMConstants.DCM_APPLICATION_CODE);
                } else {
                    filterApps = dcmApps;
                }
                this.userApplications = filterApps;
            },
            (error) => this.throwError(error));
    }

    private getUserData(): void {
        this.selectedCustomers = [];
        this.customerService
            .getCustomersAndJobsitesForUser(this.loggedUserId)
            .subscribe((data) => {
                this.selectedCustomers = [];
                this.$customers.next(data);
                data.map((customer) => {
                    customer.jobSites.map((jobsite) => {
                        this.jobsitesLoggedUser.push(jobsite);
                    });
                });
            },
                (error) => this.throwError(error));
    }

    private throwError(error, useModal?: boolean): void {
        this.errorService
            .handleError(error)
            .subscribe((apiError: ApiError) => {
                if (apiError) {
                    this.errorMsg = `${apiError.errorCode}: ${this.translationService.pt(apiError.errorDesc)}`;
                    if (useModal) {
                        this.errorModal.open();
                    } else {
                        Logger.error(this.errorMsg);
                    }
                }
            });
    }

    private formatRolesString(rolesList: string): string {
        let formattedRolesList = '';
        const roles: string[] = rolesList.split(',');
        if (roles.length > 0) {
            roles.forEach((role) => {
                if (role) {
                    formattedRolesList += role + ', ';
                }
            });
        }

        if (formattedRolesList.slice(-1) === ' ') {
            formattedRolesList = formattedRolesList.substr(0, formattedRolesList.length - 2);
        }

        return formattedRolesList;
    }

    private updateSelectedCustomers(event): void {
        // Select customers will deleted
        if (event.isDelete) {
            this.selectedCustomersDelet = event.customers;
            this.selectedCustomersDelet = this.selectedCustomersDelet.filter((r) => r.customerId !== this.customerId);
            this.isDeleteCustomer = true;
            this.deleteMultipleLegalEntitiesLocations();
        } else {
            event = event.filter((r) => r.customerId !== this.customerId);
            this.selectedCustomersDelet = event || [];
        }
    }

    private updateSelectedJobsites(event): void {
        if (event.isDelete) {
            this.selectedJobsiteDelet = event.jobsites;
            this.isDeleteCustomer = false;
            this.deleteMultipleLegalEntitiesLocations();
        } else {
            this.selectedJobsiteDelet = event || [];
        }
    }

    private fillCustomerInfo(customerId) {
        let selectedCustomers: CustomerDetail[] = [];
        if (customerId === '' || customerId === undefined || customerId === '0') { // For SAFARI Fix
            this.form.get('company').reset('');
            this.form.get('country').reset('');
            return;
        }

        if (this.customerId) {
            selectedCustomers = this.selectedCustomers.filter((c) => c.customerId !== this.customerId);
            // default exist in the list
            selectedCustomers = selectedCustomers.filter((c) => c.customerId !== customerId);
            this.deleteCustomerForJobsiteList(this.customerId);
        }

        this.customerId = parseInt(customerId, 10);
        this.selectedCustomers = [];
        const customerSelected = this.$customers.getValue().filter((c) => c.customerId === this.customerId)[0];
        this.customerDropdown.placeholder = customerSelected.customerCode;
        this.countryCode =
            customerSelected.countryCode ? customerSelected.countryCode.trim().toUpperCase() : '';

        selectedCustomers.unshift(customerSelected);
        this.form.get('company').setValue(customerSelected.customerDesc);
        this.form.get('country').setValue(this.translationService.pt(this.countryCode));
        this.selectedCustomers = selectedCustomers;

        this.getJobsiteForCustomerList(this.selectedCustomers);

    }

    public deleteCustomerForJobsiteList(customerId) {
        this.selectedJobsites = this.selectedJobsites.filter((c) => c.customerId !== customerId);
        this.selectedCustomers = this.selectedCustomers.filter((c) => c.customerId !== customerId);
        this.filteredJobsites = this.filteredJobsites.filter((c) => c.customerId !== customerId);
    }

    public getJobsiteForCustomerList(customers: CustomerDetail[]) {
        // Get Jobsites
        this.selectedCustomersDelet = [];
        this.jobsites = [];
        customers.forEach((c) => {
            const jobsite = this.jobsitesLoggedUser.filter((j) => j.customerId === c.customerId);

            jobsite.forEach((job) => {
                this.jobsites.push(job);
            });
        });

        this.selectallCheckedJobsites = this.jobsites.length === 0 ? false : this.selectallCheckedJobsites;

        if (this.selectallCheckedJobsites) {
            this.selectedJobsites = JSON.parse(JSON.stringify(this.jobsites));
            this.filteredJobsites = JSON.parse(JSON.stringify(this.jobsites));
        }
    }

    private addPermission() {
        this.isPermissionCompleted = false;
        this.appsInDOM++;
        this.scrollableDiv.nativeElement.scrollIntoView(true);
        const factory = this.componentFactoryResolver.resolveComponentFactory(PermissionComponent);
        const ref = this.viewContainerRef.createComponent(factory);
        this.permissionsContainer.insert(ref.hostView);
        (ref.instance as PermissionComponent).userApplications = this.userApplications;
        (ref.instance as PermissionComponent).counter = this.counter;
        (ref.instance as PermissionComponent).country = this.countryCode;
        (ref.instance as PermissionComponent).templateId = 'CREATE-USER';
        ref.instance.onApplicationSelected.subscribe((data) => {
            this.isPermissionCompleted = false;
            if (data.added) {
                this.counter += 1;
                this.selectedApplications.push(data.applicationToAdd);
                this.userApplications = this.userApplications.filter((app) => app !== data.applicationToAdd);
            } else {
                this.selectedApplications = this.selectedApplications.filter(
                    (app) => app.applicationId !== data.applicationToRemove.applicationId
                );
                this.selectedRoles = this.selectedRoles.filter((role) => role.appId !== data.applicationToRemove.applicationId);
                if (this.userApplications.indexOf(data.applicationToRemove) === -1) {
                    this.userApplications.splice(this.userApplications.indexOf(data.applicationToAdd), 1);
                    this.userApplications.push(data.applicationToRemove);
                }
                this.selectedApplications.push(data.applicationToAdd);
            }
        });
        ref.instance.onRoleAdded.subscribe((data) => {
            if (data.added) {
                const roleVM = {
                    roleId: data.role.roleId,
                    roleCode: data.role.roleCode,
                    roleDesc: data.role.roleDesc,
                    appId: data.appId
                };
                this.selectedRoles.push(roleVM);
            } else {
                this.selectedRoles = this.selectedRoles.filter((r) => r.roleId !== data.role.roleId);
            }
            this.isPermissionCompleted = (this.selectedRoles.filter((r) => r.appId === data.appId).length > 0);
        });
        ref.instance.onDelete.subscribe((data) => {
            if (data) {
                const rolesToDelete = (data.roles.length > 0) ? data.roles.map((r) => r.roleId) : [];
                this.selectedApplications = this.selectedApplications.filter((app) => app.applicationId !== data.applicationId);
                this.selectedRoles = this.selectedRoles.filter((r) => !rolesToDelete.includes(r.roleId));
                this.userApplications.push(data);
            }
            this.appsInDOM--;
            this.isPermissionCompleted = true;
            this.counter = (this.counter <= 1) ? this.counter : this.counter - 1;
            ref.destroy();
        });
        ref.changeDetectorRef.detectChanges();
    }

    private submitUser(): void {
        this.submitting = true;
        const jobsitesPayload = [];
        const legalEntitiesPayload = [];
        const applicationsPayload = [];
        const rolesPayload = [];

        const phone = this.form.get('phone').value;
        const phoneNum = phone.replace(/[^0-9\.]/g, '');

        this.selectedApplications.map((app) => {
            applicationsPayload.push({ applicationId: app.applicationId });
        });

        this.selectedRoles.map((role) => {
            rolesPayload.push({ roleId: role.roleId });
        });

        this.selectedJobsites.map((jobsite) => {
            jobsitesPayload.push({ jobsiteId: jobsite.jobsiteId });
        });

        this.selectedCustomers.map((customer) => {
            legalEntitiesPayload.push({ customerId: customer.customerId });
        });

        const payload = {
            countryCode: this.countryCode,
            userAccount: this.form.get('email').value,
            customerId: this.customerId,
            firstName: this.form.get('firstName').value,
            lastName: this.form.get('lastName').value,
            phoneNumber: phoneNum,
            applicationCode: 'UserProvisioning_App',
            applicationList: applicationsPayload,
            roleList: rolesPayload,
            customerList: legalEntitiesPayload,
            jobsiteList: jobsitesPayload,
            userType: 'C',
            userSelectedId: this.selectallCheckedJobsites ? 'A' : 'S',
            password: this.form.get('temporaryPassword').value,
            allowEmailUpdates: false,
            userPosition: this.form.get('position').value,
            appUserID: DCMConstants.OIM_APP_KEY
        };

        this.subscription = this.userService
            .createUser(payload)
            .subscribe((response) => {
                this.submitting = false;
                const event = {
                    key: 'createUserSuccess',
                };
                this.countlyService.addTracking('add_event', event);

                if (response && !response['emailIsSent']) {
                    const _email: string = this.form.get('email').value;
                    const emailStatus: EmailStatus = {
                        email: _email,
                        sent: false
                    };
                    this.errorService.emailStatus = emailStatus;
                }

                this.successModal.open();
            },
                (error) => {
                    this.submitting = false;
                    this.throwError(error, true);
                }
            );
    }

    public validPassword() {
        this.passtool.firstName = this.form.get('firstName').value !== null ? this.form.get('firstName').value.trim() : '';
        this.passtool.lastName = this.form.get('lastName').value !== null ? this.form.get('lastName').value.trim() : '';
        this.passtool.userAcount = this.form.get('email').value !== null ? this.form.get('email').value.trim() : '';
        this.passtool.active = true;
        setTimeout(function e(this) {
            const input = document.getElementById('cmx-input-temp-password-CREATE-USER');
            if (input) {
                input.removeAttribute('readonly');
                input.focus();
            }
        }, 0);
    }

    public OnValidPassword() {
        this.passtool.firstName = this.form.get('firstName').value !== null ? this.form.get('firstName').value.trim() : '';
        this.passtool.lastName = this.form.get('lastName').value !== null ? this.form.get('lastName').value.trim() : '';
        this.passtool.userAcount = this.form.get('email').value !== null ? this.form.get('email').value.trim() : '';
        this.validPass = this.passtool.isPassValid();
        setTimeout(function e() {
            const input = document.getElementById('cmx-input-temp-password-CREATE-USER');
            if (input) {
                input.setAttribute('readonly', 'true');
                input.blur();
            }
        }, 0);
    }

    public openWarningDialog(isDeleteCustomer) {
        this.isDeleteCustomer = isDeleteCustomer;
        this.templateMsg = isDeleteCustomer ? this.translationService.pt('views.userManagement.unassigned.legalEntity') :
            this.translationService.pt('views.userManagement.unassigned.locations');
        this.warningDelete.open();
    }

    private getValid() {
        this.validPass = this.passtool.isPassValid();
        return (!this.form.valid || !this.customerValidation || !this.validPass) ||
            (this.steps.stepIndex === 1 && !this.isPermissionCompleted);
    }

    /**
     * LEGAL ENTITIES OPTIONS
     */
    public openLegalEntitesDialog() {
        const customers = this.$customers.getValue().filter((c) => c.customerId !== this.customerId);
        const customer = this.$customers.getValue().filter((c) => c.customerId === this.customerId)[0];
        customers.unshift(customer);
        this.legalEntitiesModal.customers = customers;
        this.legalEntitiesModal.filteredCustomers = customers;
        this.legalEntitiesModal.selectedCustomers = JSON.parse(JSON.stringify(this.selectedCustomers));
        this.legalEntitiesModal.defaulCustomer = this.customerId;
        this.legalEntitiesModal.open();
    }

    public assingLegalEntities(event) {
        this.selectedCustomers = event || [];
        const totalCustomers = this.selectedCustomers.length - 1;
        this.getJobsiteForCustomerList(this.selectedCustomers);
        this.tableCustomerList.refreshTable();
    }

    public deleteMultipleLegalEntitiesLocations() {
        this.warningDelete.close();
        if (this.isDeleteCustomer) {
            this.selectedCustomersDelet.forEach((element) => {
                if (element.customerId !== this.customerId) {
                    this.deleteCustomerForJobsiteList(element.customerId);
                }
            });
            this.getJobsiteForCustomerList(this.selectedCustomers);
            this.tableCustomerList.refreshTable();
        } else {
            let jobsiteList = JSON.parse(JSON.stringify(this.selectedJobsites));
            this.selectedJobsiteDelet.forEach((element) => {
                jobsiteList = jobsiteList.filter((r) => r.jobsiteId !== element.jobsiteId);
            });
            this.selectedJobsiteDelet = [];
            this.selectedJobsites = jobsiteList;
            this.filteredJobsites = jobsiteList;
            this.selectallCheckedJobsites = false;
            this.tableJobsiteList.refreshTable();
        }
    }


    /**
     * LOCATIONS OPTIONS
     */

    public openJobsiteDialog() {
        const inputModal = {
            jobsites: this.jobsites,
            selectedJobsites: this.selectedJobsites,
            filteredJobsites: this.jobsites,
            selectAll: this.selectallCheckedJobsites
        };

        this.jobsiteModal.init(inputModal);
    }

    public assingjobsite(event) {
        this.selectedJobsites = event.jobsites;
        this.filteredJobsites = event.jobsites;
        this.selectallCheckedJobsites = event.checkAllJobsites;
        this.selectedJobsiteDelet = [];
        if (this.selectedJobsites.length > 0) {
            if (this.tableJobsiteList !== undefined) {
                this.tableJobsiteList.refreshTable();
            }
        }
    }
}
