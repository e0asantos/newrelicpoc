export class ContactInfo {
    public firstName: string;
    public lastName: string;
    public position: string;
    public email: string;
    public phone: string;
    public company: string;
    public country: string;
}
