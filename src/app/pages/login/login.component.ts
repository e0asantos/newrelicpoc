import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

// Providers
import { SessionService } from '@cemex-core/angular-services-v2/dist/';
import { TranslationService } from '@cemex-core/angular-localization-v1/dist/';

// Models
import { Language } from '../../models';

import { LocalizationComponent } from './../../components/localization/localization.component';

@Component({
    selector: 'app-login',
    templateUrl: 'login.component.html',
    styleUrls: ['login.component.scss']
})

export class LoginComponent extends LocalizationComponent implements OnInit {

    public touError;
    public sessionExpired;
    public currentLang: Language;
    public languages: Language[] = [
        {
            languageId: 8,
            languageName: 'English US',
            languageCountry: 'United States',
            languageISO: 'en_US',
            env: 'prod',
            region: 'US'
        },
        {
            languageId: 9,
            languageName: 'Español México',
            languageCountry: 'México',
            languageISO: 'es_MX',
            env: 'prod',
            region: 'US'
        },
        {
            languageId: 15,
            languageName: 'Español Colombia',
            languageCountry: 'Colombia',
            languageISO: 'es_CO',
            env: 'prod',
            region: 'US'
        },
        {
            languageId: 52,
            languageName: 'Español Costa Rica',
            languageCountry: 'Costa Rica ',
            languageISO: 'es_CR',
            env: 'prod',
            region: 'US'
        },
        {
            languageId: 53,
            languageName: 'Español Panamá',
            languageCountry: 'Panamá',
            languageISO: 'es_PA',
            env: 'prod',
            region: 'US'
        },
        {
            languageId: 54,
            languageName: 'Español Nicaragua',
            languageCountry: 'Nicaragua',
            languageISO: 'es_NI',
            env: 'prod',
            region: 'US'
        },
        {
            languageId: 56,
            languageName: 'Español El Salvador',
            languageCountry: 'El Salvador',
            languageISO: 'es_SV',
            env: 'prod',
            region: 'US'
        },
        {
            languageId: 57,
            languageName: 'Español Dominicana',
            languageCountry: 'República Dominicana',
            languageISO: 'es_DO',
            env: 'prod',
            region: 'US'
        },
        {
            languageId: 58,
            languageName: 'Español Guatemala',
            languageCountry: 'Guatemala',
            languageISO: 'es_GT',
            env: 'prod',
            region: 'US'
        },
        {
            languageId: 59,
            languageName: 'Español Puerto Rico',
            languageCountry: 'Puerto Rico',
            languageISO: 'es_PR',
            env: 'prod',
            region: 'US'
        },
        {
            languageId: 60,
            languageName: 'English Puerto Rico',
            languageCountry: 'Puerto Rico',
            languageISO: 'en_PR',
            env: 'prod',
            region: 'US'
        },
        {
            languageId: 61,
            languageName: 'Español Perú',
            languageCountry: 'Perú',
            languageISO: 'es_PE',
            env: 'prod',
            region: 'US'
        },
        {
            languageId: 1000,
            languageName: '-Technical-',
            languageCountry: 'Tech',
            languageISO: 'tech',
            env: 'nonprod',
            region: 'US'
        },
        {
            languageId: 22,
            languageName: 'English UK',
            languageCountry: 'United Kingdom',
            languageISO: 'en_GB',
            env: 'prod',
            region: 'UK'
        },
        {
            languageId: 29,
            languageName: 'Français',
            languageCountry: 'France',
            languageISO: 'fr_FR',
            env: 'prod',
            region: 'UK'
        },
        {
            languageId: 36,
            languageName: 'Deutsch',
            languageCountry: 'Deutschland',
            languageISO: 'de_DE',
            env: 'prod',
            region: 'UK'
        },
        {
            languageId: 43,
            languageName: 'עברית',
            languageCountry: 'Israel',
            languageISO: 'he_IL',
            env: 'prod',
            region: 'UK'
        },
        {
            languageId: 50,
            languageName: 'Polski',
            languageCountry: 'Polska',
            languageISO: 'pl_PL',
            env: 'prod',
            region: 'UK'
        },
        {
            languageId: 52,
            languageName: 'Español España',
            languageCountry: 'España',
            languageISO: 'es_ES',
            env: 'prod',
            region: 'UK'
        },
        {
            languageId: 53,
            languageName: 'Latviešu',
            languageCountry: 'Latvija',
            languageISO: 'lv_LV',
            env: 'nonprod',
            region: 'UK'
        },
        {
            languageId: 54,
            languageName: 'English Philippines',
            languageCountry: 'Philippines',
            languageISO: 'en_PH',
            env: 'prod',
            region: 'UK'
        },
        {
            languageId: 55,
            languageName: 'Norsk',
            languageCountry: 'Norway',
            languageISO: 'no_NO',
            env: 'nonprod',
            region: 'UK'
        },
        {
            languageId: 56,
            languageName: 'Svenska',
            languageCountry: 'Sweden',
            languageISO: 'sv_SE',
            env: 'nonprod',
            region: 'UK'
        },
        {
            languageId: 57,
            languageName: 'Čeština',
            languageCountry: 'Česká republika',
            languageISO: 'cs_CZ',
            env: 'prod',
            region: 'UK'
        },
        {
            languageId: 58,
            languageName: 'العربية',
            languageCountry: 'جمهورية مصر العربية',
            languageISO: 'ar_EG',
            env: 'nonprod',
            region: 'UK'
        },
        {
            languageId: 69,
            languageName: 'العربية - الأمارات',
            languageCountry: 'الأمارات العربية المتحدة',
            languageISO: 'ar_AE',
            env: 'prod',
            region: 'UK'
        },
        {
            languageId: 68,
            languageName: 'English UAE',
            languageCountry: 'United Arab Emirates',
            languageISO: 'en_AE',
            env: 'nonprod',
            region: 'UK'
        },
        {
            languageId: 59,
            languageName: 'Hrvatski jezik Hrvatska',
            languageCountry: 'Hrvatska',
            languageISO: 'hr_HR',
            env: 'nonprod',
            region: 'UK'
        },
        {
            languageId: 60,
            languageName: 'Hrvatski jezik Bosnia and Herzegovina',
            languageCountry: 'Bosnia and Herzegovina',
            languageISO: 'hr_BA',
            env: 'nonprod',
            region: 'UK'
        },
        {
            languageId: 61,
            languageName: 'Bosanski jezik',
            languageCountry: 'Bosnia and Herzegovina',
            languageISO: 'bs_BA',
            env: 'nonprod',
            region: 'UK'
        },
        {
            languageId: 62,
            languageName: 'Црногорски језик',
            languageCountry: 'Црна Гора',
            languageISO: 'cnr_ME',
            env: 'nonprod',
            region: 'UK'
        }
    ];
    public $languages: BehaviorSubject<Language[]> = new BehaviorSubject<Language[]>([]);
    public showMaintenanceAlert = false;
    public redirectTo: string;

    private env = window['ENV'] || 'dev';
    private defaultLang = window['LANGUAGE'];
    private envLanguages: Language[] = window['CMX_LANGUAGES'];
    private envEuHost = window['HOST_EUROPE'] || '';
    private envAmeHost = window['HOST_AMERICA'] || '';
    private envShowMaintenanceAlert = window['SHOW_MAINTENANCE_ALERT'] || 'false';

    constructor(
        public translationService: TranslationService,
        private activatedRoute: ActivatedRoute,
        private sessionService: SessionService,
        private route: ActivatedRoute) {
        super();

        this.showMaintenanceAlert = (this.envShowMaintenanceAlert === 'true');
        this.touError = window.sessionStorage.getItem('touError');

        this.sessionService.clean();
        window.sessionStorage.removeItem('currentAlertEmail');
        window.sessionStorage.removeItem('touSigned');
        window.sessionStorage.removeItem('touError');

        const langFromCookie = this.sessionService.readCookie('_cmxgo_language');
        if (langFromCookie) {
            this.currentLang = this.envLanguages.filter((lang) => lang.languageISO === langFromCookie)[0];
            const langIso = (this.currentLang) ? this.currentLang.languageISO : this.defaultLang;
            this.translationService.setLanguage(langIso);
            this.currentLang = this.translationService.selectedLanguage;
            this.sessionService.clearCookie('_cmxgo_language');
        } else {
            this.currentLang = this.translationService.selectedLanguage;
            if (this.currentLang.languageISO === 'en_US') {
                this.sessionService.createCookie('_cmxgo_language', 'en_US');
            }
        }

        this.activatedRoute.queryParams.subscribe((params) => {
            this.sessionExpired = params['sessionExpired'];
        });

    }

    public ngOnInit(): void {
        this.redirectTo = this.route.snapshot.queryParams['returnUrl'] || '/';
        if (this.env && this.env === 'prod') {
            this.languages = this.languages.filter((lang) => lang.env === this.env);
        }
        this.getLanguages();
    }

    private getLanguages(): void {
        this.$languages.next(this.languages);
    }

    private checkCookie(name) {
        const cookieName = name + '=';
        if (document.cookie.indexOf(cookieName) === 0) {
            return document.cookie.substring(cookieName.length, document.cookie.length);
        }
        return null;
    }

    private changeLanguage(languageISO: string): void {
        const langIsOk = this.envLanguages.find((lang) => lang.languageISO === languageISO);
        if (langIsOk) {
            this.currentLang = this.envLanguages.filter((lang) => lang.languageISO === languageISO)[0];
            this.translationService.setLanguage(languageISO);
            if (languageISO === 'en_US') {
                this.sessionService.createCookie('_cmxgo_language', languageISO, 30);
            } else {
                this.sessionService.clearCookie('_cmxgo_language');
            }
        } else {
            this.sessionService.createCookie('_cmxgo_language', languageISO, 30);
            if (window.location.host === this.envAmeHost) {
                location.href = `${window.location.protocol}//${this.envEuHost}/login`;
            } else {
                location.href = `${window.location.protocol}//${this.envAmeHost}/login`;
            }
        }
    }
}
