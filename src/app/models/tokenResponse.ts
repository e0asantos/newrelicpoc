import { Application } from './application';
import { UserProfile } from './user-profile';

export class CemexAuth {
    public jwt: string;
    public json: string;
    public oauth2: OAuth2;
    public country: string;
    public applications: Application[];
    public profile: UserProfile;
}

export class OAuth2 {
    public access_token: string;
    public expires_in: number;
    public scope: string;
    public refresh_token: string;
}
