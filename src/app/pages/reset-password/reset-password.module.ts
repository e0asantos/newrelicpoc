import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

// CMX Dependencies
import { CmxButtonModule } from '@cemex/cmx-button-v4';
import { CmxFormFieldModule } from '@cemex/cmx-form-field-v4';
import { CmxDialogModule } from '@cemex/cmx-dialog-v4';

import { PasswordTooltipModule} from './../../components/password-tooltip/password-tooltip.module';

// Internal Dependencies
import { TooltipModule } from '../../components/tooltip/tooltip.module';
import { PanelModule } from '../../components/panel/panel.module';

// Providers
import { PasswordService } from '../../services';

import { ResetPasswordComponent } from './reset-password.component';

export const ROUTES: Routes = [
    { path: '', component: ResetPasswordComponent }
];

@NgModule({
    imports: [
        RouterModule.forChild(ROUTES),
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        NgbModule.forRoot(),
        PasswordTooltipModule,
        TooltipModule,
        PanelModule,
        CmxFormFieldModule,
        CmxButtonModule,
        CmxDialogModule
    ],
    declarations: [
        ResetPasswordComponent
    ],
    exports: [
        ResetPasswordComponent
    ],
    providers: [
        PasswordService
    ]
})
export class ResetPasswordModule { }
