import {
    Component, ComponentFactoryResolver, OnDestroy, OnInit,
    ViewChild, ViewContainerRef, HostListener, ElementRef
} from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

// Providers
import { SessionService, Logger } from '@cemex-core/angular-services-v2/dist/';
import { TranslationService } from '@cemex-core/angular-localization-v1/dist/';
import { ApplicationService, RequestService, ErrorService } from '../../services';
import { Broadcaster } from '@cemex-core/events-v1/dist';

// Components
import { LocalizationComponent } from './../../components/localization/localization.component';
import { AddPermissionComponent } from '../../components/add-permission/add-permission.component';
import { CmxDialogComponent } from '@cemex/cmx-dialog-v4';

// Models
import { UserProfile, Role, Application, EmailStatus, ApiError } from '../../models';

// Utils
import { DCMConstants } from '../../helpers/DCM.constants';

class AppViewModel {
    public applicationId: number;
    public applicationName: string;
    public applicationCode: string;
    public rolesViewModel: RolesViewModel[];
    public assigned: boolean;
}

class RolesViewModel {
    public roleId: number;
    public roleName: string;
    public roleCode: string;
    public assigned: boolean;
}

@Component({
    selector: 'app-permission-request',
    templateUrl: 'permission-request.component.html',
    styleUrls: ['permission-request.component.scss'],
    entryComponents: [AddPermissionComponent]
})
export class PermissionRequestComponent extends LocalizationComponent implements OnInit, OnDestroy {

    @ViewChild('permissionsContainer', { read: ViewContainerRef })
    public permissionsContainer: ViewContainerRef;

    @ViewChild('successModal')
    public successModal: CmxDialogComponent;

    @ViewChild('errorModal')
    public errorModal: CmxDialogComponent;

    @ViewChild('pendingChangesWarningModal')
    public pendingChangesWarningModal: CmxDialogComponent;

    @ViewChild('scrollableDiv')
    public scrollableDiv: ElementRef;

    public breadCrumbs: any[] = [];
    public sidebarIsCollapsed = false;
    public applicationsCatalog: Application[] = new Array<Application>();
    public selectedApplications = new Array<any>();
    public selectedRoles = new Array<any>();
    public permissonIsCompleted = true;
    public errorMsg = '';
    private userApplications: Application[] = new Array<Application>();
    private subscription: Subscription;
    private counter = 0;
    private permissionViewModel = [];
    private userLogged: UserProfile;
    private userCountry: string;
    private redirectTo = '';

    constructor(
        public translationService: TranslationService,
        private applicationService: ApplicationService,
        private componentFactoryResolver: ComponentFactoryResolver,
        private viewContainerRef: ViewContainerRef,
        private userRequestService: RequestService,
        private sessionService: SessionService,
        private router: Router,
        private errorService: ErrorService) {

        super();

        this.userLogged = this.sessionService.userProfile;
        this.userCountry = sessionStorage.getItem('country') || '';
    }

    public ngOnInit(): void {
        this.breadCrumbs = [
            { item: 'views.requestPermission.lblProfile', link: '/profile' },
            { item: 'views.requestPermission.lblRequest' },
        ];
        this.getApplicationsCatalog();
        this.successModal.afterClosed().subscribe((result: boolean) => {
            this.router.navigate(['/profile']);
        });
    }

    public canDeactivate() {
        if (this.hasPendingChanges) {
            this.pendingChangesWarningModal.open();
            return false;
        }
        return true;
    }

    public ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

    public get canAssignNewPermission() {
        return (this.selectedApplications.length !== this.applicationsCatalog.length) && this.permissonIsCompleted;
    }

    public submitPermissionRequest() {
        this.selectedApplications.forEach((app) => {
            app.applicationName = this.translationService.pt(app.applicationName);
        });
        const accessRequest = {
            request: {
                type: 'authorization',
                applicationList: this.selectedApplications,
                roleList: this.selectedRoles,
            },
            notes: 'Access Request'
        };

        this.userRequestService
            .submitPermissionRequest(accessRequest)
            .subscribe((response) => {
                if (response && !response['emailIsSent']) {
                    const emailStatus: EmailStatus = {
                        email: undefined,
                        sent: false
                    };
                    this.errorService.emailStatus = emailStatus;
                }

                this.successModal.open();
                this.selectedApplications = [];
                this.selectedRoles = [];
                window.scrollTo(0, 0);
            },
                (error) => this.throwError(error, true));
    }

    private throwError(error, useModal?: boolean): void {
        this.errorService
            .handleError(error)
            .subscribe((apiError: ApiError) => {
                if (apiError) {
                    this.errorMsg = `${apiError.errorCode}: ${this.translationService.pt(apiError.errorDesc)}`;
                    if (useModal) {
                        this.errorModal.open();
                    } else {
                        Logger.error(this.errorMsg);
                    }
                }
            });
    }

    // Get Applications and Roles which can be assigned to the user
    private getApplicationsCatalog(): void {
        this.subscription = this.applicationService
            .getAllApplicationsAndRoles()
            .subscribe((data) => {
                if (data && data.length > 0) {
                    data.forEach((app) => {
                        if (DCMConstants.DCM_APPLICATIONS.some((s) => s === app.applicationCode)) {
                            this.applicationsCatalog.push(app);
                        }
                    });
                    this.getUserApplications();
                }
            },
                (error) => this.throwError(error));
    }

    // Get User current applications and roles
    private getUserApplications(): void {
        this.subscription = this.applicationService
            .getApplicationsAndRolesForUser(this.userLogged.userId)
            .subscribe((data) => {
                this.userApplications = data;
                this.buildPermisionViewModel();
            },
                (error) => this.throwError(error));
    }

    // Build View Model based on Applications Catalog and Current Applications and Roles of User.
    private buildPermisionViewModel(): void {
        let appViewModel: AppViewModel;
        this.applicationsCatalog.forEach((app) => {
            appViewModel = {
                applicationName: app.applicationName,
                applicationId: app.applicationId,
                applicationCode: app.applicationCode,
                assigned: this.userHasApplication(app.applicationId),
                rolesViewModel: this.getRolesViewModel(app.roles, app.applicationId)
            };
            this.permissionViewModel.push(appViewModel);
        });
    }

    private getRolesViewModel(roles, appId: number) {
        const rolesViewModel: RolesViewModel[] = [];
        if (roles.length > 0) {
            roles.forEach((role) => {
                if (!this.userHasRoleForApplication(appId, role.roleId)) {
                    const roleVM: RolesViewModel = {
                        roleId: role.roleId,
                        roleName: role.roleName,
                        roleCode: role.roleCode,
                        assigned: false
                    };

                    rolesViewModel.push(roleVM);
                }
            });
        }
        return rolesViewModel;
    }

    private userHasApplication(appId: number): boolean {
        return this.userApplications.some((app) => app.applicationId === appId) ||
            this.permissionViewModel.some((app) => app.applicationId === appId);
    }

    private userHasRoleForApplication(appId: number, roleId: number): boolean {
        const appSelected = this.userApplications.filter((app) => app.applicationId === appId)[0];
        if (appSelected) {
            return appSelected.roles.some((role) => role.roleId === roleId);
        } else {
            return false;
        }
    }

    public get hasPendingChanges(): boolean {
        return this.selectedApplications.length > 0 || this.selectedRoles.length > 0;
    }

    private goToProfile(): void {
        this.redirectTo = 'profile';
        this.router.navigate([this.redirectTo]);
    }

    private goToHome(): void {
        this.redirectTo = 'dashboard';
        this.router.navigate([this.redirectTo]);
    }

    public leaveRoute() {
        this.selectedApplications = [];
        this.selectedRoles = [];
        this.permissonIsCompleted = true;
        this.router.navigate([this.redirectTo]);
    }

    private appIsAlreadySelected(applicationId: number): boolean {
        if (this.selectedApplications.length > 0) {
            return this.selectedApplications.findIndex((app) => app.applicationId === applicationId) > -1;
        } else {
            return false;
        }
    }

    public addPermissionRow() {
        this.scrollableDiv.nativeElement.scrollIntoView(true);
        this.permissonIsCompleted = false;
        const factory = this.componentFactoryResolver.resolveComponentFactory(AddPermissionComponent);
        const ref = this.viewContainerRef.createComponent(factory);
        this.permissionsContainer.insert(ref.hostView);

        this.counter += 1;
        (ref.instance as AddPermissionComponent).userApplications =
            this.permissionViewModel.filter((p) => !this.appIsAlreadySelected(p.applicationId));
        (ref.instance as AddPermissionComponent).counter = this.counter;
        (ref.instance as AddPermissionComponent).templateId = 'PERMISSION-REQUEST';
        (ref.instance as AddPermissionComponent).isRTL = this.isRTL;
        ref.instance.onApplicationSelected.subscribe((data) => {
            this.permissonIsCompleted = false;
            const appToAdd = {
                applicationId: data.applicationToAdd.applicationId,
                applicationCode: data.applicationToAdd.applicationCode,
                applicationName: data.applicationToAdd.applicationName
            };
            if (data.added) {
                this.selectedApplications.push(appToAdd);
            } else {
                this.selectedApplications =
                    this.selectedApplications
                        .filter((app) => app.applicationId !== data.applicationToRemove.applicationId);
                this.selectedApplications.push(appToAdd);
                this.selectedRoles = this.selectedRoles.filter((role) => role.applicationId !== data.applicationToRemove.applicationId);
            }
        });
        ref.instance.onRoleToggled.subscribe((data) => {
            if (data.added) {
                this.selectedRoles.push({
                    roleId: data.role.roleId,
                    roleCode: data.role.roleCode,
                    applicationId: data.appId
                });
            } else {
                this.selectedRoles = this.selectedRoles.filter((role) => role.roleId !== data.role.roleId);
            }
            this.permissonIsCompleted = (this.selectedRoles.filter((r) => r.applicationId === data.appId).length > 0);
        });
        ref.instance.onRemovePermission.subscribe((data) => {
            if (data) {
                this.selectedApplications = this.selectedApplications.filter((app) => app.applicationId !== data.applicationId);
                this.selectedRoles = this.selectedRoles.filter((app) => app.applicationId !== data.applicationId);
                this.permissonIsCompleted = !(this.selectedRoles.filter((r) => r.applicationId === data.applicationId).length > 0);
            } else {
                this.permissonIsCompleted = true;
            }
            this.counter -= 1;
            ref.destroy();
        });
        ref.changeDetectorRef.detectChanges();
    }
}
