const gulp = require('gulp');
const purify = require('gulp-purifycss');
const minify = require('gulp-minify');
var options = {
    minify: true,
    output: "src/assets/dls/dls.bundle.css"
};

var optionsCustom = {
    minify: true,
    output: "src/assets/styles/custom.css"
};

gulp.task('css', function() {
  return gulp.src('submodules/dls/dist/css/app.css')
    .pipe(purify(['./src/**/*.html']))
    .pipe(gulp.dest('src/assets/dls'));
});

gulp.task('minify', function() {
    gulp.src('src/assets/dls/app.css')
      .pipe(purify(['./src/**/*.html'], options));
    
    gulp.src('src/assets/styles/custom.css')
      .pipe(purify(['./src/**/*.html'], optionsCustom));
    
    return true;
});
