describe('Action Log', function () {
    before(function () {
        cy.clearCookies();
        cy.clearLocalStorage();
        cy.window().then((win) => {
            win.sessionStorage.clear();
        });
    });
    beforeEach(function () {
        window.localStorage.setItem('language', 'en_US');
    });
    it('Login Page', function () {
        cy.visit('http://localhost:4200/login/');
    });
    it('Authenticate', function () {
        cy.get('#cmx-login-form-username-field').clear({force:true});
        cy.get('#cmx-login-form-username-field').type('da.us.qa@yopmail.com', {force:true});
        cy.get('#cmx-login-form-password-field').clear({force:true});
        cy.get('#cmx-login-form-password-field').type('Amazon123!', {force:true});
        cy.get('#cmx-login-form-submit-btn').click({ force: true });
        cy.wait(5000);
    });
    it('It is on Dashboard', function () {
        cy.url().should('include', 'dashboard');
    });
    it('Go to User Management', function () {
        cy.get('#cmx-card-app-UserProvisioning_App-DASHBOARD').click({ force: true });
        cy.url().should('include', 'user-management/inbox');
    });
    it('Go to Edit User', function () {
        cy.get('#1').should('have.class', 'nav-link');
        cy.get('#1').click({ force: true });
        cy.get('#cmx-table-users-username-2-USER-MANAGEMENT').click({ force: true });
        cy.wait(2000);
        cy.url().should('include', 'edit-user');
    });
    it('Go to User Action Log', function () {
        cy.get('#cmx-btn-profile-box-action-log-EDIT-USER').click({ force: true });
        cy.wait(2000);
        cy.url().should('include', 'action-log');
    });
    it('Open action detail', function () {
        cy.get('#cmx-result-detail-0-ACTION-LOG').click({ force: true });
    });
    it('Close action detail and go back to edit user page', function () {
        cy.get('#cmx-btn-ok-notes-ACTION-LOG').click({ force: true });
        cy.get('#cmx-btn-back-ACTION-LOG').click({ force: true });
    });
});