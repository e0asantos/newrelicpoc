import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';

// Providers
import { TranslationService } from '@cemex-core/angular-localization-v1/dist';

// Models
import { UserProfile } from '../../models';

// Components
import { PasswordChangeModal } from '../../components/modals/password-change/password-change.modal';

@Component({
    selector: 'profile-box',
    templateUrl: 'profile-box.component.html',
    styleUrls: ['profile-box.component.scss']
})

export class ProfileBoxComponent {

    public _profile: UserProfile;
    public isDigitalAdmin = false;

    @Input() set dataSource(value) {
        this._profile = value;
        this.isDigitalAdmin = value ? value.isDigitalAdmin  : false;
    }

    get profile(): UserProfile {
        return this._profile;
    }
    @Input() public templateId: string;
    @Input() public imgPath = '';
    @Input() public isAdmin = false;
    @Input() public isRTL = false;

    @Output() public onDisableUser = new EventEmitter();
    @Output() public onOpenActionLog = new EventEmitter();

    @ViewChild('passwordChange') public passwordChange: PasswordChangeModal;

    constructor(public translationService: TranslationService) { }

    public disableUser(): void {
        this.onDisableUser.emit();
    }

    public openActionLog(): void {
        this.onOpenActionLog.emit();
    }

    private openPasswordModal() {
        const event = {
            userId: this._profile.userId.toString(),
            userAccount: this._profile.userAccount
        };
        this.passwordChange.open(event);
    }
}
