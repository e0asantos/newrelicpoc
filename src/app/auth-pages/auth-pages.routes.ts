import { Routes, RouterModule, UrlSegment } from '@angular/router';

// Guards
import { AuthGuard } from '../guards/auth.guard';
import { AdminAuthGuard } from '../guards/admin-auth.guard';
import { SeveralErrorComponent } from './several-error/several-error.component';

import { AuthPagesComponent } from './auth-pages.component';
import { CustomerInfoLauncherComponent } from '../launchers/customer-info-launcher/customer-info-launcher.component';
import { TermsConditionsGuard } from '../guards/terms-conditions.guard';
import { NgModule } from '@angular/core';

const pagesRoutes: Routes = [
    {
        path: '',
        component: AuthPagesComponent,
        canActivate: [AuthGuard, TermsConditionsGuard],
        children: [
            { path: 'several-error', component: SeveralErrorComponent },
            {
                path: 'dashboard',
                loadChildren: './dashboard/dashboard.module#DashboardModule'
            },
            {
                path: 'profile',
                loadChildren: './profile/profile.module#ProfileModule'
            },
            {
                path: 'settings',
                loadChildren: './settings/settings.module#SettingsModule'
            },
            {
                path: 'user-management',
                loadChildren: './user-management/user-management.module#UserManagementModule',
                canActivate: [AuthGuard, AdminAuthGuard],
            },
            // {
            //     matcher: (url: UrlSegment[]) => {
            //         return url.length === 1 && url[0].path.match(/(customerinfo|commercialconditions)/) ? ({ consumed: url }) : null;
            //     },
            //     component: CustomerInfoLauncherComponent
            //     // loadChildren: '../launchers/customer-info-launcher/customer-info-launcher.module#CustomerInfoLauncherModule'
            // },
            {
                path: 'customerinfo',
                component: CustomerInfoLauncherComponent
                // loadChildren: '../launchers/customer-info-launcher/customer-info-launcher.module#CustomerInfoLauncherModule'
            },
            {
                path: 'commercialconditions',
                component: CustomerInfoLauncherComponent
            },
            {
                path: 'commercialconditions/quote-requests',
                component: CustomerInfoLauncherComponent
            },
            { path: '', redirectTo: 'dashboard', pathMatch: 'full' }
        ]
    },
    {
        path: '',
        component: AuthPagesComponent,
        children: [
            {
                path: 'public/legal',
                loadChildren: './terms/terms.module#TermsModule'
            },
            {
                path: 'public/privacy',
                loadChildren: './privacy/privacy.module#PrivacyModule'
            },
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(pagesRoutes)
    ],
    exports: [RouterModule]
})

export class AuthRoutingModule { }
