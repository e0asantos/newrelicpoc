import { Component, OnInit, ViewChild, Inject, Optional } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

// Providers
import { SessionService } from '@cemex-core/angular-services-v2/dist/';
import { TranslationService } from '@cemex-core/angular-localization-v1/dist';
import { UserService } from '../../services';

// Utils
import { PasswordValidator, NumberValidators, EmailValidators } from '../../helpers/validators';
import { DCMConstants } from '../../helpers/DCM.constants';

// Components
import { CmxDialogComponent } from '@cemex/cmx-dialog-v4';
import { LocalizationComponent } from './../../components/localization/localization.component';

// Models
import { Country, Language } from '../../models';
declare var global: any;
@Component({
    selector: 'app-signup',
    templateUrl: 'signup.component.html',
    styleUrls: ['signup.component.scss']
})

export class SignupComponent extends LocalizationComponent implements OnInit {

    private static USE_TRANSLATION_SERVER = 'USE_TRANSLATION_SERVER';

    @ViewChild('successModal')
    public successModal: CmxDialogComponent;

    @ViewChild('errorModal')
    public errorModal: CmxDialogComponent;

    @ViewChild('passwordWarningModal')
    public passwordWarningModal: CmxDialogComponent;

    public form: FormGroup;
    public submitted = false;
    public key = DCMConstants.GOOGLE_RECAPTCHA_KEY;
    public search = '';
    public countries = [];
    public languages: Language[] = [];
    public countrySelected: boolean;
    public passwordHasDigit: boolean = false;
    public captchaOk = false;
    private subscription: Subscription;
    private currentLangFromStorage = '';
    private errorMessage = '';
    private euHost = (global as any)['HOST_EUROPE'];
    private ameHost = (global as any)['HOST_AMERICA'];
    private subdomain = (global as any)['SITE_DOMAIN'];
    private euRegion = ['GB', 'IL', 'DE', 'FR', 'PL'];

    constructor(
        private fb: FormBuilder,
        private userService: UserService,
        private router: Router,
        public translationService: TranslationService,
        private sessionService: SessionService,
        @Inject(SignupComponent.USE_TRANSLATION_SERVER) @Optional()
        private useTranslationServer: boolean) {

        super();
        this.currentLangFromStorage = (localStorage.getItem('language') || 'en_US');
    }

    public ngOnInit(): void {
        this.form = this.fb.group({
            firstName: [{ value: '', disabled: false }, Validators.compose([
                Validators.required, Validators.pattern(DCMConstants.REGULAR_EXPRESSIONS.NoSpecialChars)])],
            lastName: [{ value: '', disabled: false }, Validators.compose([
                Validators.required, Validators.pattern(DCMConstants.REGULAR_EXPRESSIONS.NoSpecialChars)])],
            position: [{ value: '', disabled: false },
            Validators.compose([Validators.required, Validators.pattern(DCMConstants.REGULAR_EXPRESSIONS.NoSpecialChars)])],
            customerCode: [{ value: '', disabled: false }, Validators.compose([Validators.required])],
            company: [{ value: '', disabled: false }, Validators.required],
            country: [{ value: '', disabled: false }, Validators.required],
            email: [{ value: '', disabled: false }, Validators.compose([Validators.required, EmailValidators.pattern])],
            phone: [null, Validators.compose(
                [Validators.required,
                Validators.pattern(DCMConstants.REGULAR_EXPRESSIONS.PhoneNumber)])],
            password: ['', Validators.compose([
                Validators.required,
                Validators.pattern(DCMConstants.REGULAR_EXPRESSIONS.Password),
                Validators.maxLength(20),
                PasswordValidator.strength])],
            confirmPassword: ['', Validators.compose([
                Validators.required])]
        }, { validator: PasswordValidator.match('password', 'confirmPassword') });

        this.languages = window['CMX_LANGUAGES'];
    }

    public goToLogin(): void {
        this.router.navigate(['/login']);
    }

    public resolved(captchaResponse: string): void {
        this.captchaOk = captchaResponse !== '';
    }

    public selectLangCountry(countryCode: string): void {
        if (countryCode === '') {
            this.form.controls['countryCode'].setValidators(null);
            this.countrySelected = false;
            return;
        }
        this.countrySelected = true;
    }

    public createUser(skipWarning: boolean = false): void {
        if (!skipWarning && PasswordValidator.force <= 45) {
            this.passwordWarningModal.open();
        } else {
            this.submitted = true;
            const phone = this.form.get('phone').value;
            const phoneNum = parseInt(phone.replace(/[^0-9\.]/g, ''), 10);
            const _user = {
                customerCode: this.form.get('customerCode').value,
                userAccount: this.form.get('email').value,
                firstName: this.form.get('firstName').value,
                lastName: this.form.get('lastName').value,
                countryCode: this.form.get('country').value,
                applicationCode: 'UserProvisioning_App',
                phoneNumber: phoneNum.toString(),
                password: this.form.get('password').value,
                userPosition: this.form.get('position').value,
                allowInformationShare: true,
                allowEmailUpdates: true
            };

            this.subscription = this.userService.signUp(_user).subscribe(
                (response) => {
                    this.submitted = false;
                    this.successModal.open();
                },
                (error) => {
                    this.submitted = false;
                    this.errorModal.open();
                }
            );
            this.passwordWarningModal.close();
        }
    }

    private createCookie(name: string, value: string, days: number) {
        let expires = '';
        if (days) {
            const date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = date.toUTCString();
        }
    }

    private readCookie(name: string): string {
        const ca = document.cookie.split(';');
        const caLen: number = ca.length;
        const cookieName = `${name}=`;
        let c: string;
        for (let i: number = 0; i < caLen; i += 1) {
            c = ca[i].replace(/^\s+/g, '');
            if (c.indexOf(cookieName) === 0) {
                return c.substring(cookieName.length, c.length);
            }
        }
        return null;
    }

    private hasNumber(keyUpEvent: KeyboardEvent): void {
        this.passwordHasDigit = /\d/.test(this.form.controls['password'].value);
    }

}
