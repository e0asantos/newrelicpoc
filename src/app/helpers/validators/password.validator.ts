import { AbstractControl, FormGroup } from '@angular/forms';

export class PasswordValidator {

    public static force: number = 0;

    public static strength(c: AbstractControl): { [key: string]: boolean } {
        const p = c.value;

        if (p === null) {
            return null;
        }

        const _regex = /[\!@#$-/:-?{-~!"^_`\[\]]/g; // "

        const _lowerLetters = /[a-z]+/.test(p);
        const _upperLetters = /[A-Z]+/.test(p);
        const _numbers = /[0-9]+/.test(p);
        const _symbols = _regex.test(p);
        const _flags = [_lowerLetters, _upperLetters, _numbers, _symbols];

        let _passedMatches = 0;
        for (const _flag of _flags) {
            _passedMatches += _flag === true ? 1 : 0;
        }

        PasswordValidator.force += 2 * p.length + ((p.length >= 10) ? 1 : 0);
        PasswordValidator.force += _passedMatches * 10;

        // penality (short password)
        PasswordValidator.force = (p.length <= 7) ? Math.min(PasswordValidator.force, 10) : PasswordValidator.force;
        // penality (poor variety of characters)
        PasswordValidator.force = (_passedMatches === 1) ? Math.min(PasswordValidator.force, 10) : PasswordValidator.force;
        PasswordValidator.force = (_passedMatches === 2) ? Math.min(PasswordValidator.force, 20) : PasswordValidator.force;
        PasswordValidator.force = (_passedMatches === 3) ? Math.min(PasswordValidator.force, 30) : PasswordValidator.force;
        PasswordValidator.force = (_passedMatches === 4) ? Math.min(PasswordValidator.force, 50) : PasswordValidator.force;

        return null;
    }

    public static match(passwordKey: string, confirmPasswordKey: string) {
        return (group: FormGroup): { [key: string]: any } => {
            const password = group.controls[passwordKey];
            const confirmPassword = group.controls[confirmPasswordKey];
            if (password.value !== confirmPassword.value) {
                return {
                    mismatchedPasswords: true
                };
            }
        };
    }
}
