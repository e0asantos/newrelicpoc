import { Component, OnInit, ViewChild } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

// Providers
import { SessionService, Logger } from '@cemex-core/angular-services-v2/dist';
import { TranslationService } from '@cemex-core/angular-localization-v1/dist';
import { CountlyService } from '@cemex-core/helpers-v1/dist';
import {
    ApplicationService,
    RequestService,
    JobsiteService,
    CustomerService,
    ErrorService
} from '../../services';

// Components
import { CmxDialogComponent } from '@cemex/cmx-dialog-v4';
import { LocalizationComponent } from './../../components/localization/localization.component';
import { SearchComponent } from '../../components/search/search.component';
import { JobsitesModalComponent } from '../../components/modals/jobsites/jobsites.modal';

// Utils
import { SearchFunctions } from '../../helpers/functions/search.functions';
import { DCMConstants } from '../../helpers/DCM.constants';

// Models
import {
    Application,
    UserProfile,
    Jobsite,
    CustomerDetail,
    Request,
    EmailStatus,
    ApiError
} from '../../models';

import * as _ from 'lodash';

@Component({
    selector: 'app-profile',
    templateUrl: 'profile.component.html',
    styleUrls: ['profile.component.scss']
})

export class ProfileComponent extends LocalizationComponent implements OnInit {

    @ViewChild('requestDetailModal')
    public requestDetailModal: CmxDialogComponent;

    @ViewChild('confirmRequestCancellationModal')
    public confirmRequestCancellationModal: CmxDialogComponent;

    @ViewChild('successModal')
    public successModal: CmxDialogComponent;

    @ViewChild('successModalAccessRequest')
    public successModalAccessRequest: CmxDialogComponent;

    @ViewChild('errorModal')
    public errorModal: CmxDialogComponent;

    @ViewChild('searchCustomer')
    public searchCustomer: SearchComponent;

    @ViewChild('searchJobsite')
    public searchJobsite: SearchComponent;

    @ViewChild('dataRequestJobsiteModal')
    public dataRequestJobsiteModal: JobsitesModalComponent;

    public profilePicPath = 'dist/assets/images/no-profile.jpg';
    public user: UserProfile;
    public breadCrumbs: any[] = [];
    public tabSelected = 'apps';
    public selectedRequest: Request;
    public requestsToCancel: number[] = [];
    public userApplications = new BehaviorSubject<Application[]>([]);
    public userCustomers = new BehaviorSubject<CustomerDetail[]>([]);
    public userJobsites = new BehaviorSubject<Jobsite[]>([]);
    public userRequests = new BehaviorSubject<Request[]>([]);
    public backupUserJobsites: Jobsite[] = [];
    public requestJobsites: Jobsite[] = [];
    public backupUserCustomers: CustomerDetail[] = [];
    public backupUserRequests: Request[] = [];
    public userSelectedId = 'S';
    public isDigitalAdmin = false;
    public customerUserType = DCMConstants.USER_TYPE_CAT.Customer;
    public errorMsg = '';
    public emailSent = true;
    private searchTerm: string;
    public userCustomersIds: string;
    private _mapIcon: {
        [index: string]: string;
    };

    constructor(
        public searchFunctions: SearchFunctions,
        public translationService: TranslationService,
        private sessionService: SessionService,
        private applicationService: ApplicationService,
        private customerService: CustomerService,
        private jobsiteService: JobsiteService,
        private requestService: RequestService,
        private countlyService: CountlyService,
        private errorService: ErrorService) {

        super();
        this._mapIcon = {
            Foreman_App: 'cmx-icon-module-track',
            INVOICE_PAYMENTS: 'cmx-icon-module-payment',
            Drivers_App: 'cmx-icon-module-drivers',
            CustomerInfo_App: ' cmx-icon-module-customer-information',
            QuotationPricing_App: 'cmx-icon-module-commercial-condition',
            OrderProductCat_App: 'cmx-icon-module-order-product-catalogue',
            DeliverySchedule_App: 'cmx-icon-module-delivery-schedule',
            UserProvisioning_App: 'cmx-icon-module-user-management'
        };
        const userFromSession: any = this.sessionService.userProfile;
        const countryFromSession = sessionStorage.getItem('country') || '';
        this.user = {
            userAccount: userFromSession.userAccount,
            userId: userFromSession.userId,
            firstName: userFromSession.firstName,
            lastName: userFromSession.lastName,
            countryCode: countryFromSession,
            userPosition: userFromSession.userPosition,
            phoneNumber: userFromSession.phoneNumber,
            customerDesc: (userFromSession.customer) ? userFromSession.customer.customerDesc : '',
            customerCode: (userFromSession.customer) ? userFromSession.customer.customerCode : '',
            customerId: userFromSession.customerId,
            userType: userFromSession.userType,
            status: userFromSession.status
        };
        const user = JSON.parse(sessionStorage.getItem('user_profile'));
        this.userSelectedId = user.userSelectedId;
        this.isDigitalAdmin = user.isDigitalAdmin || false;
    }

    public ngOnInit(): void {
        this.checkEmailStatus();
        this.breadCrumbs = [
            { item: 'views.userManagement.homeRoute', link: '/dashboard' },
            { item: 'views.profile.title' },
        ];
        if (this.countlyService && this.countlyService.Countly.q) {
            this.countlyService.addTrackingPage('Profile');
        }
        this.getUserApplications();
        this.getUserCustomers();

        if (this.user.userType === DCMConstants.USER_TYPE_CAT.Customer) {
            this.getUserRequests();
        }

        this.successModal.afterClosed().subscribe((result: boolean) => {
            this.requestDetailModal.close();
            this.getUserRequests();
        });

        this.requestDetailModal.afterClosed().subscribe((result: boolean) => {
            this.selectedRequest = undefined;
        });

        this.errorModal.afterClosed().subscribe((result: boolean) => {
            this.getUserRequests();
        });

        this.successModalAccessRequest.afterClosed().subscribe((result: boolean) => {
            this.getUserRequests();
        });
    }

    public filter(filterBy: string): void {
        this.searchTerm = filterBy;
        if (this.tabSelected === 'customers') {
            this.userCustomers.next([]);
            this.userCustomers.next(this.backupUserCustomers);
            const arrayFilter = {
                customerDesc: this.searchTerm,
                customerCode: this.searchTerm
            };
            if (this.searchTerm !== '') {
                const filteredCustomers = this.searchFunctions.filterData(this.userCustomers.getValue(), arrayFilter);
                this.userCustomers.next(filteredCustomers);
            }
        } else if (this.tabSelected === 'locations') {
            this.userJobsites.next([]);
            this.userJobsites.next(this.backupUserJobsites);
            const arrayFilter = {
                jobsiteCode: this.searchTerm,
                jobsiteDesc: this.searchTerm,
                customerDesc: this.searchTerm
            };
            if (this.searchTerm !== '') {
                const filteredJobsites = this.searchFunctions.filterData(this.userJobsites.getValue(), arrayFilter);
                this.userJobsites.next(filteredJobsites);
            }
        }
    }

    public onTabChange(tabSelected: string) {
        this.tabSelected = tabSelected;
        this.searchTerm = '';
        this.userCustomers.next(this.backupUserCustomers);
        this.userJobsites.next(this.backupUserJobsites);

        if (this.searchCustomer && this.searchJobsite) {
            this.searchCustomer.searchTerm = '';
            this.searchJobsite.searchTerm = '';
        }
    }

    public cancelRequests(selectedReq: Request): void {
        this.confirmRequestCancellationModal.close();
        this.requestDetailModal.close();
        if (selectedReq) {
            if (!this.requestsToCancel.find((reqId) => reqId === selectedReq.requestId)) {
                this.requestsToCancel.push(selectedReq.requestId);
            }
        }
        let cancelRequest;
        const ids = this.requestsToCancel.map((r) => r).toString();
        cancelRequest = {
            ids,
            notes: this.translationService.pt('views.profile.requests.cancelledByUser')
        };

        this.requestService
            .cancelRequests(cancelRequest)
            .subscribe((status) => {
                this.successModal.open();
                this.requestsToCancel = [];
            },
                (error) => this.throwError(error));
    }

    public openRequestDetail(selectedRequest: Request): void {
        this.selectedRequest = selectedRequest;
        this.requestDetailModal.open();
    }

    public rolesPerApplication(appId: number) {
        const payload: any = this.selectedRequest.details;
        const selectedRoles = payload.roleList;
        let rolesPerApp = [];
        if (selectedRoles && appId) {
            rolesPerApp = selectedRoles.filter((r) => r.applicationId === appId);
        }
        return rolesPerApp;
    }

    private getUserApplications(): void {
        this.applicationService
            .getUserApplicationsAndRoles()
            .subscribe((dcmApps: Application[]) => {
                let filterApps: Application[];

                // Remove User Management app from the list if does not have any roles.
                const dcmApp = dcmApps.find((app => app.applicationCode === DCMConstants.DCM_APPLICATION_CODE));
                if (dcmApp && dcmApp.roles && dcmApp.roles.length === 0) {
                    filterApps = dcmApps
                        .filter((app) => app.applicationCode !== DCMConstants.DCM_APPLICATION_CODE);
                } else {
                    filterApps = dcmApps;
                }

                this.userApplications.next(filterApps);
            },
                (error) => this.throwError(error));
    }

    private getUserCustomers(): void {
        this.customerService
            .getCustomersAndJobsitesForUser(this.user.userId)
            .subscribe((data) => {
                this.userCustomers.next(data);
                this.backupUserCustomers = data;
                this.userCustomersIds = data.length > 0 ? data.map((c) => c.customerId).toString() : '';

                this.backupUserJobsites = data.map(a => a.jobSites).flat();
                this.userJobsites.next(this.backupUserJobsites);
                // GET JOBSITE FOR REQUESTS
                this.getUserJobsitesForCustomers();
            },
                (error) => this.throwError(error));
    }

    private getUserJobsitesForCustomers(): void {
        this.jobsiteService.getUserJobsitesForCustomers(this.userCustomersIds).subscribe(
            (data) => {
                if (data.length > 0) {
                    this.requestJobsites = data.filter((j) =>
                        !this.backupUserJobsites.some((jobsite) => jobsite.jobsiteId === j.jobsiteId));
                }
            },
            (error) => this.throwError(error)
        );
    }

    private getUserRequests(): void {
        this.backupUserRequests = [];
        this.requestService
            .getRequestsForUser(this.user.userId)
            .subscribe((data) => {
                let requests;
                requests = data.map((request) => {
                    return {
                        requestId: request.authorizationRequestId,
                        userId: request.userId,
                        requesterName: request.userFullName,
                        customerDesc: request.customerDesc,
                        requestDate: request.requestDate,
                        authorizedBy: request.authorizedBy,
                        authorizedByFullName: request.authorizedByFullName,
                        notes: request.notes,
                        status: request.status,
                        statusDesc: this.translationService.pt(this.getStatusName(request.status)),
                        customerId: request.customerId,
                        details: (request.payload) ? JSON.parse(request.payload) : null,
                        requestTypeDesc: this.translationService.pt(this.getRequestTypeName(request.requestType)),
                        requestType: request.requestType,
                    };
                });
                this.backupUserRequests = requests;
                this.userRequests.next(requests);
            },
                (error) => this.throwError(error));
    }

    private throwError(error): void {
        this.errorService.handleError(error)
            .subscribe((apiError: ApiError) => {
                if (apiError) {
                    this.errorMsg = `${apiError.errorCode}: ${this.translationService.pt(apiError.errorDesc)}`;
                    Logger.error(this.errorMsg);
                }
            });
    }

    private checkEmailStatus(): void {
        const emailStatus: EmailStatus = this.errorService.emailStatus;
        if (emailStatus && !emailStatus.sent) {
            this.emailSent = emailStatus.sent;
            if (emailStatus.email) {
                this.errorMsg = this.translationService.pt('views.resetPassword.EmailNotDelivered');
            } else {
                this.errorMsg = this.translationService.pt('views.profile.genericEmailError');
            }
        } else {
            this.emailSent = true;
            this.errorMsg = '';
        }
        this.errorService.resetEmailStatus();
    }

    private getStatusName(statusCode: string): string {
        if (statusCode === 'N' || statusCode === 'New') {
            return 'views.global.statusNew';
        } else if (statusCode === 'A' || statusCode === 'Approved') {
            return 'views.global.statusApproved';
        } else if (statusCode === 'R' || statusCode === 'Rejected') {
            return 'views.global.statusRejected';
        } else if (statusCode === 'P' || statusCode === 'Pending') {
            return 'views.global.statusPending';
        } else if (statusCode === 'C' || statusCode === 'Cancelled') {
            return 'views.global.statusCancelled';
        } else {
            return '';
        }
    }

    private getRequestTypeName(requestType: string): string {
        if (requestType === 'R' || requestType === 'Role') {
            return 'views.requestDetailModal.lblRole';
        } else if (requestType === 'D' || requestType === 'Data') {
            return 'views.requestDetailModal.lblData';
        } else { return ''; }
    }

    private getIdIcon(applicationIcon: string): string {
        const appIcon: string = this._mapIcon[applicationIcon];
        return appIcon;
    }

    private openDataRequest() {
        const input = {
            jobsites: this.requestJobsites,
            filteredJobsites: this.requestJobsites,
            selectedJobsites: [],
            selectAll: false
        };
        this.dataRequestJobsiteModal.init(input);
    }

    public submitDataAccessRequest(event): void {
        const dataRequest: any = {
            request: {
                type: 'authorization',
                jobsiteList: event
            },
            notes: 'Data Request'
        };

        this.requestService.submitDataAccessRequest(dataRequest)
            .subscribe((response) => {
                if (response && !response['emailIsSent']) {
                    const emailStatus: EmailStatus = {
                        email: undefined,
                        sent: false
                    };
                    this.errorService.emailStatus = emailStatus;
                }
                this.successModalAccessRequest.open();
            },
                (error) => this.throwError(error));
    }
}
